/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "imagepreview.h"
#include <QMouseEvent>
#include <QDebug>
#include <QPainter>

ImagePreview::ImagePreview(QWidget* parent, Qt::WindowFlags f)
    : QLabel(parent)
{
}

ImagePreview::~ImagePreview() {}

void ImagePreview::mousePressEvent(QMouseEvent *event) {
//    qDebug() << event->pos();
    painted_pixmap = frame_pixmap;
    if (tip[0] == -1) {
        tip[0] = event->pos().x();
        tip[1] = event->pos().y();
        drawSnoutLine();
    }
    else if (middle[0] == -1) {
        middle[0] = event->pos().x();
        middle[1] = event->pos().y();
        drawSnoutLine();
    }
    emit clicked();
}

void ImagePreview::clearSnoutLine()
{
    tip[0] = -1;
    middle[0] = -1;
    this->setPixmap(frame_pixmap);
    emit(lineCleared());
}

void ImagePreview::drawSnoutLine()
{
    painted_pixmap = frame_pixmap;
    if (tip[0] != -1 && middle[0] == -1) {
        // draw the tip point as a red circle
        QPainter painter(&painted_pixmap);
        QPen pen;
        pen.setColor(Qt::red);
        pen.setWidth(3);
        painter.setPen(pen);
        QPoint center;
        center.setX(tip[0]);
        center.setY(tip[1]);
        painter.drawEllipse(center, 2, 2);
        this->setPixmap(painted_pixmap);
    }
    else if (tip[0] != -1 && middle[0] != -1) {
        // draw complete snout line
        QPainter painter(&painted_pixmap);
        QPen pen;
        pen.setColor(Qt::red);
        pen.setWidth(2);
        painter.setPen(pen);
        painter.drawLine(tip[0], tip[1], middle[0], middle[1]);
        // show image with snout line
        this->setPixmap(painted_pixmap);

        emit lineDrawn();
    }
}
