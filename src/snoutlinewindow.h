/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SNOUTLINEWINDOW_H
#define SNOUTLINEWINDOW_H

#include <QDialog>
#include <opencv2/opencv.hpp>
#include "src/options/options.h"
#include "imagepreview.h"

namespace Ui {
class SnoutLineWindow;
}

class SnoutLineWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SnoutLineWindow(cv::VideoCapture video, int startFrame, QWidget *parent = 0);
    ~SnoutLineWindow();

    void showFrame();

    int tip[2];
    int middle[2];
    int frame_nr;
    cv::VideoCapture whisker_video;
    int startFrame;
    int endFrame;
    bool upside_whiskers = false;
    bool invertColors = false;

    void resizeEvent(QResizeEvent *event);

    void closeEvent(QCloseEvent *event);

signals:
    void frameChanged();
    void bgFramesChanged(int first, int last);

private slots:
    void on_ConfirmButtons_accepted();

    void on_ConfirmButtons_rejected();

    void on_LineDrawn();

    void on_LineCleared();

    void on_frameSlider_valueChanged(int value);

    void on_firstFrame_bg_slider_valueChanged(int value);

    void on_firstFrame_bg_spinBox_valueChanged(int arg1);

    void on_lastFrame_bg_slider_valueChanged(int value);

    void on_lastFrame_bg_spinBox_valueChanged(int arg1);

    void on_whiskersUpside_checkBox_clicked(bool checked);

    void on_InvertColors_checkBox_clicked(bool checked);

private:
    Ui::SnoutLineWindow *ui;
};

#endif // SNOUTLINEWINDOW_H
