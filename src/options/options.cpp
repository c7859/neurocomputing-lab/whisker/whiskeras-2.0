/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "options.h"
#include "../stages/whisker_point_detection/imgproc.h"

void get_opts(general_opts &generalOpts, dump_opts &dumpOpts, preprocessing_opts &pre_opts, c_extraction_opts &cExtractionOpts,
              c_extraction_cuda &detectionCuda, clustering_opts &clusteringOpts, stitch_I_opts &stitchIOpts,
              stitch_II_opts &stitchIIOpts, param_opts &paramOpts, track_opts &trackOpts, train_opts &trainOpts,
              fwptd_opts &fwptdOpts, recog_opts &recogOpts, N_expert_opts &NExpertOpts) {
    get_preprocessing_opts(pre_opts);
    get_general_opts(generalOpts, pre_opts.upscaling);
    get_dump_opts(dumpOpts);
    get_clustering_opts(clusteringOpts, generalOpts);
    get_c_extraction_opts(cExtractionOpts, clusteringOpts.upside_whiskers, clusteringOpts.a_value, clusteringOpts.b_value, dumpOpts.getTimingProfile);
    get_stitching_I_opts(stitchIOpts, pre_opts.upscaling);
    get_stitching_II_opts(stitchIIOpts, pre_opts.upscaling);
    get_parametrization_opts(paramOpts, pre_opts, clusteringOpts, generalOpts);
    get_tracking_opts(trackOpts, clusteringOpts.origin, pre_opts);
    get_training_opts(trainOpts, paramOpts.minAngleToSnout);
    get_fwptd_opts(fwptdOpts);
    get_recognition_opts(recogOpts);
    get_N_expert_opts(NExpertOpts);
}

void get_general_opts(general_opts &generalOpts, float upscaling) {
//    generalOpts.videopath = "/media/petros/WD_Petros/Thesis/Whisker_Videos/temp3.avi"; // whole temp3 video (18 min)
    // temp3
    generalOpts.tip[0] = 382*upscaling; generalOpts.tip[1] = 35*upscaling;
    generalOpts.middle[0] = 14*upscaling; generalOpts.middle[1] = 138*upscaling;
    // temp4
//    generalOpts.tip[0] = 500*upscaling; generalOpts.tip[1] = 70*upscaling;
//    generalOpts.middle[0] = 70*upscaling; generalOpts.middle[1] = 210*upscaling;
    // red
//    generalOpts.tip[0] = 420; generalOpts.tip[1] = 820;
//    generalOpts.middle[0] = 100; /* 120 */ generalOpts.middle[1] = 400; /* 370 */
    // silver
//    generalOpts.tip[0] = 200*upscaling; generalOpts.tip[1] = 361*upscaling;
//    generalOpts.middle[0] = 80*upscaling; generalOpts.middle[1] = 162*upscaling;
    // blue
//    generalOpts.tip[0] = 440*upscaling; generalOpts.tip[1] = 210*upscaling;
//    generalOpts.middle[0] = 270*upscaling; generalOpts.middle[1] = 2*upscaling;
    generalOpts.startFrame = 0;
    generalOpts.endFrame =  4000; // -1 means until the end of the video
//    generalOpts.use_cuda = true;
    generalOpts.loadTracks = false; // load existing tracks
    generalOpts.loadTracksLocation = "../output/tracks/tracks.txt";
    generalOpts.verbose = 0;
}

void get_plotting_opts(plotting_opts &plottingOpts, clustering_opts clusteringOpts, float upscaling, cv::VideoCapture video,
                       bool &preprocessing, bool &centerlineExtraction, bool &clustering, bool &stitching_I,
                       bool &stitching_II, bool &parametrization, bool &tracking) {
    plottingOpts.width = video.get(cv::CAP_PROP_FRAME_WIDTH) * upscaling;
    plottingOpts.height = video.get(cv::CAP_PROP_FRAME_HEIGHT) * upscaling;
    plottingOpts.startFrame = -1;
    plottingOpts.endFrame = -1;
    plottingOpts.colormap = cv::COLORMAP_HSV;
    plottingOpts.framing = 0;

    plottingOpts.dontShow = false;
    plottingOpts.saveIntermediateResults = false;
    plottingOpts.outputDir = "../output/stages/";

    // plotting options for each step
    plottingOpts.plot_unprocessed_frame = false;
    plottingOpts.plot_unprocessed_frame_with_snout_line = false;
    preprocessing = false;
    centerlineExtraction = false;
    clustering = false;
    stitching_I = false;
    stitching_II = false;
    parametrization = false;
    tracking = false;
}

void determinePlottingSnoutCutoff(plotting_opts &plottingOpts, clustering_opts clusteringOpts) {
    plottingOpts.cutoff = clusteringOpts.cutoff;
    plottingOpts.origin[0] = clusteringOpts.origin[0];
    plottingOpts.origin[1] = clusteringOpts.origin[1];
    plottingOpts.sbase[0][0] = clusteringOpts.sbase[0][0];
    plottingOpts.sbase[0][1] = clusteringOpts.sbase[0][1];
    plottingOpts.sbase[1][0] = clusteringOpts.sbase[1][0];
    plottingOpts.sbase[1][1] = clusteringOpts.sbase[1][1];
}

void get_dump_opts(dump_opts &dumpOpts) {
    dumpOpts.saveTracks = false; // save tracks
    dumpOpts.saveTracksLocation = "../output/tracks/tracks.txt";
    dumpOpts.getTimingProfile = false;
    dumpOpts.timeProfileLocation = "../output/timing_profile/profile.txt";
//    dumpOpts.outputFilename = "angles.csv";
//    dumpOpts.getQualityResults = false; // get detection/tracking results according to quality criteria
//    dumpOpts.qualityOutputDir = "../output/quality_results/";
}

void get_preprocessing_opts(preprocessing_opts &pre_opts) {
    // background extraction parameters
    // whiskers shave and morphology extraction parameters
    pre_opts.bwThreshold = 1;
    pre_opts.dilate_value = 10; // How much of the fur should be shaved?
    // noise filtering parameters
    pre_opts.deinterlacingFlag = false;
    pre_opts.initialGaussianSigma = 0;
    // contrast enhancement parameters
    pre_opts.histEqMinT = 3;
    pre_opts.histEqMaxT = 100;
    pre_opts.histEqGamma = 0.95;
    computeAdjustRange(pre_opts);
    // invert colors if back is black and whiskers are white
    pre_opts.invertColors = false;
    // edge removal parameters
    pre_opts.remove_lowlevels = 10;
    pre_opts.framing = 2;
    pre_opts.upscaling = 2;
    pre_opts.verbose = 0;
    pre_opts.plot = false;
}

void computeAdjustRange(preprocessing_opts &pre_opts) {
    pre_opts.adjustRangeIn[0] = double(pre_opts.histEqMinT)/255;
    pre_opts.adjustRangeIn[1] = double(pre_opts.histEqMaxT)/255;
}

void get_c_extraction_opts(c_extraction_opts &cExtractionOpts, bool upside_whiskers, float a, float b, bool verbose) {
    cExtractionOpts.tr2 = -1.5; // decrease (abs value) for more sensitivity
    cExtractionOpts.sigma = 2.3;
    // don't increase gaussianKernelSize when using CUDA
    // if you do, you will have to increase KERNEL_LENGTH in convolutionSeperable_common.h to increase the size
    // of the global memory array to store the gx, gxx etc kernels
    cExtractionOpts.gaussianKernelSize = 20;
    cExtractionOpts.precision = CV_32F;
    cExtractionOpts.upside_whiskers = upside_whiskers;
    cExtractionOpts.a_value = a;
    cExtractionOpts.b_value = b;
    cExtractionOpts.plot = false;
    cExtractionOpts.verbose = 0;

    // compute gaussian kernels
    computeGaussianKernels(cExtractionOpts, verbose);
}

void get_clustering_opts(clustering_opts &clusteringOpts, general_opts generalOpts) {
    clusteringOpts.upside_whiskers = false;
    determineSnoutCutoff(clusteringOpts, generalOpts);
    /// parameters to alter
    clusteringOpts.steger_c = 1; // weight for angle [deg] difference when determining neighbours, with relation to distance.
    clusteringOpts.cutoff = 0; // distance from the snout that whiskers are detected (prevents detection of fur)
    clusteringOpts.maxAngle = 7; // [deg] max angle difference for two neighbouring points
    /// improved curve tracing
    clusteringOpts.L = 3;
    clusteringOpts.M = 15;
    clusteringOpts.K = 15;
    clusteringOpts.minClusterSizeToExtend = 10;
    clusteringOpts.plot = false;
    clusteringOpts.verbose = 0;
}

void determineSnoutCutoff(clustering_opts &clusteringOpts, general_opts generalOpts) {
    clusteringOpts.origin[0] = (generalOpts.tip[0]-generalOpts.middle[0])/2 + generalOpts.middle[0];
    clusteringOpts.origin[1] = (generalOpts.tip[1]-generalOpts.middle[1])/2 + generalOpts.middle[1];
    clusteringOpts.a_value = (float) (generalOpts.tip[1]-generalOpts.middle[1]) / (generalOpts.tip[0]-generalOpts.middle[0]);
    // if the whiskers grow on the upper side, reverse the angle (+CV_PI)
    float alpha = atan(clusteringOpts.a_value) + (clusteringOpts.upside_whiskers ? CV_PI : 0);
    clusteringOpts.b_value = generalOpts.tip[1] - clusteringOpts.a_value*generalOpts.tip[0];
    clusteringOpts.sbase[0][0] = cos(alpha); clusteringOpts.sbase[0][1] = sin(alpha);
    clusteringOpts.sbase[1][0] = -sin(alpha); clusteringOpts.sbase[1][1] = cos(alpha);

}

void get_stitching_I_opts(stitch_I_opts &stitchIOpts, float upscaling) {
    stitchIOpts.coneMin = 7 * CV_PI / 180; // [rads] the cone value is modelled as a linear function
    stitchIOpts.coneMax = 20 * CV_PI / 180; // [rads] of distance d between clusters with a max and a min value
    stitchIOpts.coneMinDist = 25; // distance at which cone gets its minimum value.
    stitchIOpts.c = 200; // Similar to <steger_c>, there's a weight for angle [rad] difference,
    // for each potential stitching, a linear Radon Transform between the two whiskers is done to rule out erroneous stitchings
    stitchIOpts.radonLength = 20; // length of intermediate parts whose intensity is checked
    stitchIOpts.radonThreshold = 30; // threshold for intensity per point to accept stitching
    // [rads] max angle difference between tip and bottom.
    stitchIOpts.maxAD = 20 * CV_PI/180.0;
    // minimum length of tip and bottom in px.
    stitchIOpts.lengthOfTipAndBottom = 15;
    // Minimum cluster size (in points) after local clustering.
    // Clusters shorter than this value will be regarded as noise.
    stitchIOpts.minClusterSize = 7;
    stitchIOpts.plot = false;
    stitchIOpts.separate_plot = false;
    stitchIOpts.verbose = 0;
}

void get_stitching_II_opts(stitch_II_opts &stitchIIOpts, float upscaling) {
    // If a whisker's lowest point is higher than <highestRoot>, try to extend it towards the snout
    stitchIIOpts.highestRoot = 10;
    // The procedure works by 'inoculating' a whisker onto the whisker it has been hiding behind. These two variables
    // indicate what is the acceptable distance between the lowest point of the whisker and the whisker it was hiding behind
    stitchIIOpts.acceptableXdistance = 100;
    stitchIIOpts.lengthOfTipAndBottom = 30;
    stitchIIOpts.minClusterSize = 20;
    stitchIIOpts.cone = 20 * CV_PI / 180; // [rads] only accept inoculation points in a cone
    stitchIIOpts.maxAD = 20 * CV_PI / 180; // max angle difference between tip and bottom, where tip ends at inoculation point
    // define a line above which the area is blind
//    stitchIIOpts.blind_start[0] = 370; stitchIIOpts.blind_start[1] = 110;
//    stitchIIOpts.blind_end[0] = 550; stitchIIOpts.blind_end[1] = 160;
    stitchIIOpts.verbose = 0;
    stitchIIOpts.plot = false;
    stitchIIOpts.separate_plot = false;
}

void get_parametrization_opts(param_opts &paramOpts, preprocessing_opts pre_opts, clustering_opts clusteringOpts, general_opts generalOpts) {
    // Clusters with fewer than <param_opt.minNrOfDots> datapoints will be ignored.
    paramOpts.minNrOfDots = 40;
    // Clusters with the distance between tip and bottom smaller than <param_opt.minLength> will be ignored.
    paramOpts.minLength = 150;
    // Parametrizations that do not comply with these requirements will be ignored.
    paramOpts.minAngleToSnout = 5 * CV_PI / 180; // rads
    paramOpts.maxBend = 0.01;
    paramOpts.absMaxX = sqrt(pow(generalOpts.tip[0]-generalOpts.middle[0], 2) + pow(generalOpts.tip[1]-generalOpts.middle[1], 2)) / 1.4;
    paramOpts.wgtfun = "talwar";
    paramOpts.maxMSE = FLT_MAX;
    paramOpts.verbose = 0;
    paramOpts.plot = false;
    paramOpts.separate_plot = false;
}

void get_tracking_opts(track_opts &trackOpts, float *origin, preprocessing_opts pre_opts) {
    trackOpts.dontTrack = false;
    // Kalman filter
    trackOpts.useKalman = true;
    trackOpts.kalmanDistance = 10; // use data from up to <kalmanDistance> earlier frames
    trackOpts.weightB = 0.05;
    trackOpts.weightL = 0.01;
    trackOpts.weightAlpha = 1.5;
    trackOpts.weightX = 2.5;
    trackOpts.maxChangeInAngle = 15 * CV_PI / 180;
    trackOpts.maxChangeInXf = 50;
    trackOpts.maxChangeInBend = 0.01;
    trackOpts.maxChangeInLength = 1000;
    trackOpts.maxScoreForMatch = 50;
    // plotting
    trackOpts.plot = false;
    trackOpts.verbose = 0;
}

void get_training_opts(train_opts &trainOpts, float minAngleToSnout) {
    trainOpts.retrainFreqInit = 1; // Minimum retraining frequency in the beginning (frames/retraining).
    trainOpts.retrainAtLeast = 5; // Absolute minimum retraining frequency.
    // Increase <train_opt.retrainFreqInit> with <train_opt.increaseWith> every <train_opt.increaseEvery> frames.
    trainOpts.increaseEvery = 100;
    trainOpts.increaseWith = 1;
    trainOpts.bootstrap = 100;
    trainOpts.window = 7000; // Sliding window size for training data
    // Maximum size of training data per whisker during training.
    // If <train_opt.maxSizeTrainingData> is smaller than <train_opt.window>,
    // the training examples of which the average angle of all the whiskers is most similar to the current situation will be chosen.
    trainOpts.maxSizeTrainingData = 2500;
    trainOpts.chance_train = 20; // percentage of data used for training

    // libLinear parameters
    // support vector classification by Crammer and Singer --only this solver is good for the task
    trainOpts.linear_param.solver_type = 2;// 4;
    // C provides better accuracy as it increases from 1 to 100 but also increases cost tremendously
    // better set it to 1 as the accuracy increases anyway as the SVM works its way through the examples
    // or even configure it high at the beginning with few examples and set it low later
    trainOpts.linear_param.C = 1;
    trainOpts.linear_param.p = 0.1;
    trainOpts.linear_param.nu = 0.5;
    trainOpts.linear_param.eps = HUGE_VAL;
//    trainOpts.linear_param.eps = 1e-3;
    trainOpts.linear_param.nr_weight = 0;
    trainOpts.linear_param.regularize_bias = 1;
    trainOpts.linear_param.weight_label = NULL;
    trainOpts.linear_param.weight = NULL;
    trainOpts.linear_param.init_sol = NULL;
    // set to QUIET mode
    void (*print_func)(const char*) = NULL;	// default printing to stdout
    print_func = &print_null;
    set_print_string_function(print_func); // QUIET mode
    trainOpts.verbose = 0;
}

void get_fwptd_opts(fwptd_opts &fwptdOpts) {
    // Max average absolute distance (as a function of real distance and difference in length) between cluster and parametrization.
    fwptdOpts.maxMatchDistance = pow(40, 2);
    // Maximum for minimum distance between cluster point and parametrization.
    // If the distance between the closest point and the parametrization is higher than this, matching is impossible.
    fwptdOpts.maxLowestDistance = pow(10, 2);
    // define the above as the square of the distance (for practical purposes)
}

void get_recognition_opts(recog_opts &recogOpts) {
    recogOpts.weightX = 1;
    recogOpts.weightA = 1;
    recogOpts.weightB = 0.05;
    recogOpts.weightL = 0.09;
    recogOpts.verbose = 0;
}

void get_N_expert_opts(N_expert_opts &NExpertOpts) {
    NExpertOpts.maxChangeInAngle = 15 * CV_PI / 180;
    NExpertOpts.maxChangeInXf = 100;
    NExpertOpts.maxChangeInBend = 0.01;
    NExpertOpts.maxChangeInLength = 1000;
    NExpertOpts.verbose = 0;
}
