/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "imgproc.h"

cv::Mat gaussian2D(float sigma, int ksize) {
   cv::Mat gaussian(ksize, ksize, CV_32FC1);

   for (int i = 0; i < ksize; i++) {
       for (int j = 0; j < ksize; j++) {
           int x = i+1 - ksize / 2;
           int y = j+1 - ksize / 2;
           float &intensity = (float&) gaussian.ptr<float>(i)[j];
           intensity = exp(-(pow(x,2) + pow(y,2))/(2*pow(sigma, 2))) / (2*CV_PI*(pow(sigma, 2)));
       }
   }

   return gaussian;
}

cv::Mat derivativeXImg(cv::Mat src, int degree) {

    cv::Mat derivative(src.rows, src.cols-1, CV_32FC1);
    for (int i = 0; i < derivative.rows; i++) {
        for (int j = 0; j < derivative.cols; j++) {
            int y = i;
            int x = j + 1;
            float intensity_curr = src.at<float>(y, x);
            float intensity_prev = src.at<float>(y, x-1);
            float &intensity = (float&) derivative.ptr<float>(i)[j];
            intensity = intensity_curr - intensity_prev;
        }
    }
    return (degree > 1) ? derivativeXImg(derivative, degree - 1) : derivative;
}

cv::Mat derivativeYImg(cv::Mat src, int degree) {

    cv::Mat derivative(src.rows-1, src.cols, CV_32FC1);
    for (int i = 0; i < derivative.rows; i++) {
        for (int j = 0; j < derivative.cols; j++) {
            int y = i + 1;
            int x = j;
            float intensity_curr = src.at<float>(y, x);
            float intensity_prev = src.at<float>(y-1, x);
            float &intensity = (float&) derivative.ptr<float>(i)[j];
            intensity = intensity_curr - intensity_prev;
        }
    }
    return (degree > 1) ? derivativeYImg(derivative, degree - 1) : derivative;
}

void splitSeparableFilter(cv::Mat sepFilter, cv::Mat& kcol, cv::Mat& krow) {
    cv::Mat s, u, vt;
    cv::SVD::compute(sepFilter, s, u, vt);
    kcol = u.col(0) * sqrt(s.at<float>(0));
    krow = vt.row(0) * sqrt(s.at<float>(0));
}

/**
 * @brief   an approximation of the matlab function imajust
 * @copydoc https://stackoverflow.com/questions/31647274/is-there-any-function-equivalent-to-matlabs-imadjust-in-opencv-with-c
 */
void imadjust(const cv::Mat& src, cv::Mat& dst, double in[2], double out[2], float gamma){
    // src : input CV_8U image
    // dst : output CV_8U image
    // in  : src image bounds
    // out : dst image bounds
    CV_Assert(src.depth() == CV_8U);
    if (invalidRange(in[0]) || invalidRange(in[1]) || invalidRange(out[0]) || invalidRange(out[1])) {
        perror("Invalid range in imadjust");
        exit(EXIT_FAILURE);
    }
    if (gamma < 0) {
        perror("Gamma must not be negative in imadjust");
        exit(EXIT_FAILURE);
    }

    dst = src.clone();

    // Stretching
    double div_factor = in[1] - in[0];
    double mul_factor = out[1] - out[0];
    for (int r = 0; r < dst.rows; r++) {
        for (int c = 0; c < dst.cols; ++c) {
            uchar &intensity_src = (uchar&) src.ptr(r)[c];
            uchar &intensity_dst = (uchar&) dst.ptr(r)[c];
            double dsrc = uchar2double(intensity_src);
            // make sure intensity_src is in the range (in[0], in[1])
            double ddst = dsrc < in[0] ? in[0] : dsrc >= in[1] ? in[1] : dsrc;
            ddst = pow((ddst - in[0]) / div_factor, gamma);
            ddst = ddst * mul_factor + out[0];
            intensity_dst = cv::saturate_cast<uchar>(double2uchar(ddst));
        }
    }
}

bool invalidRange(double range) {
    bool invalid = false;
    if (range < 0 || range > 1) invalid = true;
    return invalid;
}

double uchar2double(uchar value) {
    double dval = double(value) / 255;
    return dval;
}

uchar double2uchar(double value) {
    uchar uval = lround(value * 255);
    return uval;
}

void deinterlaceLineRepImage(cv::Mat frame) {
    CV_Assert(frame.depth() == CV_8U);
    for (int j = 1; j < frame.rows; j += 2) {
        for (int k = 0; k < frame.cols; k++) {
            uchar &even = frame.ptr<uchar>(j-1)[k];
            uchar &odd = frame.ptr<uchar>(j)[k];
            odd = even;
        }
    }
}

/** @function dilation
 *  @copydoc https://docs.opencv.org/2.4/doc/tutorials/imgproc/erosion_dilatation/erosion_dilatation.html */
cv::Mat dilation(cv::Mat frame, int dilation_size, int dilation_elem) {
    int dilation_type;
    if (dilation_elem == 0 ) { dilation_type = cv::MORPH_RECT; }
    else if (dilation_elem == 1) { dilation_type = cv::MORPH_CROSS; }
    else { dilation_type = cv::MORPH_ELLIPSE; }

    cv::Mat element = cv::getStructuringElement(dilation_type,
                                                cv::Size(2*dilation_size + 1, 2*dilation_size+1 ),
                                                cv::Point(dilation_size, dilation_size));
    cv::Mat dilated;
    // Apply the dilation operation
    cv::dilate(frame, dilated, element);

    return dilated;
}

/** @function erosion
 *  @copydoc https://docs.opencv.org/2.4/doc/tutorials/imgproc/erosion_dilatation/erosion_dilatation.html */
cv::Mat erosion(cv::Mat frame, int erosion_size, int erosion_elem) {
    int erosion_type;
    if (erosion_elem == 0 ) { erosion_type = cv::MORPH_RECT; }
    else if (erosion_elem == 1) { erosion_type = cv::MORPH_CROSS; }
    else { erosion_type = cv::MORPH_ELLIPSE; }

    cv::Mat element = cv::getStructuringElement(erosion_type,
                                                cv::Size(2*erosion_size + 1, 2*erosion_size+1 ),
                                                cv::Point(erosion_size, erosion_size));
    cv::Mat eroded;
    /// Apply the dilation operation
    cv::erode(frame, eroded, element);

    return eroded;
}

cv::Mat removeEdge(cv::Mat src, int framing) {
    assert(framing < src.rows && framing < src.cols); // ensure framing is well-defined

    cv::Mat edgeRemovedImage = src.clone();
    for (int r = 0; r < framing; r++) {
        for (int c = 0; c < edgeRemovedImage.cols; ++c) {
            uchar &up = (uchar &) edgeRemovedImage.ptr<uchar>(r)[c];
            up = 0;
            uchar &down = (uchar &) edgeRemovedImage.ptr<uchar>(edgeRemovedImage.rows - 1 - r)[c];
            down = 0;
        }
    }
    for (int r = 0; r < edgeRemovedImage.rows; r++) {
        for (int c = 0; c < framing; ++c) {
            uchar &left = (uchar&) edgeRemovedImage.ptr<uchar>(r)[c];
            left = 0;
            uchar &right = (uchar&) edgeRemovedImage.ptr<uchar>(r)[edgeRemovedImage.cols-1-c];
            right = 0;
        }
    }
    return edgeRemovedImage;

}
