/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "c_extraction.h"
#include "imgproc.h"
#include "../../utils/Timer.hpp"
// cuda
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>

template<typename T>
void convertMatToVector(cv::Mat mat, std::vector<T> &vector) {
    if (mat.isContinuous()) {
        // array.assign((float*)mat.datastart, (float*)mat.dataend); // <- has problems for sub-matrix like mat = big_mat.row(i)
        vector.assign((T*)mat.data, (T*)mat.data + mat.total());
    } else {
        for (int i = 0; i < mat.rows; ++i) {
            vector.insert(vector.end(), mat.ptr<T>(i), mat.ptr<T>(i)+mat.cols);
        }
    }
}

void steger_point_detection(cv::Mat src, cv::Mat& whiskerPoints, std::vector<std::pair<int16_t,int16_t>> &startPoints,
                            cv::Mat& whiskerAngles, c_extraction_opts cExtractionOpts, plotting_opts plottingOpts) {

//   cv::Mat rx = convolute(src, cExtractionOpts.gx);
    cv::Mat rx, ry, rxx, ryy, rxy;
    cv::filter2D(src, rx, cExtractionOpts.precision, cExtractionOpts.gx);
    cv::filter2D(src, ry, cExtractionOpts.precision, cExtractionOpts.gy);
    cv::filter2D(src, rxx, cExtractionOpts.precision, cExtractionOpts.gxx);
    cv::filter2D(src, ryy, cExtractionOpts.precision, cExtractionOpts.gyy);
    cv::filter2D(src, rxy, cExtractionOpts.precision, cExtractionOpts.gxy);
    cv::Mat egv = cv::Mat::zeros(rx.rows, rx.cols, cExtractionOpts.precision);
    whiskerAngles = cv::Mat::zeros(rx.rows, rx.cols, cExtractionOpts.precision);
    cStegerOriginal(whiskerPoints, egv, whiskerAngles, rx, ry, rxx, rxy, ryy, cExtractionOpts);

    std::vector<float> egvVec;
    convertMatToVector(egv, egvVec);
    sortStartPoints(startPoints, egvVec, src.cols, src.rows, cExtractionOpts.tr2);

    if (cExtractionOpts.plot) showImage(whiskerPoints, "Whisker points detected", plottingOpts.framing,false, plottingOpts);
}

void computeGaussianKernels(c_extraction_opts &cExtractionOpts, bool verbose) {
    Timer gaussians_timer;
    if (verbose) gaussians_timer.start();
    cv::Mat tDGauss = gaussian2D(cExtractionOpts.sigma, cExtractionOpts.gaussianKernelSize);
    cExtractionOpts.gx = derivativeXImg(tDGauss, 1);
    cExtractionOpts.gxx = derivativeXImg(tDGauss, 2);
    cExtractionOpts.gxy = derivativeYImg(derivativeXImg(tDGauss, 1), 1);
    cExtractionOpts.gy = derivativeYImg(tDGauss, 1);
    cExtractionOpts.gyy = derivativeYImg(tDGauss, 2);

    // split separable kernels
    cv::Mat gxxcol, gxxrow, gxycol, gxyrow, gycol, gyrow, gyycol, gyyrow;
    splitSeparableFilter(cExtractionOpts.gx, cExtractionOpts.gxCol, cExtractionOpts.gxRow);
    splitSeparableFilter(cExtractionOpts.gxx, cExtractionOpts.gxxCol, cExtractionOpts.gxxRow);
    splitSeparableFilter(cExtractionOpts.gxy, cExtractionOpts.gxyCol, cExtractionOpts.gxyRow);
    splitSeparableFilter(cExtractionOpts.gy, cExtractionOpts.gyCol, cExtractionOpts.gyRow);
    splitSeparableFilter(cExtractionOpts.gyy, cExtractionOpts.gyyCol, cExtractionOpts.gyyRow);
    if (verbose) {
        gaussians_timer.stop();
        std::cout << "Computing the gaussian kernels took " << gaussians_timer.seconds() << " seconds" << std::endl;
    }
}

// the original implementation of curvilinear point detection by C. Steger
void cStegerOriginal(cv::Mat& whiskerPoints, cv::Mat& egv, cv::Mat& whiskerAngles, cv::Mat rx, cv::Mat ry, cv::Mat a,
                     cv::Mat b, cv::Mat d, c_extraction_opts cExtractionOpts) {
    // Trace
    cv::Mat T = a + d;

    // Determinant
    cv::Mat b_sq; cv::pow(b, 2, b_sq);
    cv::Mat D; // = a*d; //+ b_sq;
    cv::multiply(a, d, D);
    D = D - b_sq;

    // Discriminant of equality
    cv::Mat T_sq; cv::multiply(T, T, T_sq);
    cv::Mat discr = T_sq/4 - D;

    // split real and imaginary parts into two matrices
    cv::Mat real = cv::max(discr, 0); // real values are non-negative
    cv::Mat im = cv::abs(cv::min(discr, 0)); // imaginary values are negative, take their abs value to compute square root
    cv::Mat discr_real; cv::sqrt(real, discr_real);
    cv::Mat discr_im; cv::sqrt(im, discr_im);

    // eigenvalues (complex)
    cv::Mat egv1_real = T/2 + discr_real;
    cv::Mat egv1_im = discr_im;
    cv::Mat egv2_real = T/2 - discr_real;
    cv::Mat egv2_im = - discr_im;

    // eigenvalues' magnitude for comparison
    cv::Mat egv1_abs; cv::magnitude(egv1_real, egv1_im, egv1_abs);
    cv::Mat egv2_abs; cv::magnitude(egv2_real, egv2_im, egv2_abs);

    // find largest eigenvalues
    cv::Mat egv1_inds = (egv1_abs > egv2_abs) / 255; egv1_inds.convertTo(egv1_inds, cExtractionOpts.precision);
    cv::Mat egv2_inds = (egv1_abs <= egv2_abs) / 255; egv2_inds.convertTo(egv2_inds, cExtractionOpts.precision);
    cv::Mat eig1; cv::multiply(egv1_real, egv1_inds, eig1);
    cv::Mat eig2; cv::multiply(egv2_real, egv2_inds, eig2);
    egv = eig1 + eig2;
    // ORing produces different results for some reason (and they seem worse)
//    cv::Mat eigtest = eig1 | eig2;
//    cv::Mat test; cv::absdiff(egv, eigtest, test);
//    showImage(test, "test difffs");

    cv::Mat ev1 = egv - d;                                                                                                  // first component of eigenvector. Second one is equal to b
    cv::Mat ev1_sq; cv::pow(ev1, 2, ev1_sq);
    cv::Mat n; cv::sqrt(ev1_sq + b_sq, n);                                                                                  // length of eigenvector (necessary for normalization)
    // normalize eigenvector
    ev1 = ev1 / n; cv::pow(ev1, 2, ev1_sq);
    b = b / n; cv::pow(b, 2, b_sq);

    // Calculate t
    cv::Mat t1; cv::multiply(rx, ev1, t1);
    cv::Mat t2; cv::multiply(ry, b, t2);
    cv::Mat t3; cv::multiply(a, ev1_sq, t3);
    cv::Mat t4; cv::multiply(2*b_sq, ev1, t4);
    cv::Mat t5; cv::multiply(d, b_sq, t5);
    cv::Mat t = - (t1 + t2) / (t3 + t4 + t5);

    // Calculate X and Y components of t
    cv::Mat xComponent_t; cv::multiply(t, ev1, xComponent_t);
    cv::Mat yComponent_t; cv::multiply(t, b, yComponent_t);

    // detect presence of a whisker
    whiskerPoints = (cv::abs(xComponent_t) < 0.5) & (cv::abs(yComponent_t) < 0.5) & (egv < 0);
    // set second derivative to zero at non-whisker points and compute whisker angles
    // also mask points above/below the snout
    for (int i = 0; i < egv.rows; i++) {
        for (int j = 0; j < egv.cols; j++) {
            float &egv_intensity = (float&) egv.ptr<float>(i)[j];
            float &wa_intensity = (float&) whiskerAngles.ptr<float>(i)[j];
            if (whiskerPoints.at<uchar>(i,j) == 0
                    || (!cExtractionOpts.upside_whiskers && j*cExtractionOpts.a_value + cExtractionOpts.b_value >= i)
                    || (cExtractionOpts.upside_whiskers && j*cExtractionOpts.a_value + cExtractionOpts.b_value <= i)) {
                egv_intensity = 0; // could be anything >= 0
                wa_intensity = FLT_MAX;
            }
            else {
                wa_intensity = atan2(yComponent_t.at<float>(i, j), xComponent_t.at<float>(i, j)) * 180.0 / CV_PI;
                // angles in the 3rd-4th quadrant are transferred to the 1st-2nd quadrant
                if (wa_intensity < 0) wa_intensity += 180.0;
            }
        }
    }
}

void sortStartPoints(std::vector<std::pair<int16_t,int16_t>> &startPoints, const std::vector<float> &egv,
                     int width, int height, float tr2) {
    std::vector<std::pair<float, std::complex<int16_t>>> startingPositions;
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            float value = egv[i*width+j];
            if (value < tr2) {
                startingPositions.push_back(std::make_pair(value, std::complex<int16_t>(i,j)));
            }
        }
    }
    std::sort(startingPositions.begin(), startingPositions.end(), sort_by_value);

    // store the points in the final array
    for (size_t i = 0; i < startingPositions.size(); i++) {
        startPoints.push_back(std::make_pair(startingPositions[i].second.real(), startingPositions[i].second.imag()));
    }
}

// Driver function to sort the vector elements by
// first element of pair in ascending order
bool sort_by_value(const std::pair<float, std::complex<int>> &a,
                   const std::pair<float, std::complex<int>> &b) {
    return (a.first > b.first);
}

