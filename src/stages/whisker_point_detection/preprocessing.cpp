/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "preprocessing.h"
#include "imgproc.h"
#include "../../cuda/preprocessing_cuda.h"

cv::Mat preprocessing(cv::Mat src, cv::Mat bgImage, cv::Mat bwShape, const preprocessing_opts pre_opts, plotting_opts plottingOpts) {

    // convert to grayscale
    cv::cvtColor(src, src, cv::COLOR_BGR2GRAY);

    cv::Mat whiteImage(src.rows, src.cols, src.type(), cv::Scalar(255));
    // invert colors if background is black and whiskers are white
    if (pre_opts.invertColors) {
        cv::subtract(whiteImage, src, src);
    }


    // perform deinterlacing if specified
    if (pre_opts.deinterlacingFlag) {
        // deinterlacing using line repetition
        deinterlaceLineRepImage(src);
    }

    cv::Mat whiskersBackgroundRemoved = bgImage - src;
    cv::Mat whiskersBackgroundAndAnimalRemoved;
    cv::multiply(whiskersBackgroundRemoved, bwShape, whiskersBackgroundAndAnimalRemoved);
    if (pre_opts.initialGaussianSigma > 0) {
        cv::GaussianBlur(whiskersBackgroundAndAnimalRemoved, whiskersBackgroundAndAnimalRemoved,
                         cv::Size(2*ceil(2*pre_opts.initialGaussianSigma)+1,
                                  2*ceil(2*pre_opts.initialGaussianSigma)+1),
                                  pre_opts.initialGaussianSigma, pre_opts.initialGaussianSigma);
    }
    cv::Mat preprocessedImage;
    imadjust(whiskersBackgroundAndAnimalRemoved, preprocessedImage,
             (double*) pre_opts.adjustRangeIn, (double*) pre_opts.adjustRangeOut, pre_opts.histEqGamma);

    cv::Mat lowlevel_img;
    cv::threshold(preprocessedImage, lowlevel_img, pre_opts.remove_lowlevels, 1, cv::THRESH_BINARY);
    // remove low levels of intensity from image
    cv::multiply(preprocessedImage, lowlevel_img, preprocessedImage);

    // remove edge of the frame (to remove timestamp, which is regarded as noise for our purpose)
    cv::Mat edgeRemovedImage = removeEdge(preprocessedImage, pre_opts.framing);

    cv::Mat upscaledImage;

    // very thin whiskers are difficult to detect by the Steger algorithm. Therefore, upscaling is necessary
    cv::resize(edgeRemovedImage, upscaledImage, cv::Size(lround(pre_opts.upscaling * src.cols),
                                                         lround(pre_opts.upscaling * src.rows)), 0, 0, cv::INTER_CUBIC);


    if (pre_opts.plot) {
        cv::Mat preprocessedInverse(upscaledImage.rows, upscaledImage.cols, CV_8U, cv::Scalar(255));
        for (int i = 0; i < preprocessedInverse.rows; i++) {
            for (int j = 0; j < preprocessedInverse.cols; j++) {
                preprocessedInverse.at<uchar>(i, j) = 255 - upscaledImage.at<uchar>(i, j);
            }
        }
        showImage(preprocessedInverse, "Frame after preprocessing", plottingOpts.framing, true, plottingOpts);
    }

    return upscaledImage;
}

cv::Mat getBackGroundImageAndShape(cv::VideoCapture video, cv::Mat &bwShape, preprocessing_opts *pre_opts,
                                   int startFrame, int endFrame) {

    int videoWidth = video.get(cv::CAP_PROP_FRAME_WIDTH);
    int videoHeight = video.get(cv::CAP_PROP_FRAME_HEIGHT);
    // initialize background image as a black image
    cv::Mat bgImage(cv::Size(videoWidth, videoHeight), CV_8UC1, cv::Scalar(0));

    // step over frames to perform background extraction
    int framesStep = (endFrame-startFrame) / 60;
    if (framesStep < 1) framesStep = 1;

    cv::Mat frame;
    for (int i=startFrame; i < endFrame; i += framesStep) {

        video.set(cv::CAP_PROP_POS_FRAMES, i);
        video >> frame;

        // If the frame is empty, break immediately
        if (frame.empty())
            break;

        // convert to grayscale
        cv::cvtColor(frame, frame, cv::COLOR_BGR2GRAY);

        cv::Mat whiteImage(frame.rows, frame.cols, frame.type(), cv::Scalar(255));
        // invert colors if background is black and whiskers are white
        if (pre_opts->invertColors) {
            cv::subtract(whiteImage, frame, frame);
        }

        if (pre_opts->deinterlacingFlag) {
            // deinterlacing using line repetition
            deinterlaceLineRepImage(frame);
        }

        // since the background is light, extract the brightest pixels
        bgImage = cv::max(bgImage, frame);

        if (i == startFrame) {
            // extract mouse silhouette once (mouse is not moving)
            bwShape = extractWhiskersGrayValuesWithMorphologyAndShave(frame, pre_opts->bwThreshold, pre_opts->dilate_value);
        }
    }
    // go to starting frame for the rest of the processing
    video.set(cv::CAP_PROP_POS_FRAMES , 0);

    return bgImage;
}

cv::Mat extractWhiskersGrayValuesWithMorphologyAndShave(cv::Mat frame, float bwThreshold, int razorSize) {
    cv::Mat BWtemp, bwShape, intermediate;
    // perform thresholding to the image
    cv::threshold(frame, BWtemp, bwThreshold, 255, cv::THRESH_BINARY);

    BWtemp = dilation(BWtemp, 2, 2);
    bwShape = erosion(BWtemp, lround(razorSize), 2);

    // assign all its non-black pixels to 1 to multiply this later with every frame
    cv::threshold(bwShape, bwShape, 0, 1, cv::THRESH_BINARY);

    return bwShape;
}


