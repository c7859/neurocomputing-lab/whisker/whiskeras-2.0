/*
    this file is part of whiskeras 2.0.

    whiskeras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the gnu general public license as published by
    the free software foundation, either version 3 of the license, or
    (at your option) any later version.

    whiskeras 2.0 is distributed in the hope that it will be useful,
    but without any warranty; without even the implied warranty of
    merchantability or fitness for a particular purpose.  see the
    gnu general public license for more details.

    you should have received a copy of the gnu general public license
    along with whiskeras 2.0.  if not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_C_EXTRACTION_H
#define FWTS_C_C_EXTRACTION_H

#include <opencv2/opencv.hpp>
#include "../../utils/plotting.h"

struct c_extraction_opts {
    float tr2 = 2;
    float sigma = 2.4;
    int gaussianKernelSize = 50;
    int precision = CV_32F;
    // snout position related
    bool upside_whiskers = false;
    float a_value;
    float b_value;
    bool plot = false;
    cv::Mat gx, gxCol, gxRow;
    cv::Mat gxx, gxxCol, gxxRow;
    cv::Mat gxy, gxyCol, gxyRow;
    cv::Mat gy, gyCol, gyRow;
    cv::Mat gyy, gyyCol, gyyRow;
    float *whiskerAngles;
    float *preprocessedImage;
    int verbose = 1;
};

void steger_point_detection(cv::Mat src, cv::Mat& whiskerPoints, std::vector<std::pair<int16_t,int16_t>> &startPoints,
                            cv::Mat& whiskerAngles, c_extraction_opts cExtractionOpts, plotting_opts plottingOpts);

void computeGaussianKernels(c_extraction_opts &cExtractionOpts, bool verbose);

void cStegerOriginal(cv::Mat& whiskerPoints, cv::Mat& egv, cv::Mat& whiskerAngles, cv::Mat rx, cv::Mat ry, cv::Mat a,
                     cv::Mat b, cv::Mat d, c_extraction_opts cExtractionOpts);

void sortStartPoints(std::vector<std::pair<int16_t,int16_t>> &startPoints, const std::vector<float> &egv,
                     int width, int height, float tr2);

bool sort_by_value(const std::pair<float, std::complex<int>> &a,
                   const std::pair<float, std::complex<int>> &b);

#endif //FWTS_C_C_EXTRACTION_H
