/*
    this file is part of whiskeras 2.0.

    whiskeras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the gnu general public license as published by
    the free software foundation, either version 3 of the license, or
    (at your option) any later version.

    whiskeras 2.0 is distributed in the hope that it will be useful,
    but without any warranty; without even the implied warranty of
    merchantability or fitness for a particular purpose.  see the
    gnu general public license for more details.

    you should have received a copy of the gnu general public license
    along with whiskeras 2.0.  if not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_IMGPROC_H
#define FWTS_C_IMGPROC_H
#include <opencv2/opencv.hpp>

void deinterlaceLineRepImage(cv::Mat frame);

cv::Mat dilation(cv::Mat frame, int dilation_size, int dilation_elem);

cv::Mat erosion(cv::Mat frame, int erosion_size, int erosion_elem);

void imadjust(const cv::Mat& src, cv::Mat& dst, double in[2], double out[2], float gamma);

bool invalidRange(double range);

double uchar2double(uchar value);

uchar double2uchar(double value);

cv::Mat removeEdge(cv::Mat src, int framing);

cv::Mat gaussian2D(float sigma, int ksize);

cv::Mat derivativeXImg(cv::Mat src, int degree);

cv::Mat derivativeYImg(cv::Mat src, int degree);

void splitSeparableFilter(cv::Mat sepFilter, cv::Mat& kcol, cv::Mat& krow);

#endif //FWTS_C_IMGPROC_H
