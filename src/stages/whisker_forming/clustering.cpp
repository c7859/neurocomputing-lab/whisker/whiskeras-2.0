/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "clustering.h"
#include <iostream>
#include "../whisker_point_detection/imgproc.h"
#include "../../utils/error.h"


void clustering_I(std::vector<std::pair<int16_t,int16_t>> startPoints, float *whiskerAngles, int &numOfPoints,
                  std::vector<std::pair<uint16_t, uint16_t>> &whiskerPositions, std::vector<int> &clusters,
                  clustering_opts clusteringOpts) {
    Timer t1, maskTimer, waTimer;
    t1.start();

    // perform clustering based on improved curve tracing by K. Raghupathy
    std::vector<int> IDX(clusteringOpts.height*clusteringOpts.width, -1);
    curveTracing(startPoints, whiskerPositions, clusters, IDX, whiskerAngles, clusteringOpts);
    numOfPoints = clusters.size();

    t1.stop();
    if (clusteringOpts.verbose > 0) std::cout << "\tClustering I took " << t1.seconds() << " seconds" << std::endl;

    if (clusteringOpts.verbose > 1) std::cout << "\tLocal Clustering: Number of clusters = " << clusters[clusters.size()-1] << std::endl;
}

template <typename T>
int clustering_II(std::vector<int> &clusters, std::vector<std::pair<uint16_t, uint16_t>> whiskerPositions,
        std::vector<float> &rotatedPositions, int numOfPoints, T *preprocessedImage, plotting_opts plottingOpts,
        clustering_opts clusteringOpts, stitch_I_opts stitchIOpts, stitch_II_opts stitchIIOpts, timers &timers) {
    Timer tt, tt1;
    if (clusteringOpts.verbose > 0) tt1.start();
    if (clusteringOpts.verbose > 1) tt.start();
    rotatedPositions.resize(2*numOfPoints);
    rotatedPositions.assign(2*numOfPoints, -1);
    rotatePositions(rotatedPositions, whiskerPositions, clusters, clusteringOpts);
    // Plot clusters
    if (clusteringOpts.plotPointsOnly) rotateAndPlotClusters(clusters, rotatedPositions, -1, -1, plottingOpts, false, "Whisker Points");
    if (clusteringOpts.plot) rotateAndPlotClusters(clusters, rotatedPositions, -1, -1, plottingOpts, true, "Initial Clusters");
    if (clusteringOpts.verbose > 1) tt.stop();
    if (clusteringOpts.verbose > 1) std::cout << "\tRotating positions I took " << tt.seconds() << " seconds" << std::endl;

    if (stitchIOpts.verbose > 0) tt.start();
    std::vector<int> clusterSize;
    std::vector<int> clusterPointsID;
    int numOfClusters = clusters[clusters.size()-1];
    Eigen::MatrixXf dots[numOfClusters];
    // STITCHING I
    numOfClusters = stitching_I(clusters, rotatedPositions, clusterSize, dots, clusterPointsID,
                                preprocessedImage, stitchIOpts, plottingOpts);
    if (numOfClusters == 0) {
        std::cout << "(Stitching I) 0 Clusters found." << std::endl;
        return 0;
    }
    if (stitchIOpts.verbose > 0) tt.stop();
    if (stitchIOpts.verbose > 0) std::cout << "\tStitching I took " << tt.seconds() << " seconds" << std::endl;
    timers.stitching_I_time += tt.seconds();
    if (stitchIIOpts.verbose > 1) std::cout << "\t\tNumber of clusters after stitching I is " << numOfClusters << std::endl;

    // Plot clusters
    if (stitchIOpts.plot) rotateAndPlotClusters(clusters, rotatedPositions, numOfClusters, -1, plottingOpts, true, "Clusters after Stitching I");
    for (int i = 0; stitchIOpts.separate_plot && i < numOfClusters; i++) {
        rotateAndPlotClusters(clusters, rotatedPositions, numOfClusters, i, plottingOpts, true, "Separate clusters after Stitching I");
    }

    if (stitchIIOpts.verbose > 0) tt.start();
    // STITCHING II
    numOfClusters = stitching_II(clusters, rotatedPositions, clusterSize, stitchIIOpts, plottingOpts);
    if (numOfClusters == 0) {
        std::cout << "(Stitching II) 0 Clusters found." << std::endl;
        return 0;
    }

    if (stitchIIOpts.verbose > 0) tt.stop();
    if (clusteringOpts.verbose > 0) tt1.stop();
    if (stitchIIOpts.verbose > 0) std::cout << "\tStitching II took " << tt.seconds() << " seconds" << std::endl;
    timers.stitching_II_time += tt.seconds();
    if (stitchIIOpts.verbose > 1) std::cout << "\t\tNumber of clusters after stitching II is " << numOfClusters << std::endl;
    if (clusteringOpts.verbose > 0) std::cout << "\tClustering II took " << tt1.seconds() << " seconds" << std::endl;
    // Plot clusters
    if (stitchIIOpts.plot) rotateAndPlotClusters(clusters, rotatedPositions, numOfClusters, -1, plottingOpts, true, "Clusters after Stitching II");
    for (int i = 0; stitchIIOpts.separate_plot && i < numOfClusters; i++) {
        rotateAndPlotClusters(clusters, rotatedPositions, numOfClusters, i, plottingOpts, true, "Separate clusters after Stitching II");
    }
    return numOfClusters;
}

template int clustering_II<float>(std::vector<int> &clusters, std::vector<std::pair<uint16_t, uint16_t>> whiskerPositions,
                   std::vector<float> &rotatedPositions, int numOfPoints, float *preprocessedImage, plotting_opts plottingOpts,
                   clustering_opts clusteringOpts, stitch_I_opts stitchIOpts, stitch_II_opts stitchIIOpts, timers &timers);

template int clustering_II<uchar>(std::vector<int> &clusters, std::vector<std::pair<uint16_t, uint16_t>> whiskerPositions,
                                   std::vector<float> &rotatedPositions, int numOfPoints, uchar *preprocessedImage, plotting_opts plottingOpts,
                                   clustering_opts clusteringOpts, stitch_I_opts stitchIOpts, stitch_II_opts stitchIIOpts, timers &timers);

void rotatePositions(std::vector<float> &rotatedPositions, std::vector<std::pair<uint16_t,uint16_t>> whiskerPositions,
        std::vector<int> &clusters, clustering_opts clusteringOpts) {
    std::vector<int> clustersBelowSnout(clusters.size(), -1);
    int i = 0;
    for (size_t ID = 0; ID < clusters.size(); ID++) {
        int origX = whiskerPositions[ID].first  - clusteringOpts.origin[0];
        int origY = whiskerPositions[ID].second - clusteringOpts.origin[1];

        // shift and rotate whiskers, so that the line along the snout + offset is defined as the x-axis. Everything under this x-axis will be discarded.
        float rotX = clusteringOpts.sbase[0][0] * origX +
                                  clusteringOpts.sbase[0][1] * origY;
        float rotY = clusteringOpts.sbase[1][0] * origX +
                                      clusteringOpts.sbase[1][1] * origY - clusteringOpts.cutoff;

        // Discard all whisker pixels under the x axis
        if (rotY > 0) {
           rotatedPositions[2*i] = rotX;
           rotatedPositions[2*i+1] = rotY;
           clustersBelowSnout[i] = clusters[ID];
           i++;
        }
    }
    clustersBelowSnout.resize(i);
    rotatedPositions.resize(2*i);
    clusters = clustersBelowSnout;
}

void curveTracing(std::vector<std::pair<int16_t,int16_t>> startPoints,
                  std::vector<std::pair<uint16_t, uint16_t>> &whiskerPositions, std::vector<int> &clusters,
                  std::vector<int> &IDX, float *whiskerAngles, clustering_opts clusteringOpts) {
    int idx = 0; // initialize cluster number
    // locate point with minimum second derivative
    int minIdx = startPoints.size()-1;
    cv::Point currPoint = cv::Point(startPoints[minIdx].second, startPoints[minIdx].first);
    while (true) {
//        statsTimer.start();
        float angle = whiskerAngles[currPoint.y*clusteringOpts.width+currPoint.x];
        IDX[currPoint.y*clusteringOpts.width+currPoint.x] = idx;
        // store point's cluster and position
        whiskerPositions.push_back(std::make_pair(currPoint.x, currPoint.y));
        clusters.push_back(idx);
        // array to store the last M points' angles from which we take an average
        std::vector<float> angle_stats_up; // from upwards extension
        std::vector<float> angle_stats_down; // from downwards extension
        angle_stats_up.push_back(angle);
        angle_stats_down.push_back(angle);
        float averageAngleDown = angle;
        float averageAngleUp = angle;
        int prevSizeUp = 1, prevSizeDown = 1;

        cv::Point lastPointUp = currPoint;
        cv::Point lastPointDown = currPoint;
        float lastAngleUp = angle;
        float lastAngleDown = angle;
        int clusterSize = 1;
        extensionSide chosenSide = up;
        // perform improved curve tracing in two steps: i) steger linking, ii) curve following
        tracingStep step = stegerLinking;
        while (true) {
            cv::Point choice(-1,-1); // initialize the neighbour choice as none

            // define boundaries for neighbour searching
            bool diag = false;
            int x_b[2];
            int y_b[2];
            defineSearchBounds(x_b, y_b, diag, angle, clusteringOpts.L);

            bool rejectPointFlag = false;
            scanNeighbours(choice, rejectPointFlag, diag, x_b, y_b, angle, chosenSide, currPoint,
                           whiskerAngles,IDX, whiskerPositions, clusters, clusteringOpts);

            // if the point was rejected as noise, proceed to next point
            if (rejectPointFlag) {
                idx = idx + 1;
                break;
            }

            if (step == curveFollowing && choice.y == -1 && clusterSize > clusteringOpts.minClusterSizeToExtend) {
                float averageAngle = (chosenSide == up) ? averageAngleUp : averageAngleDown;
                lookFurther(currPoint, choice, averageAngle, clusteringOpts.K, IDX,
                            whiskerAngles, chosenSide, clusteringOpts, true);
            }

            // if a neighbour was found, continue linking from next point
            if (choice.y != -1) {
                IDX[choice.y*clusteringOpts.width+choice.x] = idx;
                // store point's cluster and position
                whiskerPositions.push_back(std::make_pair(currPoint.x, currPoint.y));
                clusters.push_back(idx);
                currPoint = choice;
                // add new point's angle and intensity to the list of last points' angles
                float new_angle = whiskerAngles[choice.y*clusteringOpts.width + choice.x];
                angle = new_angle;
                float old_angle = (chosenSide == up) ? angle_stats_up[0] : angle_stats_down[0];
                if (chosenSide == up) {
                    updateAngleList(averageAngleUp, new_angle, clusteringOpts.M, angle_stats_up);
                    averageAngleUp = computeAverageAngle(averageAngleUp, angle_stats_up, prevSizeUp, old_angle, clusteringOpts.M);
                }
                else {
                    updateAngleList(averageAngleDown, new_angle, clusteringOpts.M, angle_stats_down);
                    averageAngleDown = computeAverageAngle(averageAngleDown, angle_stats_down, prevSizeDown, old_angle, clusteringOpts.M);
                }
                clusterSize++;
            }
            // if no neighbour was found
            else if (step == stegerLinking) {
                // extend downwards too
                if (chosenSide == up) {
                    chosenSide = down;
                    cv::Point lastPoint = currPoint;
                    currPoint = lastPointUp;
                    lastPointUp = lastPoint;
                    lastAngleUp = angle;
                    angle = lastAngleDown;
                }
                // done with extending by Steger Linking, proceed with Curve Following
                else {
                    chosenSide = up;
                    lastPointDown = currPoint;
                    lastAngleDown = angle;
                    currPoint = lastPointUp;
                    angle = lastAngleUp;
                    step = curveFollowing;
                }
            } else { // curve following step
                // follow curves downwards too
                if (chosenSide == up) {
                    chosenSide = down;
                    currPoint = lastPointDown;
                    angle = lastAngleDown;
                }
                    // done with both steps, finished with this cluster
                else {
                    idx++;
                    break;
                }
            }
        }
        // remove processed starting points from the list
        startPoints.pop_back(); // remove the last point that was extended
        minIdx = startPoints.size()-1;
        while (minIdx >= 0 && IDX[startPoints[minIdx].first*clusteringOpts.width+startPoints[minIdx].second] != -1) {
            startPoints.pop_back();
            minIdx--;
        }
        if (minIdx < 0) {
            break; // when the list of starting points is finished exit loop
        }
        // locate the next point to start extending
        currPoint = cv::Point(startPoints[minIdx].second, startPoints[minIdx].first);
    }
}

void defineSearchBounds(int x_b[], int y_b[], bool &diag, float angle, int L) {
    if (angle < 22.5 || angle > 157.5) {
        // [1 1 1]
        // [0 0 0]
        // [1 1 1]
        x_b[0] = -1; x_b[1] = 1;
        y_b[0] = -L; y_b[1] = -1;
    }
    else if (angle > 22.5 && angle < 67.5) {
        // [0 1 1]
        // [1 0 1]
        // [1 1 0]
        x_b[0] = 0; x_b[1] = L;
        y_b[0] = -L; y_b[1] = 0;
        diag = true;
    }
    else if (angle > 112.5 && angle < 157.5) {
        // [1 1 0]
        // [1 0 1]
        // [0 1 1]
        x_b[0] = -L; x_b[1] = 0;
        y_b[0] = -L; y_b[1] = 0;
        diag = true;
    }
    else {
        // [1 0 1]
        // [1 0 1]
        // [1 0 1]
        x_b[0] = -L; x_b[1] = -1;
        y_b[0] = -1; y_b[1] = 1;
    }
}

inline void scanNeighbours(cv::Point& choice, bool &rejectPointFlag, bool diag, int x_b[], int y_b[], float angle,
                    extensionSide chosenSide, cv::Point currPoint, float *whiskerAngles,
                    std::vector<int> &IDX, std::vector<std::pair<uint16_t, uint16_t>> &whiskerPositions,
                    std::vector<int> &clusters, clustering_opts clusteringOpts) {
    int k; int l;
    float dist = FLT_MAX;
    for (int i = y_b[0]; i <= y_b[1]; i++) {
        k = (chosenSide == up) ? i : -i; // bounds are reversed if we extend downwards
        for (int j = x_b[0]; j <= x_b[1]; j++) {
            l = (chosenSide == up) ? j : -j; // bounds are reversed if we extend downwards
            if (currPoint.y+k >= clusteringOpts.height || currPoint.x+l >= clusteringOpts.width // check boundaries
                || currPoint.y+k < 0 || currPoint.x+l < 0
                || (k == 0 && l == 0)
                || (diag && abs(abs(i) - abs(j)) > 1) ) { // force diagonal search if in diagonal direction
                continue;
            }
            float pdist = sqrt(k*k+l*l);
            float angle2 = whiskerAngles[(currPoint.y+k)*clusteringOpts.width + (currPoint.x+l)];
            if (angle2 > 360) continue; // this point has either been processed or not a whisker point
            // make sure angle difference is at most 90 deg.
            float angle_diff = abs(angle2-angle);
            if (angle_diff > 90) angle_diff = 180-angle_diff;
            // reject neighbour if it has a large angle diff
            if (angle_diff > clusteringOpts.maxAngle) continue;
            // reject points of a line that is more than 1-pixel wide
            if (IDX[(currPoint.y+k)*clusteringOpts.width + (currPoint.x+l)] != -1) {
                // remove point
                IDX[currPoint.y*clusteringOpts.width + currPoint.x] = -2;
//                whiskerAngles[currPoint.y*clusteringOpts.width + currPoint.x] = FLT_MAX;
                whiskerPositions.pop_back();
                clusters.pop_back();
                rejectPointFlag = true;
                break;
            }
            // if there is a valid whisker point there compute a score
            float dist_local = pdist + clusteringOpts.steger_c * angle_diff;
            if (dist_local < dist) {
                dist = dist_local;
                choice.y = currPoint.y + k;
                choice.x = currPoint.x + l;
            }
        }
        if (rejectPointFlag) break;
    }
}

inline float computeAverageAngle(float prev_averageAngle, std::vector<float> &angle_stats, int &prevSize,
                          float old_angle, int M) {
    // compute new average by adjusting the old one -- this is a bit faster
    float averageAngle = prev_averageAngle * (float) prevSize;
    if (prevSize == M) averageAngle -= old_angle; // subtract oldest sample if it was removed from list
    averageAngle = (averageAngle + angle_stats[angle_stats.size()-1]) / angle_stats.size();
    prevSize = angle_stats.size();

    return averageAngle;
}

inline void updateAngleList(float averageAngle, float new_angle, size_t M, std::vector<float> &angle_stats) {
    // because there is a discontinuity in the angles in which we are looking for neighbours,
    // we have to fix the new angle for consistency
    if (abs(averageAngle-new_angle) > 90 && averageAngle < new_angle) new_angle = new_angle - 180;
    else if (abs(averageAngle-new_angle) > 90 && averageAngle > new_angle) new_angle = new_angle + 180;

    // if the list is already has M points, erase the oldest one
    if (angle_stats.size() == M) {
        angle_stats.erase(angle_stats.begin());
    }
    angle_stats.push_back(new_angle);
}

void lookFurther(cv::Point currPoint, cv::Point& choice, float averageAngle, int K, std::vector<int> &IDX,
                 const float *whiskerAngles, extensionSide chosenSide, clustering_opts clusteringOpts, bool noiseCheck) {
    // restrict averageAngle in 1st-2nd quadrants
    if (averageAngle > 180) averageAngle -= 180;
    else if (averageAngle < 0) averageAngle += 180;

    float searchAngle = averageAngle;
    // choose which direction to search depending on the side
    if (chosenSide == up && averageAngle > 67.5) searchAngle = averageAngle - 180; // force the search upwards and left
    else if (chosenSide == down && averageAngle < 67.5) searchAngle = averageAngle + 180; // force the search downwards and right
    // scan in the direction of the average angle for neighbours
    float dist = FLT_MAX;
    for (int i = 0; i < K; i++) {
        int k = -cos(searchAngle*CV_PI/180)*i + currPoint.y;
        int l = sin(searchAngle*CV_PI/180)*i + currPoint.x;
        for (int y = floor(k); y <= ceil(k); y++) {
            for (int x = floor(l); x <= ceil(l); x++) {
                if (y >= clusteringOpts.height || x >= clusteringOpts.width || y < 0 || x < 0) break;
                if ((currPoint.y == y && currPoint.x == x) || IDX[y*clusteringOpts.width+x] != -1) // if it's already classified
                continue;

                float angle2 = whiskerAngles[y*clusteringOpts.width+x];
                if (angle2 > 360) continue; // not a whisker point
                // make sure angle diff is at most 90 deg.
                float angle_diff = abs(angle2-averageAngle);
                if (angle_diff > 180) angle_diff -= 180;
                else if (angle_diff < 0) angle_diff += 180;
                if (angle_diff > 90) {
                    angle_diff = 180 - angle_diff;
                }

                if (angle_diff < clusteringOpts.maxAngle) {
                    float pdist = sqrt((x-currPoint.x)*(x-currPoint.x)+(y-currPoint.y)*(y-currPoint.y));
                    float dist_local = pdist + clusteringOpts.steger_c*angle_diff;
                    if (dist_local < dist) {
                        dist = dist_local;
                        choice.y = y; choice.x = x;
                    }
                }
            }
        }
    }
    // check that the new point is not isolated (thus not noise)
    int N = 1;
    if (choice.y != -1 && noiseCheck) {
        cv::Point currChoice = choice;
        for (int i = 0; i < N; i++) {
            cv::Point next_choice = cv::Point(-1, -1);
            lookFurther(currChoice, next_choice, averageAngle, 2, IDX, whiskerAngles,
                        chosenSide, clusteringOpts, false);
            if (next_choice.y != -1) {
                choice = cv::Point(-1, -1);
                break;
            }
            currChoice = next_choice;
        }
    }
}

