/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_PARAMETRIZATION_H
#define FWTS_C_PARAMETRIZATION_H
#include "../../../libs/eigen/Eigen/Dense"
#include <vector>
#include <opencv2/core/cvdef.h>
#include "stitching_I.h"
#include "clustering.h"
#include "whisker_forming_options.h"
#include "../../utils/plotting.h"

struct param_opts {
    // Clusters with fewer than <param_opt.minNrOfDots> datapoints will be ignored.
    int minNrOfDots = 40;
    // Clusters with the distance between tip and bottom smaller than <param_opt.minLength> will be ignored.
    int minLength = 340;
    // Parametrizations that do not comply with these requirements will be ignored.
    float minAngleToSnout = 2 * CV_PI / 180;
    float maxBend = 0.01;
    float absMaxX;
    std::string wgtfun = "andrews";
    float maxMSE = 120;
    int verbose = 1;
    bool plot = false;
    bool separate_plot = false;

    int numDetectedWhiskers = 0; // to keep track of the number of detected whiskers to display
};

struct nlinfit_options {
    int MaxIter = 50;
    float TolFun = 1e-04;
    float TolX = 1e-04;
    float DerivStep = 6.055e-06;
//    float DerivStep = 6.055e-03;
    float Tune = 1.3390;
//    std::string wgtfun = "andrews";
    Eigen::ArrayXf (*wgtfun)(Eigen::ArrayXf);
//    std::string wgtfun = "bisquare";
    float delta_threshold = 10e-10;
    int verbose = 1;
};

int clusters2whiskers(std::vector<struct whisker> &parametrizations, std::vector<int> &clusters, std::vector<float> &dots,
                      param_opts &paramOpts, plotting_opts plottingOpts);

void fitWhiskers(Eigen::MatrixXf *clusterPoints, std::vector<int> &clusterSize, param_opts paramOpts,
                 std::vector<struct whisker> &parametrizations, std::vector<bool> &valid_clusters, int numOfClusters);

float nlinfit(Eigen::MatrixXf X, Eigen::VectorXf y, Eigen::Vector3f &beta, int *success,
              Eigen::VectorXf (*model)(Eigen::MatrixXf, Eigen::Vector3f), param_opts paramOpts);

int LMfit(Eigen::MatrixXf &J, Eigen::MatrixXf X, Eigen::VectorXf y, Eigen::Vector3f &beta,
          Eigen::VectorXf (*model)(Eigen::MatrixXf, Eigen::Vector3f),Eigen::ArrayXf weights, nlinfit_options options);

inline Eigen::MatrixXf getjacobian(Eigen::Vector3f theta, float fdiffstep, Eigen::MatrixXf X, Eigen::VectorXf y0,
                            Eigen::VectorXf (*model)(Eigen::MatrixXf, Eigen::Vector3f), Eigen::ArrayXf sweights, float delta_threshold);

inline float LM_step(Eigen::Vector3f &beta, Eigen::VectorXf &yfit, Eigen::VectorXf (*model)(Eigen::MatrixXf, Eigen::Vector3f),
                 Eigen::VectorXf &step, Eigen::VectorXf &r, Eigen::VectorXf &rplus, Eigen::MatrixXf J, Eigen::Vector3f diagJtJ,
                 float lambda, Eigen::MatrixXf X, Eigen::VectorXf y, Eigen::ArrayXf sweights, bool lambda_increased);

float nlrobustfit(Eigen::MatrixXf X, Eigen::VectorXf y, Eigen::Vector3f &beta, Eigen::MatrixXf &J,
                 Eigen::VectorXf (*model)(Eigen::MatrixXf, Eigen::Vector3f), float ols_s, nlinfit_options options);

Eigen::VectorXf optimization_func(Eigen::MatrixXf X, Eigen::Vector3f beta);

inline float madsigma(Eigen::ArrayXf r, int size);

inline Eigen::ArrayXf andrews(Eigen::ArrayXf r);

inline Eigen::ArrayXf bisquare(Eigen::ArrayXf r);

inline Eigen::ArrayXf talwar(Eigen::ArrayXf r);

float whisker_function(float x, float shape);

template<typename value_type, typename function_type>
inline value_type integral(const value_type a,
                           const value_type b,
                           const value_type shape,
                           const value_type tol,
                           function_type func(value_type, value_type)) {
    unsigned n = 1U;

    value_type h = (b - a);
    value_type I = (func(a, shape) + func(b, shape)) * (h / 2);

    for(unsigned k = 0U; k < 8U; k++)
    {
        h /= 2;

        value_type sum(0);
        for(unsigned j = 1U; j <= n; j++)
        {
            sum += func(a + (value_type((j * 2) - 1) * h), shape);
        }

        const value_type I0 = I;
        I = (I / 2) + (h * sum);

        const value_type ratio     = I0 / I;
        const value_type delta     = ratio - 1;
        const value_type delta_abs = ((delta < 0) ? -delta : delta);

        if((k > 1U) && (delta_abs < tol))
        {
            break;
        }

        n *= 2U;
    }

    return I;
}

float statrobustsigma(Eigen::ArrayXf r, float s, Eigen::ArrayXf h, int p, nlinfit_options options);

void depthTransform(whisker &whisker, float y_min, float D);

void removeRedundantWhiskers(std::vector<struct whisker> &parametrizations, std::vector<int> &clusterSize,
                             std::vector<bool> &valid_clusters, int numOfClusters);

#endif //FWTS_C_PARAMETRIZATION_H
