/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stitching_I.h"
#include <iostream>
#include "../../utils/error.h"
#include "stitching_II.h"
#include "../../utils/Timer.hpp"

template <typename T>
int stitching_I(std::vector<int> &clusters, std::vector<float> &rotatedPositions, std::vector<int> &clusterSize,
                Eigen::MatrixXf *dots, std::vector<int> &clusterPointsID, T *preprocessedImage,
                stitch_I_opts stitchIOpts, plotting_opts plottingOpts) {
    int maxPointsOfCluster, numOfPoints;
    clusterSize.resize(clusters.size());
    std::fill(clusterSize.begin(), clusterSize.end(), 0);
    int numOfClusters = filterNoisyClusters(clusters, rotatedPositions, clusterSize, &maxPointsOfCluster,
                                            stitchIOpts.minClusterSize); // filter out clusters with few points as noise
    if (numOfClusters == 0) return 0;
    numOfPoints = clusters.size();

    std::vector<bool> removedClusters(numOfClusters, false);
    Eigen::MatrixXf clusterPoints[numOfClusters];
    for (int i = 0; i < numOfClusters; i++) {
        clusterPoints[i].resize(2, maxPointsOfCluster);
    }
    clusterPointsID.resize(numOfClusters * numOfPoints);
    clusterSize.resize(numOfClusters);
    std::fill(clusterSize.begin(), clusterSize.end(), 0);
    for (int i = 0; i < numOfPoints; i++) {
        clusterPointsID[clusters[i] * numOfPoints + clusterSize[clusters[i]]] = i;
        clusterPoints[clusters[i]](0, clusterSize[clusters[i]]) = rotatedPositions[2 * i];
        clusterPoints[clusters[i]](1, clusterSize[clusters[i]]) = rotatedPositions[2 * i + 1];
        clusterSize[clusters[i]]++;
    }

    // resize clusterPoints matrix to fit each cluster's size
    for (int i = 0; i < numOfClusters; i++) {
        dots[i] = clusterPoints[i].block(0, 0, 2, clusterSize[i]);
    }
    cluster_tips whisker_clusters[numOfClusters];
    std::vector<bool> increasedClusters(numOfClusters, true);
    std::vector<bool> clustersToFilter(numOfClusters, false);


    // count the number of merging loops
    int times = 0;
    // first round is to stitch close chunks of whiskers
    // second round is to stitch interrupted whiskers
    while (true){
        // compute rotation data for every cluster
        getRotationData(whisker_clusters, dots, increasedClusters, clusterSize, removedClusters, numOfClusters, stitchIOpts);
        increasedClusters.assign(numOfClusters, false);
        // matrix to keep track of "similarity" between clusters, initialized to FLT_MAX (not similar)
        Eigen::MatrixXf scores(numOfClusters, numOfClusters);
        scores.fill(FLT_MAX);

        getStitchingScores(scores, whisker_clusters, dots, removedClusters, numOfClusters, preprocessedImage, stitchIOpts, plottingOpts);

        // if no other stitchings can take place, exit loop
        Eigen::MatrixXf::Index minRow, minCol;
        float minVal = scores.minCoeff(&minRow, &minCol);
        if (minVal > 100000) break;

        // find pairs of clusters with the smallest distance (greatest similarity) and merge them
        mergeClusters(clusters, scores, clusterSize, dots, clusterPointsID, &numOfClusters,
                      &maxPointsOfCluster, removedClusters, increasedClusters);

        times++;
    }

    if (stitchIOpts.verbose > 1) std::cout << "\tStitching I: " << times << " loops of merging." << std::endl;
    removeClusters(clusters, rotatedPositions, clusterSize, removedClusters, &numOfClusters);
    clusterSize.resize(numOfClusters);
    return numOfClusters;
}

template int stitching_I<float>(std::vector<int> &clusters, std::vector<float> &rotatedPositions, std::vector<int> &clusterSize,
                Eigen::MatrixXf *dots, std::vector<int> &clusterPointsID, float *preprocessedImage,
                stitch_I_opts stitchIOpts, plotting_opts plottingOpts);

template int stitching_I<uchar>(std::vector<int> &clusters, std::vector<float> &rotatedPositions, std::vector<int> &clusterSize,
                                Eigen::MatrixXf *dots, std::vector<int> &clusterPointsID, uchar *preprocessedImage,
                                stitch_I_opts stitchIOpts, plotting_opts plottingOpts);


int filterNoisyClusters(std::vector<int> &clusters, std::vector<float> &rotatedPositions, std::vector<int> clusterSize,
                        int *maxPointsOfCluster, int minClusterSize) {
//    std::vector<int> clusterSize(clusters.size(), 0);
    int numOfClusters = 0;
    if (clusterSize[0] == 0) {
        *maxPointsOfCluster = 0;
        // count the number of points in each cluster
        for (int cluster : clusters) {
            clusterSize[cluster]++;
            if ((cluster + 1) > numOfClusters) numOfClusters = cluster + 1;
            if (*maxPointsOfCluster < clusterSize[cluster])
                *maxPointsOfCluster = clusterSize[cluster]; // store the max number of points in clusters
        }
    }
    // this case is for stitching II.
    else {
        *maxPointsOfCluster = *std::max_element(clusterSize.begin(), clusterSize.end());
        numOfClusters = clusterSize.size();
    }
    // determine how many clusters are skipped as noise
    std::vector<int> skipped_clusters(numOfClusters + 1); // array to hold number of skipped clusters
    skipped_clusters[0] = 0;
    for (int i = 1; i < numOfClusters + 1; i++) {
        skipped_clusters[i] = skipped_clusters[i - 1];
        if (clusterSize[i - 1] < minClusterSize) skipped_clusters[i]++;
    }

    // filter out the clusters with few points as noise
    int j = 0;
    for (size_t i = 0; i < clusters.size(); i++) {
        if (clusterSize[clusters[i]] >= minClusterSize) {
            clusters[j] = clusters[i] - skipped_clusters[clusters[i]]; // copy valid cluster points
            rotatedPositions[2 * j] = rotatedPositions[2 * i]; // copy x coord
            rotatedPositions[2 * j + 1] = rotatedPositions[2 * i + 1]; // copy y coord
            j++;
        }
    }
    clusters.resize(j);
    rotatedPositions.resize(2 * j);
//    plotPositions.resize(j);

    numOfClusters -= skipped_clusters[numOfClusters];
//    std::cout << "number of clusters is " << numOfClusters << std::endl;
    return numOfClusters;                                                                                                               // returns number of clusters
}

void getRotationData(cluster_tips *whisker_clusters, Eigen::MatrixXf *dots, const std::vector<bool> increasedClusters,
                     const std::vector<int> clusterSize, const std::vector<bool> removedClusters, int numOfClusters,
                     stitch_I_opts stitchIOpts) {
#ifdef USE_OPENMP
    omp_set_num_threads(8);
#pragma omp parallel for
#endif
    for (int i = 0; i < numOfClusters; i++) {
        // if cluster has been removed or not increased, do not compute its rotation data again.
        if (removedClusters[i] || !increasedClusters[i]) continue;
        // Linearize tip of clusters with lengthOfTipAndBottom to define axis p
        whisker_clusters[i] = rotationData(dots[i], clusterSize[i], stitchIOpts.lengthOfTipAndBottom, edges);
    }
}

template <typename T>
void getStitchingScores(Eigen::MatrixXf &scores, const cluster_tips *whisker_clusters, const Eigen::MatrixXf *dots,
                        const std::vector<bool> removedClusters, int numOfClusters, T *preprocessedImage,
                        stitch_I_opts stitchIOpts, plotting_opts plottingOpts) {
#ifdef USE_OPENMP
    omp_set_num_threads(8);
#pragma omp parallel for
#endif
    for (int q = 0; q < numOfClusters; q++) {
        if (removedClusters[q] == true) continue;
        for (int r = 0; r < numOfClusters; r++) {
            if (removedClusters[r] == true) continue;
            if (q == r || whisker_clusters[r].isEmpty_bottom || whisker_clusters[q].isEmpty_tip) continue;

            // distance between bottom point of cluster r and tip of cluster q
            Eigen::Vector2f bp_rot_r = whisker_clusters[q].R_tip * (whisker_clusters[r].beginPoint -
                                                                    whisker_clusters[q].endPoint);
            // also compute distance backwards because d_y may be significantly different
            Eigen::Vector2f bp_rot_q = whisker_clusters[r].R_bottom * (whisker_clusters[r].beginPoint -
                                                                       whisker_clusters[q].endPoint);

            // if the tip is further to the snout than the bottom then skip
            if (bp_rot_r(0) < -5) continue;

            float d_x = abs(bp_rot_r(0));
            // because edge whisker points are often noisy due to Steger's algorithm,
            // take d_y to be the least of them, to minimize rejections later
            float d_y = std::min(abs(bp_rot_r(1)), abs(bp_rot_q(1)));

            // if distance is too large skip cluster r
            float beta = abs(whisker_clusters[r].angle_bottom - whisker_clusters[q].angle_tip);

            // debugging
//            Eigen::MatrixXf plottingDots[2];
//            plottingDots[0] = dots[q];
//            plottingDots[1] = dots[r];
//            plotMultipleClusters(plottingDots, 2, plottingOpts, "Dots");

            // reject stitchings between whisker parts that point very differently
            // BUT, sometimes the angle is distorted near intersections, so don't reject for nearby parts
            if (beta > stitchIOpts.maxAD && (d_x > 3 || d_y > 2))
                continue;

            float d = sqrtf(d_x*d_x+d_y*d_y);

            // reject stitching if the part to stitch is outside cone
            // that starts wide (due to noise) and becomes thinner linearly with distance up until coneMinDist
            float cone = std::max(stitchIOpts.coneMin, (stitchIOpts.coneMin - stitchIOpts.coneMax)/stitchIOpts.coneMinDist * d  + stitchIOpts.coneMax);
            // don't reject for small distances
            if (d_y > d * sin(cone) && d_y > 2)
                continue;

            // reject stitching if there is no valid line between the whisker parts
            if (!radonTransformCheck(whisker_clusters, dots, q, r, preprocessedImage, stitchIOpts, plottingOpts))
                continue;

            // take the average of the two distances to compute score
            d_x = (abs(bp_rot_r(0)) + abs(bp_rot_q(0))) / 2;
            d_y = (abs(bp_rot_r(1)) + abs(bp_rot_q(1))) / 2;

            // weight d_y more the less the d_x and use the high value of stitchIOpts.c only when d is
            // non-negligible, that is because very close points are disconnected due to noise in intersections
            float c = stitchIOpts.c;
            float w_y = stitchIOpts.c/d_x;
//            w_y = 1000/d_x;
            if (d < 5) {
                c = 1;
                w_y = 1;
            }
            scores(q,r) = d_x + w_y * d_y + c * beta;
        }
    }
}

template void getStitchingScores<float>(Eigen::MatrixXf &scores, const cluster_tips *whisker_clusters, const Eigen::MatrixXf *dots,
                        const std::vector<bool> removedClusters, int numOfClusters, float *preprocessedImage,
                        stitch_I_opts stitchIOpts, plotting_opts plottingOpts);

template void getStitchingScores<uchar>(Eigen::MatrixXf &scores, const cluster_tips *whisker_clusters, const Eigen::MatrixXf *dots,
                                        const std::vector<bool> removedClusters, int numOfClusters, uchar *preprocessedImage,
                                        stitch_I_opts stitchIOpts, plotting_opts plottingOpts);

/**
 * @param   cluster points
 * @param   length Lmin
 * @returns rotation matrix, begin point, end point, length and angle of a cluster
 *
 */
cluster_tips rotationData(const Eigen::MatrixXf dots, int cluster_size, int lengthOfTipAndBottom, pointsSelection whiskerPart) {
    xassert(cluster_size > 1, "rotationData: cluster size must be > 1. Exiting");
    cluster_tips tips;

    Eigen::MatrixXf covMat = Covariance(dots, cluster_size);
    Eigen::MatrixXf R_init = RotationMatrix(covMat);

    Eigen::MatrixXf rotatedDots(2, cluster_size);
    rotatedDots = R_init * dots; // Rotate all the cluster

    Eigen::MatrixXf::Index minIdx, maxIdx, placeholder;
    rotatedDots.row(0).maxCoeff(&placeholder, &maxIdx);
    rotatedDots.row(0).minCoeff(&placeholder, &minIdx);
    tips.endPoint = dots.col(maxIdx);
    tips.beginPoint = dots.col(minIdx);
    tips.length = (tips.endPoint - tips.beginPoint).norm();

    tips.R_general = R_init;
    if (whiskerPart != general) {
    AlignToTipAndBottom(tips, R_init, dots, covMat, cluster_size, lengthOfTipAndBottom, whiskerPart);
}
xassert(tips.R_general(0, 0) >= -1 && tips.R_general(0, 0) <= 1,
            "(parametrization) Rotation matrix element R[0][0] is not in [-1, 1] which can potentially cause a problem with the angle. Exiting.");
tips.angle_general = acos(tips.R_general(0, 0));
//        tips.angle_general = atan2(tips.R_general(0, 1), tips.R_general(0, 0));
if ((whiskerPart & 1) && !tips.isEmpty_tip) {
    xassert(tips.R_tip(0, 0) >= -1 && tips.R_tip(0, 0) <= 1,
            "(stiching I) Rotation matrix element R[0][0] is not in [-1, 1] which can potentially cause a problem with the angle. Exiting.");
    tips.angle_tip = acos(tips.R_tip(0, 0));
}

if ((whiskerPart & 2) && !tips.isEmpty_bottom) {
    xassert(tips.R_bottom(0, 0) >= -1 && tips.R_bottom(0, 0) <= 1,
            "(stiching) Rotation matrix element R[0][0] is not in [-1, 1] which can cause a problem with the angle. Exiting.");
    tips.angle_bottom = acos(tips.R_bottom(0, 0));
}

return tips;
}

Eigen::MatrixXf Covariance(Eigen::MatrixXf input, int size) {
Eigen::MatrixXf covMat(2, 2);
Eigen::MatrixXf expectedX(1, size);
Eigen::MatrixXf expectedY(1, size);
expectedX = (input.row(0).array() - input.row(0).mean());
expectedY = (input.row(1).array() - input.row(1).mean());
covMat(0, 0) = (expectedX * expectedX.transpose()).value() / (size - 1);
covMat(1, 1) = (expectedY * expectedY.transpose()).value() / (size - 1);
covMat(0, 1) = covMat(1, 0) = (expectedX * expectedY.transpose()).value() / (size - 1);

return covMat;
}

Eigen::MatrixXf RotationMatrix(Eigen::MatrixXf input) {
Eigen::EigenSolver<Eigen::MatrixXf> es(input);
// the matlab code sums over columns but not sure this corresponds to summing over real and im parts so just taking the real part for now
xassert(es.eigenvalues()[0].imag() == 0 && es.eigenvalues()[1].imag() == 0,
        "(stitching) Eigenvalues of rotation matrix are complex. Exiting.");
float eig1 = es.eigenvalues()[0].real();
float eig2 = es.eigenvalues()[1].real();

int argmax_ev = (abs(eig1) >= abs(eig2)) ? 0 : 1; // Find largest eigenvalue
Eigen::Vector2f ev;
xassert(es.eigenvectors().col(argmax_ev).imag() == Eigen::Vector2f::Zero(),
        "(stitching) Eigenvector of rotation matrix is complex. Exiting.");
ev = es.eigenvectors().col(argmax_ev).real(); // Take the corresponding eigenvector
if (ev(1) < 0)
    ev = -ev; // Eigenvector should always point away from the snout.

Eigen::MatrixXf R(2,2); // Construct rotation matrix
R << ev(0), ev(1),
        -ev(1), ev(0);

return R;
}

void AlignToTipAndBottom(cluster_tips &tips, const Eigen::MatrixXf R_init, const Eigen::MatrixXf dots,
                         Eigen::MatrixXf covMat, int cluster_size, int lengthOfTipAndBottom, pointsSelection whiskerPart) {
    int selectedCount;
    Eigen::MatrixXf selectedDots;
    Eigen::MatrixXf dotsOrig(2, cluster_size);

    /// align to the tip
    if (whiskerPart & 1) {
        dotsOrig.row(0) = dots.row(0).array() - tips.endPoint(0);
        dotsOrig.row(1) = dots.row(1).array() - tips.endPoint(1);
        Eigen::MatrixXf rotatedDots = R_init * dotsOrig;

        int selectLength = lengthOfTipAndBottom;
        selectedCount = (rotatedDots.row(0).array() > -selectLength).count();
        while (selectedCount < 2) {
            selectLength += 5;
            selectedCount = (rotatedDots.row(0).array() > -selectLength).count();
        }
        selectedDots.resize(2, selectedCount);

        for (int i = 0, j = 0; i < cluster_size; i++) {
            if (rotatedDots(0, i) > -selectLength) {
                selectedDots.col(j) = dots.col(i);
                j++;
            }
        }
        covMat = Covariance(selectedDots, selectedCount);
        tips.R_tip = RotationMatrix(covMat);
    }

    /// align to the bottom
    if (whiskerPart & 2) {
        dotsOrig.row(0) = dots.row(0).array() - tips.beginPoint(0);
        dotsOrig.row(1) = dots.row(1).array() - tips.beginPoint(1);
        Eigen::MatrixXf rotatedDots = R_init * dotsOrig;

        float selectLength = lengthOfTipAndBottom;
        selectedCount = (rotatedDots.row(0).array() < selectLength).count();
        while (selectedCount < 2) {
            selectLength += 5;
            selectedCount = (rotatedDots.row(0).array() < selectLength).count();
        }
        selectedDots.resize(2, selectedCount);

        for (int i = 0, j = 0; i < cluster_size; i++) {
            if (rotatedDots(0, i) < selectLength) {
                selectedDots.col(j) = dots.col(i);
                j++;
            }
        }
        covMat = Covariance(selectedDots, selectedCount);
        tips.R_bottom = RotationMatrix(covMat);
    }
}

void mergeClusters(std::vector<int> &clusters, Eigen::MatrixXf scores, std::vector<int> &clusterSize,
                   Eigen::MatrixXf *clusterPoints, std::vector<int> &clusterPointsID, int *numOfClusters,
                   int *maxPointsOfCluster, std::vector<bool> &removedClusters, std::vector<bool> &increasedClusters) {
    std::vector<std::pair<int, int>> theseAreTheSame(*numOfClusters);
    float minVal = 0;
    int numOfMerges = 0;
    while (true) {
        //get location of minimum
        Eigen::MatrixXf::Index minRow, minCol;
        minVal = scores.minCoeff(&minRow, &minCol);
        if (minVal > 100000) break;
        theseAreTheSame[numOfMerges].first = minRow;
        theseAreTheSame[numOfMerges].second = minCol;
        scores.row(minRow).fill(FLT_MAX);
        scores.col(minCol).fill(FLT_MAX);
        numOfMerges++;
    }
    if (numOfMerges == 0) return;
    theseAreTheSame.resize(numOfMerges);
    // uncomment to sort clusters
//    std::sort(theseAreTheSame.begin(), theseAreTheSame.end(), sortinrev_first);
//    std::sort(theseAreTheSame.begin(), theseAreTheSame.end(), sortinrev_second);


    // this will be used to do sequential stitchings e.g. 174 -> 173, 175 -> 174, second stitching becomes 175 -> 173
    std::vector<int> matches(*numOfClusters, -1);
    for (int i = 0; i < numOfMerges; i++) {
        int clusterToMerge = theseAreTheSame[i].second;
        int clusterInc = theseAreTheSame[i].first;
        // if the cluster to be merged has already been merged to another, skip
        // second case is to prevent exchange of cluster points, e.g. cluster 2->1 and 1->2
        if (clusterSize[clusterToMerge] == 0 || clusterToMerge == matches[clusterInc]) continue;
        // if the cluster to stitch to has already been merged to another, skip stitching as redundant
        if (clusterSize[clusterInc] == 0) continue;
        // if the cluster to stitch to has already been merged to another, stitch directly to the other cluster
        matches[clusterToMerge] = clusterInc;
        // save new cluster points
        Eigen::MatrixXf newCluster(2, clusterPoints[clusterInc].cols() + clusterPoints[clusterToMerge].cols());
        newCluster << clusterPoints[clusterInc], clusterPoints[clusterToMerge];
        clusterPoints[clusterInc] = newCluster;

        for (int j = 0; j < clusterSize[clusterToMerge]; j++) {
            clusters[clusterPointsID[clusterToMerge * clusters.size() + j]] = clusterInc;
            clusterPointsID[clusterInc * clusters.size() + clusterSize[clusterInc]] = clusterPointsID[
                    clusterToMerge * clusters.size() + j];
            clusterSize[clusterInc]++;
        }
        clusterSize[clusterToMerge] = 0;
        removedClusters[clusterToMerge] = true;
        removedClusters[clusterInc] = false; // this cluster could have been removed in the same round of stitching
        increasedClusters[clusterInc] = true;
    }
}

void removeClusters(std::vector<int> &clusters, std::vector<float> &rotatedPositions, std::vector<int> &clusterSize,
                            std::vector<bool> clustersToRemove, int *numOfClusters) {
    // count number of removed clusters before each cluster
    std::vector<int> removedClusters(*numOfClusters, 0);
    for (int i = 1; i < *numOfClusters; i++) {
        removedClusters[i] = removedClusters[i-1];
        if (clustersToRemove[i-1]) removedClusters[i]++;
        clusterSize[i - removedClusters[i]] = clusterSize[i];
    }
    std::vector<int> clustersFromSnout;
    std::vector<float> rotatedPositionsFromSnout;
    // remove floating clusters
    for (size_t i = 0; i < clusters.size(); i++) {
        if (!clustersToRemove[clusters[i]]) {
            clustersFromSnout.push_back(clusters[i]-removedClusters[clusters[i]]);
            rotatedPositionsFromSnout.push_back(rotatedPositions[2*i]);
            rotatedPositionsFromSnout.push_back(rotatedPositions[2*i+1]);
        }
    }
    clusters = clustersFromSnout;
    rotatedPositions = rotatedPositionsFromSnout;
    *numOfClusters -= removedClusters[*numOfClusters-1] + (removedClusters[*numOfClusters-1] == 0);
    clusterSize.resize(*numOfClusters);
}

template <typename T>
bool radonTransformCheck(const cluster_tips *whisker_clusters, const Eigen::MatrixXf *dots, int q, int r, T *preprocessedImage,
                         stitch_I_opts stitchIOpts, plotting_opts plottingOpts) {
    // line equation: y = a*x+b
    // line starts from the tip of q and ends at the bottom of r
    float a = atan2((whisker_clusters[r].beginPoint(1) - whisker_clusters[q].endPoint(1)),
                     (whisker_clusters[r].beginPoint(0) - whisker_clusters[q].endPoint(0)));
//    // plot pair of clusters and connecting line for debugging
//    Eigen::MatrixXf plottingDots[3];
//    plottingDots[0] = dots[q];
//    plottingDots[1] = dots[r];
//    plottingDots[2] = Eigen::MatrixXf(2, 20);
//    for (int i = 0; i < 20; i++) {
//        plottingDots[2](0, i) = whisker_clusters[q].endPoint(0) + i * cos(a);
//        plottingDots[2](1, i) = whisker_clusters[q].endPoint(1) + i * sin(a);
//    }
//    plotMultipleClusters(plottingDots, 3, plottingOpts, "Dots");

    int k = 1;
    bool stopFlag = false;
    bool valid = true;
    Eigen::MatrixXf connectingLine(2, stitchIOpts.radonLength);
    Eigen::MatrixXf sbaseMat = Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(plottingOpts.sbase[0], 2, 2);
    Eigen::Vector2f originVec = Eigen::Map<Eigen::Vector2f>(plottingOpts.origin, 2);
    while (true) {
        // test condition every radonLength points
        float prev_x = whisker_clusters[q].endPoint(0);
        for (int i = 0; i < stitchIOpts.radonLength; i++) {
            float x = whisker_clusters[q].endPoint(0) + k * cos(a);
            // if extended line surpasses the beginning of r, stop extending
            if (abs(x - whisker_clusters[r].beginPoint(0)) > abs(prev_x - whisker_clusters[r].beginPoint(0))) {
                stopFlag = true;
                break;
            }
            float y = whisker_clusters[q].endPoint(1) + k * sin(a);
            connectingLine(0, i) = x;
            connectingLine(1, i) = y;
            k++;
            prev_x = x;
        }
        if (stopFlag) break;
        connectingLine.row(1) = connectingLine.row(1).array().eval() + plottingOpts.cutoff;
        connectingLine = (sbaseMat.inverse() * connectingLine.eval()).colwise() + originVec;
        float meanScore = 0;
        int i;
        for (i = 0; i < stitchIOpts.radonLength; i++) {
            if ((int) connectingLine(1,i) < 0 || (int) connectingLine(1,i) >= plottingOpts.height ||
                (int) connectingLine(0,i) < 0 || (int) connectingLine(0,i) >= plottingOpts.width)
                break;
            meanScore += preprocessedImage[((int) connectingLine(1,i))*plottingOpts.width + ((int) connectingLine(0,i))];
        }
        meanScore /= i;
        if (meanScore < stitchIOpts.radonThreshold) {
            valid = false;
            break;
        }
    }
    return valid;
}

template bool radonTransformCheck<float>(const cluster_tips *whisker_clusters, const Eigen::MatrixXf *dots, int q, int r, float *preprocessedImage,
                         stitch_I_opts stitchIOpts, plotting_opts plottingOpts);

template bool radonTransformCheck<uchar>(const cluster_tips *whisker_clusters, const Eigen::MatrixXf *dots, int q, int r, uchar *preprocessedImage,
                                         stitch_I_opts stitchIOpts, plotting_opts plottingOpts);

template<typename T>
T min_array(T *arr, int size) {
    T min = FLT_MAX;
    for (int i = 0; i < size; i++) {
        min = (*(arr + i) < min) ? *(arr + i) : min;
    }
    return min;
}

template<typename T>
void matmult(T *out, T *in1, T *in2, int m, int n, int l) {
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < l; j++) {
            T res = 0;
            for (int k = 0; k < n; k++) {
                res += *(in1 + i * n + k) * *(in2 + k * l + j);
            }
            *(out + i * l + j) = res;
        }
    }
}

// Driver function to sort the vector elements by
// first element of pair in descending order
bool sortinrev_first(const std::pair<int, int> &a,
                     const std::pair<int, int> &b) {
    return (a.first > b.first);
}

// Driver function to sort the vector elements by
// second element of pair in descending order
bool sortinrev_second(const std::pair<int, int> &a,
               const std::pair<int, int> &b) {
    return (a.second > b.second);
}

// Function to find mean.
float mean(int *arr, int n) {
    float sum = 0;
    for (int i = 0; i < n; i++)
        sum = sum + arr[i];
    return (float) sum / n;
}

// Function to find covariance.
float covariance(int *arr1, int *arr2, int n) {
    float sum = 0;
    for (int i = 0; i < n; i++)
        sum = sum + (float) (arr1[i] - mean(arr1, n)) *
                    (arr2[i] - mean(arr2, n));
    return (float) sum / (n - 1);
}
