/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_CLUSTERING_H
#define FWTS_C_CLUSTERING_H

#include <opencv2/opencv.hpp>
#include "../../../libs/eigen/Eigen/Dense"
#include "stitching_I.h"
#include "../../utils/plotting.h"
#include "stitching_II.h"
#include "whisker_forming_options.h"
#include "../../utils/profile.h"

enum extensionSide {up = 0, down = 1};
enum tracingStep {stegerLinking = 0, curveFollowing = 1};

void clustering_I(std::vector<std::pair<int16_t,int16_t>> startPoints, float *whiskerAngles, int &numOfPoints,
                  std::vector<std::pair<uint16_t, uint16_t>> &whiskerPositions, std::vector<int> &clusters,
                  clustering_opts clusteringOpts);

template <typename T>
int clustering_II(std::vector<int> &clusters, std::vector<std::pair<uint16_t, uint16_t>> whiskerPositions,
                   std::vector<float> &rotatedPositions, int numOfPoints, T *preprocessedImage, plotting_opts plottingOpts,
                   clustering_opts clusteringOpts, stitch_I_opts stitchIOpts, stitch_II_opts stitchIIOpts, timers &timers);

void rotatePositions(std::vector<float> &rotatedPositions, std::vector<std::pair<uint16_t,uint16_t>> whiskerPositions,
                     std::vector<int> &clusters, clustering_opts cluster_opts);

void curveTracing(std::vector<std::pair<int16_t,int16_t>> startPoints,
                  std::vector<std::pair<uint16_t, uint16_t>> &whiskerPositions, std::vector<int> &clusters,
                  std::vector<int> &IDX, float *whiskerAngles, clustering_opts clusteringOpts);

void defineSearchBounds(int x_b[], int y_b[], bool &diag, float angle, int L);

inline void scanNeighbours(cv::Point& choice, bool &rejectPointFlag, bool diag, int x_b[], int y_b[], float angle,
                           extensionSide chosenSide, cv::Point currPoint, float *whiskerAngles,
                           std::vector<int> &IDX, std::vector<std::pair<uint16_t, uint16_t>> &whiskerPositions,
                           std::vector<int> &clusters, clustering_opts clusteringOpts);

inline float computeAverageAngle(float prev_averageAngle, std::vector<float> &angle_stats, int &prevSize, float old_angle, int M);

inline void updateAngleList(float averageAngle, float new_angle, size_t M, std::vector<float> &angle_stats);

void lookFurther(cv::Point currPoint, cv::Point& choice, float averageAngle, int K, std::vector<int> &IDX,
                 const float *whiskerAngles, extensionSide chosenSide, clustering_opts clusteringOpts, bool noiseCheck);

#endif //FWTS_C_CLUSTERING_H
