/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_RECOGNITION_H
#define FWTS_C_RECOGNITION_H

#include "tracking.h"

void recognizer_N_expert(const std::vector<whisker> &lineGatherer_tracker, const std::vector<whisker> &lineGatherer_detector,
                         std::vector<int> &order, std::vector<int> &missedClusters, std::vector<int> &missedWhiskers,
                         recog_opts &recogOpts, N_expert_opts NExpertOpts, const track_opts &trackOpts, train_opts &trainOpts);

void get_norm_values(const std::vector<whisker> &lineGatherer, float &normXf, float &normA, float &normL, float &normB);

bool cmp_Xf(const whisker& whisker1, const whisker& whisker);

bool cmp_angle(const whisker& whisker1, const whisker& whisker2);

bool cmp_length(const whisker& whisker1, const whisker& whisker2);

bool cmp_shape(const whisker& whisker1, const whisker& whisker2);

void storeLinearTest(classSVMData testFeatures, feature_node *x, int index);

void SVM_Recognition(std::vector<int> &choicePerCluster, const std::vector<whisker> &lineGatherer_tracker,
                     const std::vector<whisker> &lineGatherer_detector, const std::vector<std::vector<whiskerDB>> currentClusters,
                     train_opts &trainOpts, recog_opts &recogOpts, const track_opts &trackOpts);

void predictOneVsOneLinear(feature_node *x, int &choice, recog_opts &recogOpts, int tracker_size);

void assignMatches(std::vector<int> &order, const std::vector<whisker> &lineGatherer_tracker,
                   const std::vector<whisker> &lineGatherer_detector, std::vector<int> choicePerCluster, Eigen::Array4f normArray,
                   recog_opts &recogOpts);

void N_expert(const std::vector<whisker> &lineGatherer_tracker, const std::vector<whisker> &lineGatherer_detector,
              std::vector<int> &order, N_expert_opts NExpertOpts);

#endif //FWTS_C_RECOGNITION_H
