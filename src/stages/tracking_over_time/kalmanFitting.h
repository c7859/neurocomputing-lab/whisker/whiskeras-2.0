/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_KALMANFITTING_H
#define FWTS_C_KALMANFITTING_H

#include <vector>
#include "tracking.h"
#include "../whisker_forming/whisker_forming_options.h"

void kalmanFitting(std::vector<int> &order, std::vector<int> &missedClusters, std::vector<int> &missedWhiskers,
                   const std::vector<whisker> &lineGatherer_tracker, const std::vector<whisker> &lineGatherer_detector,
                   track_opts &trackOpts);

float mean(std::vector<float> &arr, int n);


#endif //FWTS_C_KALMANFITTING_H
