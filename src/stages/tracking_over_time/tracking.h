/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_TRACKING_H
#define FWTS_C_TRACKING_H

#include "../whisker_forming/whisker_forming_options.h"
#include "../../../libs/eigen/Eigen/Dense"
#include "../../utils/plotting.h"
#include "../../../libs/liblinear/linear.h"


// struct to store data points for training
struct whiskerDB {
    float x1_p;
    float x2_p;
    float cotx2_p;
    float x1_a;
    float cotx2_a;
    float L;
    float x3;
    bool cnf;
};

struct featuresVec {
    float x1_p;
    float cotx2_p;
    float x1_a;
    float cotx2_a;
    float L;
    float x3;
};

struct classSVMData {
    size_t size = 0;
    std::vector<featuresVec> features;
};

struct track_opts {
    std::vector<bool> couldNotFit;
    std::vector<bool> couldNotTrack;
    whisker oldAverages = {0, 0, 0, 0, 0, 0};
    whisker newAverages = {0, 0, 0, 0, 0, 0};
    whisker prevAverages = {0, 0, 0, 0, 0, 0};
    whisker prevMean = {0, 0, 0, 0, 0, 0};
    bool dontTrack = false;
    std::vector<std::vector<whiskerDB>> whiskerDb;
    featuresVec featuresMean = {0,0,0,0,0};
    featuresVec featuresSTD = {0,0,0,0,0};
    int fittedInstances = 0;
    // Kalman filter options
    bool useKalman = true;
    unsigned int kalmanDistance = 10; // use data from up to <kalmanDistance> earlier frames
    // define weights of different whisker parameters for matching
    float weightB = 0.05;
    float weightL = 0.01;
    float weightAlpha = 1.5;
    float weightX = 2.5;
    // maximum allowed changes for same whiskers, frame-to-frame
    float maxChangeInAngle = 15 * CV_PI / 180;
    float maxChangeInXf = 50;
    float maxChangeInBend = 0.01;
    float maxChangeInLength = 50;
    float maxScoreForMatch = 50;
    bool plot = false;
    int verbose = 1;
};

// Tracking: Fit-whisker-point-to-data-tracking option
struct fwptd_opts {
    // Max average absolute distance (as a function of real distance and difference in length) between cluster and parametrization.
    float maxMatchDistance = 1600;
    // Maximum for minimum distance between cluster point and parametrization.
    // If the distance between the closest point and the parametrization is higher than this, matching is impossible.
    float maxLowestDistance = 100;
    float maxPrevDist = 200;
};

// Recognition: SVM parameters
struct train_opts {
    int retrainFreqInit = 1; // Minimum retraining frequency in the beginning (frames/retraining).
    int retrainAtLeast = 5; // Absolute minimum retraining frequency.
    // Increase <train_opt.retrainFreqInit> with <train_opt.increaseWith> every <train_opt.increaseEvery> frames.
    int increaseEvery = 100;
    int increaseWith = 1;
    int bootstrap = 100; // In the first <train_opt.bootstrap> only tracking happens; no recognition.
    uint window = 7000; // Sliding window size for training data
    // Maximum size of training data per whisker during training.
    // If <train_opt.maxSizeTrainingData> is smaller than <train_opt.window>,
    // the training examples of which the average angle of all the whiskers is most similar to the current situation will be chosen.
    size_t maxSizeTrainingData = 2500;
    int chance_train = 20;
    size_t newDataSize = 0;
    int numFeatures = 6; // number of features
    parameter linear_param; // liblinear
    int verbose = 1;
};

//  Recognition: weighing
struct recog_opts {
    float weightX = 1;
    float weightA = 1;
    float weightB = 0.05;
    float weightL = 0.09;
    std::vector<int> ks; // helper vectors, in order to implement
    std::vector<int> js; // one-vs-one multiclass strategy
    // libLinear
    model *linearModel;
    feature_node **feature_space;
    problem *linearProblem;
    double linearBias = -1;
    int *numExamples;
    int verbose = 1;
};

// N-expert rejection parameters
struct N_expert_opts {
    // If the angle, position or bending between a whisker recognized in frame n differs more than
    // <N_expert_op.maxChangeInAngle> degrees, <N_expert_op.maxChangeInXf> pixels, <N_expert_op.maxChangeInBend>
    // from the one in n-1, the recognition is rejected.
    float maxChangeInAngle = 15 * CV_PI / 180;
    float maxChangeInXf = 100;
    float maxChangeInBend = 0.01;
    float maxChangeInLength = 50; // added later
    int verbose = 1;
};

void tracking_and_recognition(std::vector<whisker> &lineGatherer_detector, std::vector<whisker> &lineGatherer_tracker,
                              std::vector<float> &dots, std::vector<int> &clusters, std::vector<int> &order,
                              bool start, int currFrame, int startFrame, track_opts &trackOpts, train_opts &trainOpts,
                              fwptd_opts fwptdOpts, recog_opts &recogOpts, N_expert_opts NExpertOpts, plotting_opts plottingOpts);

void fitWhisker2clusters(std::vector<int> &order, std::vector<int> &missedClusters, std::vector<int> &missedWhiskers,
                         std::vector<float> &dots, std::vector<int> &clusters, const std::vector<whisker> &lineGatherer_tracker,
                         const std::vector<whisker> &lineGatherer_detector, fwptd_opts fwptdOpts);

Eigen::MatrixXf distanceToLine(Eigen::MatrixXf dots, const std::vector<whisker> &lineGatherer);

whisker whiskerAverages(const std::vector<whisker> &lineGatherer, const std::vector<int> &order, bool use_order);

void estimateWhiskers(std::vector<whisker> &lineGatherer_tracker, const std::vector<int> &order,
                      std::vector<bool> &couldNotFit, track_opts trackOpts);

bool sort_position(const whisker &a,
                   const whisker &b);

void markMissedClustersAndWhiskers(std::vector<whisker> lineGatherer_tracker, std::vector<whisker> lineGatherer_detector,
        std::vector<int> order, std::vector<int> &missedClusters, std::vector<int> &missedWhiskers);

void P_expert(const std::vector<whisker> &lineGatherer_tracker, const std::vector<whisker> &lineGatherer_detector,
              std::vector<float> &dots, std::vector<int> &clusters, std::vector<int> &order,
              std::vector<int> &missedClusters, std::vector<int> &missedWhiskers, fwptd_opts fwptdOpts, track_opts &trackOpts);

#endif //FWTS_C_TRACKING_H
