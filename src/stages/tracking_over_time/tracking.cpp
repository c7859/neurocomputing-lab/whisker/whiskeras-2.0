/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tracking.h"
#include <numeric>
#include "recognition.h"
#include "kalmanFitting.h"

void tracking_and_recognition(std::vector<whisker> &lineGatherer_detector, std::vector<whisker> &lineGatherer_tracker,
                                  std::vector<float> &dots, std::vector<int> &clusters, std::vector<int> &order,
                                  bool start, int currFrame, int startFrame, track_opts &trackOpts, train_opts &trainOpts,
                                  fwptd_opts fwptdOpts, recog_opts &recogOpts, N_expert_opts NExpertOpts, plotting_opts plottingOpts) {
    // If this is the first frame of the analysis, some things have to be set up.
    if (start) {
        // The order is just an ascending vector of number
        order.resize(lineGatherer_detector.size());
        for (size_t i = 0; i < lineGatherer_detector.size(); i++) {
            order[i] = i;
        }
        // Sort whiskes by position, so that it's easier to recognize them in the plot (highest whisker on the image first)
        std::sort(lineGatherer_detector.begin(), lineGatherer_detector.end(), sort_position);
        lineGatherer_tracker = lineGatherer_detector; // All detected whiskers become tracks ...
        trackOpts.couldNotFit.assign(lineGatherer_tracker.size(), false); // ... so no missed whiskers yet.
        // The average of the whisker parameters is calculated, this is useful as training data.
        trackOpts.oldAverages = whiskerAverages(lineGatherer_tracker, order, true);
        trackOpts.prevAverages = whiskerAverages(lineGatherer_tracker, order, false);
        trackOpts.whiskerDb.resize(lineGatherer_tracker.size());
    }
    // If this is NOT the first frame, the detected whiskers need to be related to the earlier ones
    else {
        // The average of the whisker parameters of the previous frame.
        // This is useful for selection of training data,
        // also for estimating the position of whisker that could not be tracked.
        trackOpts.prevAverages = whiskerAverages(lineGatherer_tracker, order, false);

        if (trackOpts.verbose) std::cout << "Tracking whiskers..." << std::endl;

        std::vector<int> missedClusters, missedWhiskers;
        // During the initial `bootstrapping' period, we're only tracking, not recognizing.
        if (currFrame-startFrame <= trainOpts.bootstrap) {
            if (trackOpts.useKalman) {
                kalmanFitting(order, missedClusters, missedWhiskers, lineGatherer_tracker,
                              lineGatherer_detector, trackOpts);
            }
            else {
                // Do Fit-whisker-point-to-data tracking
                fitWhisker2clusters(order, missedClusters, missedWhiskers, dots, clusters,
                                    lineGatherer_tracker, lineGatherer_detector, fwptdOpts);
            }
        }
        // After the `bootstrapping' period, we're working with SVMs
        else {
            // This function implements both the SVM recognition process and the N-expert.
            recognizer_N_expert(lineGatherer_tracker, lineGatherer_detector, order, missedClusters,
                    missedWhiskers, recogOpts, NExpertOpts, trackOpts, trainOpts);
            // If not all the clusters and whiskers were matched, there's work to do for the P-expert
            if (missedClusters.size() > 0 || missedWhiskers.size() > 0) {
                P_expert(lineGatherer_tracker, lineGatherer_detector, dots, clusters, order,
                        missedClusters, missedWhiskers, fwptdOpts, trackOpts);
            }
        }
        // We still haven't changed lineGatherer_tracker, so here we're calculating the averages from the previous frame
        trackOpts.oldAverages = whiskerAverages(lineGatherer_tracker, order, true);
        // now we're going to put detected whiskers from frame n into the array 'lineGatherer_tracker'
        for (size_t i = 0; i < order.size(); i++) {
            if (order[i] >= 0) {
                lineGatherer_tracker[i] = lineGatherer_detector[order[i]];
                // plot matching whiskers
//                plotLines(lineGatherer_tracker, lineGatherer_tracker.size(), i, plottingOpts, "line");
//                plotLines(lineGatherer_detector, lineGatherer_detector.size(), order[i], plottingOpts, "line");
            }
        }
        // Now we're calculating the averages from the current frame
        trackOpts.newAverages = whiskerAverages(lineGatherer_tracker, order, true);

        // Performance metric: % of tracked whiskers from those detected
        trackOpts.couldNotTrack.resize(lineGatherer_detector.size());
        trackOpts.couldNotTrack.assign(lineGatherer_detector.size(), false);
        for (size_t j = 0; j < missedClusters.size(); j++) {
            if (missedClusters[j]) trackOpts.couldNotTrack[missedClusters[j]] = true;
        }
    }

    // Mark all the whiskers that are definitively missed in this frame.
    std::vector<bool> couldNotFit(lineGatherer_tracker.size(), false);
    // And estimate their positon based on the movement of the rest of the whiskers.
    estimateWhiskers(lineGatherer_tracker, order, couldNotFit, trackOpts);
    trackOpts.couldNotFit = couldNotFit;
}

void fitWhisker2clusters(std::vector<int> &order, std::vector<int> &missedClusters, std::vector<int> &missedWhiskers,
        std::vector<float> &dots, std::vector<int> &clusters, const std::vector<whisker> &lineGatherer_tracker,
        const std::vector<whisker> &lineGatherer_detector, fwptd_opts fwptdOpts) {
    Eigen::MatrixXf distanceMat = Eigen::MatrixXf::Zero(lineGatherer_detector.size(), lineGatherer_tracker.size());
    Eigen::MatrixXf lowestDistanceMat = Eigen::MatrixXf::Zero(lineGatherer_detector.size(), lineGatherer_tracker.size());
    Eigen::MatrixXf occMat = Eigen::MatrixXf::Zero(lineGatherer_detector.size(), lineGatherer_tracker.size());

    order.assign(lineGatherer_tracker.size(), -1);

    // convert dots to Eigen Matrix
    Eigen::MatrixXf dotsMat = Eigen::Map<Eigen::MatrixXf>(dots.data(), 2, dots.size()/2);

    // For every whisker point, calculate the mean square distance to every parametrization
    Eigen::MatrixXf distances = distanceToLine(dotsMat, lineGatherer_tracker);
    Eigen::MatrixXi clustersMat = Eigen::Map<Eigen::MatrixXi>(clusters.data(), 1, clusters.size());

    // configure distances for each cluster
    Eigen::MatrixXf whiskerDistances[lineGatherer_detector.size()];
    std::vector<int> clusterSize(lineGatherer_detector.size(), 0);
    for (size_t i = 0; i < lineGatherer_detector.size(); i++) {
        whiskerDistances[i].resize(clusters.size(), lineGatherer_tracker.size());
    }
    for (size_t i = 0; i < clusters.size(); i++) {
        whiskerDistances[clusters[i]].row(clusterSize[clusters[i]]) = distances.row(i);
        clusterSize[clusters[i]]++;
    }
    // Now take every cluster
    for (size_t i = 0; i < lineGatherer_detector.size(); i++) {
        whiskerDistances[i] = whiskerDistances[i].topRows(clusterSize[i]).eval(); // without eval, some values can be corrupted
        // Calculate mean distances of cluster to each parametrization
        distanceMat.row(i) = whiskerDistances[i].colwise().mean();
        // Calculate smallest distance between a point and the parametrization
        lowestDistanceMat.row(i) = whiskerDistances[i].array().abs().colwise().minCoeff();
        // If the mean square distance is too high...
        distanceMat.row(i) = distanceMat.row(i).array() + (distanceMat.row(i).array() > fwptdOpts.maxMatchDistance).cast<float>() * 100000;
        // ... or the point that is closest to the parametrization is still too far away,
        distanceMat.row(i) = distanceMat.row(i).array() + (lowestDistanceMat.row(i).array() > fwptdOpts.maxLowestDistance).cast<float>() * 100000;
        // then we're not matching the parametrization with the cluster.

    }
    // Matching clusters and parametrizations based on their scores
    while (true) {
        Eigen::MatrixXf::Index minRow, minCol;
        float minDist = distanceMat.minCoeff(&minRow, &minCol); // Get value and indices of field with the lowest value.
        if (minDist > 99999) {
            break; // if lowest score becomes too high break
        }
        distanceMat(minRow, minCol) = FLT_MAX; // Set its value to Inf so that its no longer the lowest value.
        if (!(occMat.row(minRow).sum() || occMat.col(minCol).sum())) { // If the whiskers in both frames are still `free', match them
            occMat(minRow, minCol) = 1; // they're now not free anymore
            order[minCol] = minRow; // and they're matched in the 'order' vector
        }
        if (occMat.colwise().sum().prod() || occMat.rowwise().sum().prod()) {
            break; // if all whiskers are matched break
        }
    }
    markMissedClustersAndWhiskers(lineGatherer_tracker, lineGatherer_detector, order, missedClusters, missedWhiskers);
}

Eigen::MatrixXf distanceToLine(Eigen::MatrixXf dots, const std::vector<whisker> &lineGatherer) {
//    std::vector<float> distancesq(lineGatherer.size() * dots.cols());
    Eigen::MatrixXf distances(dots.cols(), lineGatherer.size());
    for (size_t i = 0; i < lineGatherer.size(); i++) {
        Eigen::MatrixXf dotsOrig = dots;
        dotsOrig.row(0) = dots.row(0).array() - lineGatherer[i].position;
        Eigen::MatrixXf rotationMat(2, 2);
        rotationMat << cos(lineGatherer[i].angle), sin(lineGatherer[i].angle),
                -sin(lineGatherer[i].angle), cos(lineGatherer[i].angle);
        Eigen::MatrixXf points_sh(2, dotsOrig.cols());
        points_sh = rotationMat * dotsOrig;
        // We're not taking the actual length, but the position of tip of the whisker on the axis that crosses X with angle alpha
        Eigen::Array<bool, Eigen::Dynamic, 1> leq_x_max = (points_sh.row(0).array() <= lineGatherer[i].x_max);
        Eigen::ArrayXf dist1 = (lineGatherer[i].shape * points_sh.row(0).array().pow(2)
                - points_sh.row(1).array()).pow(2);
        Eigen::ArrayXf dist2 = ((points_sh.row(0).array() - lineGatherer[i].x_max) / 20).pow(2)
                + (points_sh.row(1).array() - lineGatherer[i].shape * pow(lineGatherer[i].x_max, 2)).pow(2);
        // pick one of the two distances
        distances.col(i) = (leq_x_max.cast<float>() * dist1 + (1 - leq_x_max.cast<float>()) * dist2);
    }
    return distances;
}

whisker whiskerAverages(const std::vector<whisker> &lineGatherer, const std::vector<int> &order, bool use_order) {
    whisker averages = {0, 0, 0, 0, 0, 0};
    for (size_t i = 0; i < lineGatherer.size(); i++) {
        if (!use_order || order[i] >= 0) {
            averages.position += lineGatherer[i].position;
            averages.angle += lineGatherer[i].angle;
        }
    }
    averages.position /= lineGatherer.size();
    averages.angle /= lineGatherer.size();
    return averages;
}

void estimateWhiskers(std::vector<whisker> &lineGatherer_tracker, const std::vector<int> &order,
        std::vector<bool> &couldNotFit, track_opts trackOpts) {
    for (size_t i = 0; i < order.size(); i++) {
        if (order[i] < 0) {
            lineGatherer_tracker[i].position += - trackOpts.oldAverages.position + trackOpts.newAverages.position;
            lineGatherer_tracker[i].angle += - trackOpts.oldAverages.angle + trackOpts.newAverages.angle;
            // Let the user know about this.
            if (trackOpts.verbose > 1) std::cout << "Could not detect whisker " << i << ", estimating its position" << std::endl;
            couldNotFit[i] = true; // And also mark it in the results
        }
    }
}

// Driver function to sort the vector elements by
// position of whiskers in ascending order
bool sort_position(const whisker &a,
                   const whisker &b) {
    return (a.position < b.position);
}

void markMissedClustersAndWhiskers(std::vector<whisker> lineGatherer_tracker, std::vector<whisker> lineGatherer_detector,
        std::vector<int> order, std::vector<int> &missedClusters, std::vector<int> &missedWhiskers) {
    // mark missed clusters
    missedClusters.resize(0);
    for (size_t i = 0; i < lineGatherer_detector.size(); i++) {
        size_t j;
        for (j = 0; j < order.size(); j++) {
            if (order[j] == (int) i) {
                break;
            }
        }
        if (j == order.size()) missedClusters.push_back(i);
    }

    // mark missed whiskers
    missedWhiskers.resize(0);
    for (size_t i = 0; i < lineGatherer_tracker.size(); i++) {
        if (order[i] == -1) {
            missedWhiskers.push_back(i);
        }
    }
}

void P_expert(const std::vector<whisker> &lineGatherer_tracker, const std::vector<whisker> &lineGatherer_detector,
              std::vector<float> &dots, std::vector<int> &clusters, std::vector<int> &order,
              std::vector<int> &missedClusters, std::vector<int> &missedWhiskers, fwptd_opts fwptdOpts, track_opts &trackOpts) {
    std::vector<int> order_alt, missedClusters_alt, missedWhiskers_alt;
    if (trackOpts.useKalman) {
        kalmanFitting(order_alt, missedClusters_alt, missedWhiskers_alt, lineGatherer_tracker, lineGatherer_detector, trackOpts);
    }
    else {
        fitWhisker2clusters(order_alt, missedClusters_alt, missedWhiskers_alt, dots, clusters,
                            lineGatherer_tracker, lineGatherer_detector, fwptdOpts);
    }
    // For every whisker that was not recognized by SVM but is supposed to be tracked
    for (size_t j = 0; j < missedWhiskers.size(); j++) {
        int alternativeCluster = order_alt[missedWhiskers[j]]; // Get assignment from P-expert
        // Did P-expert match missed whisker with unassigned cluster? Or didn't P-expert make a match either?
        for (size_t k = 0; k < missedClusters.size(); k++) {
            if (alternativeCluster == missedClusters[k]) { // If P-expert made a match...
                order[missedWhiskers[j]] = alternativeCluster;
                if (trackOpts.verbose > 1) std::cout << "Whisker " << missedWhiskers[j] << " was found back by P-expert" << std::endl;
                missedClusters.erase(missedClusters.begin() + k);
                missedWhiskers.erase(missedWhiskers.begin() + j);
                j--; // decrement j before incrementing it again, because the next value will be in the same position due to erasing
                break;
            }
        }
    }
}
