/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "kalmanFitting.h"
#include "../../../libs/polyfitEigen/PolyfitEigen.hpp"

void kalmanFitting(std::vector<int> &order, std::vector<int> &missedClusters, std::vector<int> &missedWhiskers,
                   const std::vector<whisker> &lineGatherer_tracker, const std::vector<whisker> &lineGatherer_detector,
                   track_opts &trackOpts) {
    // Prepare 'occupied' and score matrices.
    Eigen::MatrixXf distMat = Eigen::MatrixXf::Zero(lineGatherer_detector.size(), lineGatherer_tracker.size());
    Eigen::MatrixXf occMat = Eigen::MatrixXf::Zero(lineGatherer_detector.size(), lineGatherer_tracker.size());

    // reinitialize order vector to -1
    order.assign(lineGatherer_tracker.size(), -1);

    // Prepare vectors for the predictions.
    Eigen::VectorXf predictions_Xf = Eigen::VectorXf::Zero(lineGatherer_tracker.size());
    Eigen::VectorXf predictions_A = Eigen::VectorXf::Zero(lineGatherer_tracker.size());
    Eigen::VectorXf predictions_L = Eigen::VectorXf::Zero(lineGatherer_tracker.size());
    Eigen::VectorXf predictions_B = Eigen::VectorXf::Zero(lineGatherer_tracker.size());

    // Get the minimal and maximal values for all the
    // parameters from the previous frame. This will help in normalization.
    float minXf = FLT_MAX; float maxXf = -FLT_MAX;
    float minA = FLT_MAX; float maxA = -FLT_MAX;
    float minL = FLT_MAX; float maxL = -FLT_MAX;
    float minB = FLT_MAX; float maxB = -FLT_MAX;
    for (size_t i = 0; i < lineGatherer_tracker.size(); i++) {
        if (lineGatherer_tracker[i].position < minXf) minXf = lineGatherer_tracker[i].position;
        if (lineGatherer_tracker[i].position > maxXf) maxXf = lineGatherer_tracker[i].position;
        if (lineGatherer_tracker[i].angle < minA) minA = lineGatherer_tracker[i].angle;
        if (lineGatherer_tracker[i].angle > maxA) maxA = lineGatherer_tracker[i].angle;
        if (lineGatherer_tracker[i].length < minL) minL = lineGatherer_tracker[i].length;
        if (lineGatherer_tracker[i].length > maxL) maxL = lineGatherer_tracker[i].length;
        if (lineGatherer_tracker[i].shape < minB) minB = lineGatherer_tracker[i].shape;
        if (lineGatherer_tracker[i].shape > maxB) maxB = lineGatherer_tracker[i].shape;
    }

    // Dividing the differences in parameters between the two frames by these
    // values will normalize them, which makes it possible to weight them properly
    float normXf = maxXf - minXf;
    float normA = maxA - minA;
    float normL = maxL - minL;
    float normB = maxB - minB;

    if (normXf == 0) perror("(Kalman Fitting) Potentially only 1 whisker was tracked in the first frame. Cannot normalize. Exiting.");

    // for every whisker from frame n-1 we want to track...
    for (size_t i = 0; i < lineGatherer_tracker.size(); i++) {
        // get the data from the last <kalmanDistance> earlier frames
        std::vector<float> necc_Xf;
        std::vector<float> necc_A;
        std::vector<float> necc_L;
        std::vector<float> necc_B;
        int j = trackOpts.whiskerDb[i].size();
        for (size_t k = 0; k < trackOpts.kalmanDistance && j > 0; k++) {
            necc_Xf.push_back(trackOpts.whiskerDb[i][j-1].x1_p);
            necc_A.push_back(trackOpts.whiskerDb[i][j-1].x2_p);
            necc_L.push_back(trackOpts.whiskerDb[i][j-1].L);
            necc_B.push_back(trackOpts.whiskerDb[i][j-1].x3);
            j--;
        }

        // If there are fewer than <kalmanDistance> frames, it doesn't make much
        // sense to use a polynomial fit, so we just take a simple mean.
        if (trackOpts.whiskerDb[i].size() < trackOpts.kalmanDistance) {
            predictions_Xf(i) = mean(necc_Xf, necc_Xf.size());
            predictions_A(i) = mean(necc_A, necc_A.size());
        }
        // otherwise, we use polyfit for angle and position to predict the situation in the next frame
        else {
            // construct X's vector for polyfit
            std::vector<float> X(trackOpts.kalmanDistance);
            for (size_t k = 0; k < trackOpts.kalmanDistance; k++) {
                X[k] = k + 1;
            }

            std::vector<float> p_Xf = polyfit_Eigen(X, necc_Xf, 2);
            predictions_Xf(i) = p_Xf[0];
            std::vector<float> p_A = polyfit_Eigen(X, necc_A, 2);
            predictions_A(i) = p_A[0];
        }
        // length and bending do not really change too much, so here, it makes most sense to take an average.
        predictions_L(i) = mean(necc_L, necc_L.size());
        predictions_B(i) = mean(necc_B, necc_B.size());
    }

    // Calculate the scores for each pair of whiskers from n and n-1
    for (size_t i = 0; i < lineGatherer_tracker.size(); i++) {
        for (size_t j = 0; j < lineGatherer_detector.size(); j++) {
            distMat(j, i) = sqrt(pow(trackOpts.weightX*(predictions_Xf(i)-lineGatherer_detector[j].position)/normXf, 2)
                    + pow(trackOpts.weightAlpha*(predictions_A(i)-lineGatherer_detector[j].angle)/normA, 2)
                    + pow(trackOpts.weightL*(predictions_L(i)-lineGatherer_detector[j].length)/normL, 2)
                    + pow(trackOpts.weightB*(predictions_B(i)-lineGatherer_detector[j].shape)/normB, 2));
        }
    }

    while (true) {
        Eigen::MatrixXf::Index minRow, minCol;
        float minDist = distMat.minCoeff(&minRow, &minCol); // Get value and indices of field with the lowest value.
        // if all whiskers are matched or the score becomes too high, then break
        if (occMat.colwise().sum().prod() || occMat.rowwise().sum().prod() || minDist > trackOpts.maxScoreForMatch) {
            break;
        }
        // Set its value to Inf so that its no longer the lowest value.
        distMat(minRow, minCol) = FLT_MAX; // Set its value to Inf so that its no longer the lowest value.
        // If the whiskers in both frames are still `free', match them
        if (!(occMat.row(minRow).sum() || occMat.col(minCol).sum())) {
            occMat(minRow, minCol) = 1; // they're now not free anymore
            order[minCol] = minRow; // and they're matched in the 'order' vector
        }
    }

    // Check the matches for validity.
    for (size_t i = 0; i < order.size(); i++) {
        if (order[i] >= 0) {
            // If the difference between the matched whiskers in terms of position is too big, then unmatch them.
            float diffXf = abs(lineGatherer_detector[order[i]].position - lineGatherer_tracker[i].position);
            float diffA = abs(lineGatherer_detector[order[i]].angle - lineGatherer_tracker[i].angle);
            float diffB = abs(lineGatherer_detector[order[i]].shape - lineGatherer_tracker[i].shape);
            float diffL = abs(lineGatherer_detector[order[i]].length - lineGatherer_tracker[i].length);
            if (diffXf > trackOpts.maxChangeInXf || diffA > trackOpts.maxChangeInAngle
                || diffB > trackOpts.maxChangeInBend || diffL > trackOpts.maxChangeInLength) {
                order[i] = -1;
            }
        }
    }
    markMissedClustersAndWhiskers(lineGatherer_tracker, lineGatherer_detector, order, missedClusters, missedWhiskers);
}

// Function to find mean.
float mean(std::vector<float> &arr, int n) {
    float sum = 0;
    for (int i = 0; i < n; i++)
        sum = sum + arr[i];
    return (float) sum / n;
}
