/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_TRAINING_H
#define FWTS_C_TRAINING_H

#include "../whisker_forming/whisker_forming_options.h"
#include "tracking.h"

void training(const std::vector<whisker> &lineGatherer_tracker, std::vector<int> order, std::vector<classSVMData> &svmFeatures,
              int currFrame, int startFrame, track_opts &trackOpts, fwptd_opts fwptdOpts,
              train_opts &trainOpts, recog_opts &recogOpts, plotting_opts plottingOpts);

void allocateSVMFeatures(std::vector<classSVMData> &svmFeatures, int window);

void configureWhiskerDB(const std::vector<whisker> &lineGatherer_tracker, track_opts &trackOpts, fwptd_opts fwptdOpts,
                        const train_opts &trainOpts);

void computeNewMeanSTD(float new_value, float &mean, float &STD, int numValues);

void computeNewMean(float &mean, float new_value, int &numValues);

void computeNewSTD(float &STD, float new_value, float oldMean, float newMean, int numValues);

void prepareTrainingData(std::vector<classSVMData> &svmFeatures, const std::vector<std::vector<whiskerDB>> &whiskerDb,
                         train_opts &trainOpts, const track_opts &trackOpts, bool isTrain);

void insertDataInstance(size_t &k, classSVMData &svmFeatures, size_t window, const whiskerDB &whiskerDb,
                        const featuresVec featuresMean, const featuresVec featuresSTD);

void trainOnevsOneLinear(const std::vector<classSVMData> svmFeatures, const std::vector<whisker> &lineGatherer_tracker,
                         track_opts &trackOpts, const train_opts &trainOpts, recog_opts &recogOpts);

void storeLinearFeatures(problem &linearProblem, feature_node *feature_space, classSVMData svmFeatures, int cluster,
                         int &j, int &k, int chanceToTrain);

void copyLinearFeatures(problem &linearProblem, problem &linearProblemToCopy, int numInstances, int numFeatures,
                        int dst_offset, int src_offset);

void configLinearEps(parameter &param);

void defineOneVsOneLinear(recog_opts &recogOpts, feature_node *feature_space, int *numExamples, classSVMData svmFeatures1,
                          classSVMData svmFeatures2, int problemIdx, int chanceToTrain, bool copy1, bool copy2, int numFeatures);

void freeData(std::vector<classSVMData> &svmFeatures, track_opts &trackOpts);

void allocateLinearProblemOnevsOne(int window, int numOfWhiskers, int numOfFeatures, recog_opts &recogOpts);

void freeLinearProblem(std::vector<classSVMData> &svmFeatures, track_opts &trackOpts, recog_opts &recogOpts);

void print_array(const std::vector<float> whisker_value);

void print_whiskerDB(track_opts trackOpts);

void print_null(const char *s);

void train_accuracy_linear(std::vector<classSVMData> &svmFeatures, recog_opts recogOpts);

void train_accuracy_linearOneVsOne(const std::vector<classSVMData> &svmFeatures, recog_opts recogOpts);

#endif //FWTS_C_TRAINING_H
