/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "recognition.h"
#include "training.h"

void recognizer_N_expert(const std::vector<whisker> &lineGatherer_tracker, const std::vector<whisker> &lineGatherer_detector,
        std::vector<int> &order, std::vector<int> &missedClusters, std::vector<int> &missedWhiskers,
        recog_opts &recogOpts, N_expert_opts NExpertOpts, const track_opts &trackOpts, train_opts &trainOpts) {
    // Get differences between max and min values for all values from the previous frame.
    // This will help with normalization
    float normXf, normA, normL, normB;
    get_norm_values(lineGatherer_tracker, normXf, normA, normL, normB);
    Eigen::Array4f normArray;
    normArray << normXf, normA, normB, normL;

    // This will contain the data from the detected whiskers in this frame.
    std::vector<std::vector<whiskerDB>> currentClusters(lineGatherer_detector.size());
    // For every detected whisker in frame n get position, angle,
    // the averages of these values from the previous frame, length and shape
    for (size_t i = 0; i < currentClusters.size(); i++) {
        double sinx2_p, cosx2_p, sinx2_a, cosx2_a;
        sincos(lineGatherer_detector[i].angle, &sinx2_p, &cosx2_p);
        float cotx2_p = cosx2_p / sinx2_p;
        sincos(trackOpts.prevAverages.angle, &sinx2_a, &cosx2_a);
        float cotx2_a = cosx2_a / sinx2_a;
        currentClusters[i].push_back({lineGatherer_detector[i].position, lineGatherer_detector[i].angle, cotx2_p, trackOpts.prevAverages.position, cotx2_a,
                              lineGatherer_detector[i].length, lineGatherer_detector[i].shape, false});
    }

    // For every cluster: what is the most likely match from the whiskers from earlier frames?
    std::vector<int> choicePerCluster(lineGatherer_detector.size());

    // label each detected whisker as an existing cluster
    SVM_Recognition(choicePerCluster, lineGatherer_tracker, lineGatherer_detector, currentClusters, trainOpts,
                    recogOpts, trackOpts);

    // Prepare an array to fill in the final matching. Basically, what we want do,
    // is to convert the matching of new whiskers to old ones, into a matching of new whiskers to old ones.
    assignMatches(order, lineGatherer_tracker, lineGatherer_detector, choicePerCluster, normArray, recogOpts);

    // filter out matches of whiskers that are inaccurate
    N_expert(lineGatherer_tracker, lineGatherer_detector, order, NExpertOpts);

    markMissedClustersAndWhiskers(lineGatherer_tracker, lineGatherer_detector, order, missedClusters, missedWhiskers);
}

void get_norm_values(const std::vector<whisker> &lineGatherer, float &normXf, float &normA, float &normL, float &normB) {
    // Get the minimal and maximal for all the parameters from the previous frame.
    auto minmax_Xf_it = std::minmax_element(lineGatherer.begin(), lineGatherer.end(), cmp_Xf);
    float min_Xf = minmax_Xf_it.first->position;
    float max_Xf = minmax_Xf_it.second->position;
    auto minmax_angle_it = std::minmax_element(lineGatherer.begin(), lineGatherer.end(), cmp_angle);
    float min_angle = minmax_angle_it.first->angle;
    float max_angle = minmax_angle_it.second->angle;
    auto minmax_length_it = std::minmax_element(lineGatherer.begin(), lineGatherer.end(), cmp_length);
    float min_length = minmax_length_it.first->length;
    float max_length = minmax_length_it.second->length;
    auto minmax_shape_it = std::minmax_element(lineGatherer.begin(), lineGatherer.end(), cmp_shape);
    float min_shape = minmax_shape_it.first->shape;
    float max_shape = minmax_shape_it.second->shape;

    // Dividing the differences in parameters between the two frames by these
    // values will normalize them, which makes it possible to weight them properly
    normXf = max_Xf - min_Xf;
    normA = max_angle - min_angle;
    normL = max_length - min_length;
    normB = max_shape - min_shape;
}

bool cmp_Xf(const whisker& whisker1, const whisker& whisker2) {
    return whisker1.position < whisker2.position;
}

bool cmp_angle(const whisker& whisker1, const whisker& whisker2) {
    return whisker1.angle < whisker2.angle;
}

bool cmp_length(const whisker& whisker1, const whisker& whisker2) {
    return whisker1.length < whisker2.length;
}

bool cmp_shape(const whisker& whisker1, const whisker& whisker2) {
    return whisker1.shape < whisker2.shape;
}


void storeLinearTest(classSVMData testFeatures, feature_node *x, int index) {
    int j = 0;
    // store features
    x[j].index = 1;
    x[j++].value = testFeatures.features[index].x1_p;
    x[j].index = 2;
    x[j++].value = testFeatures.features[index].cotx2_p;
    x[j].index = 3;
    x[j++].value = testFeatures.features[index].x1_a;
    x[j].index = 4;
    x[j++].value = testFeatures.features[index].cotx2_a;
    x[j].index = 5;
    x[j++].value = testFeatures.features[index].L;
    x[j].index = 6;
    x[j++].value = testFeatures.features[index].x3;
    x[j].index = -1;
}

void SVM_Recognition(std::vector<int> &choicePerCluster, const std::vector<whisker> &lineGatherer_tracker,
                     const std::vector<whisker> &lineGatherer_detector, const std::vector<std::vector<whiskerDB>> currentClusters,
                     train_opts &trainOpts, recog_opts &recogOpts, const track_opts &trackOpts) {
    std::vector<classSVMData> svmFeatures(lineGatherer_detector.size());
    allocateSVMFeatures(svmFeatures, 1); // allocate memory for one frame's data
    // format testing data the same as training data
    prepareTrainingData(svmFeatures, currentClusters, trainOpts, trackOpts, false);

    // using libLinear
    feature_node *x = (feature_node *) malloc(7 * sizeof(struct feature_node)); // allocate memory for the prediction
    for (size_t k = 0; k < lineGatherer_detector.size(); k++) {
        storeLinearTest(svmFeatures[k], x, 0);
        // perform multi-class one-vs-one prediction
        predictOneVsOneLinear(x, choicePerCluster[k], recogOpts, lineGatherer_tracker.size());
    }
    free(x);
}

void assignMatches(std::vector<int> &order, const std::vector<whisker> &lineGatherer_tracker,
        const std::vector<whisker> &lineGatherer_detector, std::vector<int> choicePerCluster, Eigen::Array4f normArray,
        recog_opts &recogOpts) {
    order.assign(lineGatherer_tracker.size(), -1);
    for (size_t i = 0; i < lineGatherer_tracker.size(); i++) {
        std::vector<int> a; // How many options do we have per whisker?
        for (size_t j = 0; j < choicePerCluster.size(); j++) {
            if ((int) i == choicePerCluster[j]) a.push_back(j);
        }
        if (a.size() == 1) order[i] = a[0]; // if there's only one option, then that's the only choice, and we're done.
        else if (a.size() > 1) { // What if more than one newly detected whisker are recognized as the same whisker?
            Eigen::Array4f thisWhisker;
            thisWhisker << lineGatherer_tracker[i].position, lineGatherer_tracker[i].angle,
                    lineGatherer_tracker[i].shape, lineGatherer_tracker[i].length;
            Eigen::Array4f detectedWhisker;
            float minDiff = FLT_MAX;
            int finalChoice = -1;
            // Then we choose the cluster that is most similar to the matched whisker.
            for (size_t j = 0; j < a.size(); j++) {
                detectedWhisker << lineGatherer_detector[a[j]].position, lineGatherer_detector[a[j]].angle,
                        lineGatherer_detector[a[j]].shape, lineGatherer_detector[a[j]].length;
                Eigen::Array4f diffs;
                diffs = (thisWhisker - detectedWhisker).abs() / normArray;
                float weightedDiff = recogOpts.weightX * pow(diffs(0),2) + recogOpts.weightA * pow(diffs(1),2)
                                     + recogOpts.weightB * pow(diffs(2),2) + recogOpts.weightL * pow(diffs(3),2);
                if (weightedDiff < minDiff) {
                    minDiff = weightedDiff;
                    finalChoice = a[j];
                }
            }
            order[i] = finalChoice;
        }
        else {
            if (recogOpts.verbose) std::cout << "Could not fit whisker " << i << "." << std::endl;// No newly detected whiskers recognized as this whisker?
        }
    }
}

void N_expert(const std::vector<whisker> &lineGatherer_tracker, const std::vector<whisker> &lineGatherer_detector,
        std::vector<int> &order, N_expert_opts NExpertOpts) {
    for (size_t i = 0; i < lineGatherer_tracker.size(); i++) {
        if (order[i] >= 0) {
            Eigen::Array4f diffs;
            diffs << abs(lineGatherer_tracker[i].position-lineGatherer_detector[order[i]].position),
                    abs(lineGatherer_tracker[i].angle-lineGatherer_detector[order[i]].angle),
                    abs(lineGatherer_tracker[i].shape-lineGatherer_detector[order[i]].shape),
                    abs(lineGatherer_tracker[i].length-lineGatherer_detector[order[i]].length);
            if (diffs(0) > NExpertOpts.maxChangeInXf || diffs(1) > NExpertOpts.maxChangeInAngle
                || diffs(2) > NExpertOpts.maxChangeInBend || diffs(3) > NExpertOpts.maxChangeInLength) {
                order[i] = -1;
                if (NExpertOpts.verbose) std::cout << "Fit of whisker " << i << " is estimated as not accurate by N-expert." << std::endl;
            }

        }
    }
}

void predictOneVsOneLinear(feature_node *x, int &choice, recog_opts &recogOpts, int tracker_size) {
    std::vector<int> before(tracker_size); // All the 'candidates' for recognition
    for (size_t i = 0; i < before.size(); i++) {
        before[i] = i;
    }
    std::vector<int> after = {1, 1}; // initialize to be larger than 1 in size
    while (after.size() > 1) { // While the final choice hasn't been made yet
        // Divide the candidates into two groups, so that we can make pairs of candidates
        std::vector<int> option1;
        std::vector<int> option2;
        for (size_t i = 0; i < before.size(); i++) {
            if (i%2) option2.push_back(before[i]);
            else option1.push_back(before[i]);
        }

        size_t Lo1 = option1.size();
        size_t Lo2 = option2.size();
        after.resize(std::max(Lo1, Lo2)); // This array will contain all the candidates that are still left.
        // For every pair of candidates: which one is more likely for this (unknown) whisker?
        for (size_t i = 0; i < std::min(Lo1, Lo2); i++) {
            int classifier = option1[i]*tracker_size - option1[i]*(option1[i]+1)/2 + option2[i] - option1[i] - 1;
            after[i] = (int) predict(&recogOpts.linearModel[classifier], x);
        }
        //  if the groups were not of equal size (when the number of candidates is odd),
        //  just add the `leftover' whisker to the pool of candidates and test it next iteration.
        if (Lo1 > Lo2) after[after.size()-1] = option1[option1.size() - 1];
        else if (Lo1 < Lo2) after[after.size()-1] = option2[option2.size() - 1];
        before = after;
    }
    choice = after[0]; // When the choice is made, fill it in in this array
}
