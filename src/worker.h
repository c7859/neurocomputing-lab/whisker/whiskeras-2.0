/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WORKER_H
#define WORKER_H

#include <opencv2/opencv.hpp>
#include <QThread>

#include "src/utils/video.h"
#include "src/stages/whisker_point_detection/preprocessing.h"
#include "src/stages/whisker_point_detection/c_extraction.h"
#include "src/stages/whisker_forming/parametrization.h"
#include "src/utils/plotting.h"
#include "src/options/options.h"
#include "src/stages/whisker_forming/whisker_forming_options.h"
#include "src/cuda/c_extraction_cuda.h"
#include "src/utils/profile.h"
#include "src/pipeline/pipeline.h"
#include "src/stages/tracking_over_time/tracking.h"
#include "src/utils/dump.h"

#ifdef USE_CUDA
#include "src/pipeline/pipeline_cuda.h"
#endif


class Worker : public QObject
{
    Q_OBJECT

public:
    void setupWorkerProcessing(QThread &workerThread) {
        connect(&workerThread, SIGNAL(started()), this, SLOT(processWhiskerVideo()));
    }

#ifdef USE_CUDA
    void processUsingGPU(timers timers, cv::Mat overlaid_whiskers);
#endif
    void processUsingCPU(timers timers, cv::Mat overlaid_whiskers);

    // options
    general_opts generalOpts; // general options
    dump_opts dumpOpts; // dumping options
    plotting_opts plottingOpts; // plotting options
    preprocessing_opts preprocessingOpts; // preprocessing parameters
    c_extraction_opts cExtractionOpts; // centerline extraction parameters
    clustering_opts clusteringOpts; // local clustering parameters
    stitch_I_opts stitchIOpts; // cluster stitching (I) parameters (stitch the clusters together)
    stitch_II_opts stitchIIOpts; // cluster stitching (II) parameters (detect overlapping centerlines)
    param_opts paramOpts; // parametrization parameters
    track_opts trackOpts; // tracking options
    fwptd_opts fwptdOpts; // fit whisker-points-to-data (whiskers) options
    train_opts trainOpts; // training options
    recog_opts recogOpts; // recognition options
    N_expert_opts NExpertOpts; // N-Expert options
    cv::VideoCapture video; // whisker video
    int numFrames;
    // background and snout silhouette
    cv::Mat bgImage, bwShape;
#ifdef USE_CUDA
    c_extraction_cuda cExtractionCuda; // centerline extraction struct to accomodate CUDA storage data
    cv::cuda::GpuMat bgImage_d, bwShape_d, preprocessedImage_d;
#endif


public slots:
    void processWhiskerVideo();


signals:
    void newFrame(int val);
};



#endif // WORKER_H
