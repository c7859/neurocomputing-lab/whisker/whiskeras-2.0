/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv2/opencv.hpp>
#include <QLabel>
#include <QMouseEvent>
#include <QDebug>
#include <QThread>
#include <QScrollArea>
#include <QResizeEvent>
#include "snoutlinewindow.h"

#include "src/utils/video.h"
#include "src/stages/whisker_point_detection/preprocessing.h"
#include "src/stages/whisker_point_detection/c_extraction.h"
#include "src/stages/whisker_forming/parametrization.h"
#include "src/utils/plotting.h"
#include "src/options/options.h"
#include "src/stages/whisker_forming/whisker_forming_options.h"
#include "src/cuda/c_extraction_cuda.h"
#include "src/utils/profile.h"
#include "src/pipeline/pipeline.h"
#include "src/stages/tracking_over_time/tracking.h"
#include "src/utils/dump.h"
#include "worker.h"

#ifdef USE_CUDA
#include "src/pipeline/pipeline_cuda.h"
#endif

namespace Ui {
class MainWindow;
//class previewLabel;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void initLimitsAndSliders();

    void videoProps2Options();

    void getBackgroundForPreview();

    void previewMode();

    void getOptionsAndConfigUI();

    void configUI();

    std::string getFilenameFromPath(std::string path, std::string default_str, size_t bytes_per_char);

    std::string removeFilenameExtension(std::string filename);

    void initializeOptions();

    void setHistEqLimits();

    void setupProcessingThread(QThread &thread);

    void saveSettings();

    void loadSettings();

    void resizeEvent(QResizeEvent *event);

    void closeEvent(QCloseEvent *event);

    // options
    general_opts generalOpts; // general options
    dump_opts dumpOpts; // dumping options
    plotting_opts plottingOpts; // plotting options
    preprocessing_opts preprocessingOpts; // preprocessing parameters
    c_extraction_opts cExtractionOpts; // centerline extraction parameters
    clustering_opts clusteringOpts; // local clustering parameters
    stitch_I_opts stitchIOpts; // cluster stitching (I) parameters (stitch the clusters together)
    stitch_II_opts stitchIIOpts; // cluster stitching (II) parameters (detect overlapping centerlines)
    param_opts paramOpts; // parametrization parameters
    track_opts trackOpts; // tracking options
    fwptd_opts fwptdOpts; // fit whisker-points-to-data (whiskers) options
    train_opts trainOpts; // training options
    recog_opts recogOpts; // recognition options
    N_expert_opts NExpertOpts; // N-Expert options
    cv::VideoCapture video; // whisker video
    int numFrames, videoWidth, videoHeight;
    // background and snout silhouette
    cv::Mat bgImage, bwShape;
#ifdef USE_CUDA
    c_extraction_cuda cExtractionCuda; // centerline extraction struct to accomodate CUDA storage data
    cv::cuda::GpuMat bgImage_d, bwShape_d, preprocessedImage_d;
#endif
    int worker_startFrame, worker_endFrame;

    // Qt items
    SnoutLineWindow *snoutLineWindow;

    QString default_stylesheet;

    QScrollArea *scrollArea;

    QThread workerThread;

    bool processing = false;

    bool snoutLineDrawn = false;


private slots:
    void on_beginProcessing_button_clicked();

    void enableProcessingButton();

    void on_SnoutLineButton_clicked();

    void on_drawSnoutLine_advanced_button_clicked();

    void onBgFramesChanged(int first, int last);

    void on_openWhiskerVideo_triggered();

    void on_previewFrame_slider_valueChanged(int value);

    void on_useGPU_checkBox_clicked(bool checked);

    void on_bwThreshold_spinBox_valueChanged(int arg1);

    void on_gaussianSigma_spinBox_valueChanged(double arg1);

    void on_dilateValue_spinBox_valueChanged(int arg1);

    void on_deinterlacingFlag_checkBox_clicked(bool checked);

    void on_histEqMinT_spinBox_valueChanged(int arg1);

    void on_histEqMaxT_spinBox_valueChanged(int arg1);

    void on_histEqGamma_spinBox_valueChanged(double arg1);

    void on_removeLowLevels_spinBox_valueChanged(int arg1);

    void on_framing_spinBox_valueChanged(int arg1);

    void on_upscaling_spinBox_valueChanged(double arg1);

    void on_cutoff_spinBox_valueChanged(int arg1);

    void on_tr2_spinBox_valueChanged(double arg1);

    void on_sigma_spinBox_valueChanged(double arg1);

    void on_gaussianKernelSize_spinBox_valueChanged(int arg1);

    void on_steger_c_spinBox_valueChanged(double arg1);

    void on_maxAngle_spinBox_valueChanged(double arg1);

    void on_M_spinBox_valueChanged(int arg1);

    void on_K_spinBox_valueChanged(int arg1);

    void on_L_spinBox_valueChanged(int arg1);

    void on_beamThreshold_spinBox_valueChanged(int arg1);

    void on_coneMin_spinBox_valueChanged(double arg1);

    void on_coneMax_spinBox_valueChanged(double arg1);

    void on_coneMinDist_spinBox_valueChanged(double arg1);

    void on_c_spinBox_valueChanged(double arg1);

    void on_maxAD_spinBox_valueChanged(double arg1);

    void on_LengthOfTipAndBottom_spinBox_valueChanged(double arg1);

    void on_radonLength_spinBox_valueChanged(int arg1);

    void on_radonThreshold_spinBox_valueChanged(double arg1);

    void on_minClusterSize_I_spinBox_valueChanged(int arg1);

    void on_highestRoot_spinBox_valueChanged(double arg1);

    void on_acceptableXDistance_spinBox_valueChanged(int arg1);

    void on_LengthOfTipAndBottom_II_spinBox_valueChanged(double arg1);

    void on_cone_spinBox_valueChanged(double arg1);

    void on_maxAD_II_spinBox_valueChanged(double arg1);

    void on_minClusterSize_II_spinBox_valueChanged(int arg1);

    void on_minNrOfDots_spinBox_valueChanged(int arg1);

    void on_minLength_spinBox_valueChanged(int arg1);

    void on_minAngleToSnout_spinBox_valueChanged(double arg1);

    void on_maxBend_spinBox_valueChanged(double arg1);

    void on_useKalman_checkBox_clicked(bool checked);

    void on_kalmanDistance_spinBox_valueChanged(int arg1);

    void on_weightB_spinBox_valueChanged(double arg1);

    void on_weightL_spinBox_valueChanged(double arg1);

    void on_weightXf_spinBox_valueChanged(double arg1);

    void on_weightAlpha_spinBox_valueChanged(double arg1);

    void on_dontTrack_checkBox_clicked(bool checked);

    void on_maxChangeInAngle_spinBox_valueChanged(double arg1);

    void on_maxChangeInXf_spinBox_valueChanged(double arg1);

    void on_maxChangeInBend_spinBox_valueChanged(double arg1);

    void on_maxChangeInLength_spinBox_valueChanged(double arg1);

    void on_maxScoreForMatch_spinBox_valueChanged(double arg1);

    void on_retrainFreqInit_spinBox_valueChanged(int arg1);

    void on_retrainAtLeast_spinBox_valueChanged(int arg1);

    void on_increaseWith_spinBox_valueChanged(int arg1);

    void on_increaseEvery_spinBox_valueChanged(int arg1);

    void on_bootstrap_spinBox_valueChanged(int arg1);

    void on_window_spinBox_valueChanged(int arg1);

    void on_maxSizeTrainingData_spinBox_valueChanged(int arg1);

    void on_chance_train_spinBox_valueChanged(double arg1);

    void on_weightB_spinBox_2_valueChanged(double arg1);

    void on_weightL_spinBox_2_valueChanged(double arg1);

    void on_weightXf_spinBox_2_valueChanged(double arg1);

    void on_weightAlpha_spinBox_2_valueChanged(double arg1);

    void on_maxChangeInBend_spinBox_2_valueChanged(double arg1);

    void on_maxChangeInLength_spinBox_2_valueChanged(double arg1);

    void on_maxChangeInAngle_spinBox_2_valueChanged(double arg1);

    void on_maxChangeInXf_spinBox_2_valueChanged(double arg1);

    void on_startFrame_slider_valueChanged(int value);

    void on_startFrame_spinBox_valueChanged(int arg1);

    void on_endFrame_slider_valueChanged(int value);

    void on_endFrame_spinBox_valueChanged(int arg1);

    void on_results_dir_button_clicked();

    void on_saveOptions_button_clicked();

    void on_loadOptions_button_clicked();

    void on_preview_histEqGamma_slider_valueChanged(int value);

    void on_preview_dilateValue_slider_valueChanged(int value);

    void on_preview_minClusterSize_I_slider_valueChanged(int value);

    void on_preview_minClusterSize_II_slider_valueChanged(int value);

    void on_preview_minLength_slider_valueChanged(int value);

    void on_preview_histEqGamma_spinBox_valueChanged(double arg1);

    void on_preview_dilateValue_spinBox_valueChanged(int arg1);

    void on_preview_minClusterSize_I_spinBox_valueChanged(int arg1);

    void on_preview_minClusterSize_II_spinBox_valueChanged(int arg1);

    void on_preview_minLength_spinBox_valueChanged(int arg1);

    void on_frameProcessed(int val);

    void on_videoOutput_checkBox_clicked(bool checked);

    void on_videoOutputDir_button_clicked();

public slots:
    void snoutLineDialog_accepted();

private:
    Ui::MainWindow *ui;
    bool videoOpened = false;

};

#endif // MAINWINDOW_H
