/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <opencv2/core/mat.hpp>
#include <iostream>
#include "evaluation.h"
#include <fstream>


void writeDetectedWhiskersNr(std::vector<int> &whiskers_detected_Nr, int startFrame, int endFrame, int videoStart, std::string outputDir) {

    std::vector<int> X, whiskers_detected_window;
    int whiskers_nr_max = 0;
    int whiskers_nr_min = 100;
    std::vector<int> whisker_hist(100);
    for (int i = 0; i + startFrame < endFrame-1; i++) {
        X.push_back(i + (startFrame - videoStart));
        whiskers_detected_window.push_back(whiskers_detected_Nr[i+(startFrame-videoStart)]);
        // find max and min number of whiskers found in all frames
        if (whiskers_detected_Nr[i+(startFrame-videoStart)] > whiskers_nr_max)
            whiskers_nr_max = whiskers_detected_Nr[i+(startFrame-videoStart)];
        if (whiskers_detected_Nr[i+(startFrame-videoStart)] < whiskers_nr_min)
            whiskers_nr_min = whiskers_detected_Nr[i+(startFrame-videoStart)];
        // add to <number of whiskers found> histogram
        whisker_hist[whiskers_detected_Nr[i+(startFrame-videoStart)]]++;
    }

    std::ofstream FILE;
    std::string textLoc = outputDir + "number_of_detected_whiskers_per_frame_histogram.csv";
    FILE.open(textLoc, std::ios::out);
    FILE << "Whiskers detected," << "Number of frames" "\n";
    for (int i = whiskers_nr_min; i < whiskers_nr_max; i++) {
        FILE << i << "," << whisker_hist[i] << "\n";
    }
    FILE.close();
}

void writeDetectionRates(track_opts trackOpts, std::string outputDir) {
    std::ofstream detectionRatioFILE;
    detectionRatioFILE.open(outputDir + "detection_ratios.csv",std::ios::out);
    float average_ratio = 0;
    detectionRatioFILE << "Whisker Nr.," << "Detection Ratio" << "\n";
    for (size_t i = 0; i < trackOpts.whiskerDb.size(); i++) {
        float sum = 0;
        for (size_t j = 0; j < trackOpts.whiskerDb[i].size(); j++) {
            if (!trackOpts.whiskerDb[i][j].cnf) {
                sum += 1;
            }
        }
        float ratio = sum / trackOpts.whiskerDb[i].size();
        detectionRatioFILE << "Whisker "<< i+1 << "," << ratio << "\n";
        average_ratio += ratio;
    }
    average_ratio /= trackOpts.whiskerDb.size();
    detectionRatioFILE << "Average," << average_ratio << "\n";
    detectionRatioFILE.close();
}

void writeWhiskerAnglesToCSV(std::vector<std::vector<float>> &whisker_angles, int numTrackedWhiskers, int startFrame,
                             int endFrame, int videoStart, std::string outputPath) {
    // create CSV file
    std::ofstream anglesFile;
    anglesFile.open(outputPath + "_angles.csv", std::ios::out);

    // frame indices headers
    anglesFile << "Frame" << ",";

    // whiskers headers
    for (int i = 0; i < numTrackedWhiskers; i++) {
        anglesFile << "Whisker " << std::to_string(i+1);
        if (i + 1 < numTrackedWhiskers) anglesFile << ","; // don't insert blank column
    }
    anglesFile << "\n";
    for (int j = 0; j + startFrame < endFrame; j++) {
        anglesFile << (j + startFrame + 1) << ",";
        for (int i = 0; i < numTrackedWhiskers; i++) {
            if (whisker_angles[i][j+(startFrame-videoStart)] != -1) {
                anglesFile << std::to_string(whisker_angles[i][j + (startFrame - videoStart)]);
            }
            else {
                anglesFile << std::to_string(NAN);
            }
            if (i + 1 < numTrackedWhiskers) anglesFile << ","; // don't insert blank column
        }
        anglesFile << "\n";
    }
    anglesFile.close();
}

void addAngles(std::vector<std::vector<float>> &whisker_angles, std::vector<whisker> &lineGatherer_tracker, std::vector<bool> &couldNotFit) {
    whisker_angles.resize(lineGatherer_tracker.size());
    for (size_t k = 0; k < lineGatherer_tracker.size(); k++) {
        if (!couldNotFit[k]) {
            whisker_angles[k].push_back(lineGatherer_tracker[k].angle);
        }
        else {
            whisker_angles[k].push_back(-1);
        }
    }
}

void clearAngles(std::vector<std::vector<float>> &whisker_angles, int numTrackedWhiskers) {
    for (int i = 0; i < numTrackedWhiskers; i++) {
        whisker_angles[i].clear();
    }
    whisker_angles.clear();
}

void addWhiskers(std::vector<std::vector<whisker>> &whiskers, std::vector<whisker> &lineGatherer_tracker, std::vector<bool> &couldNotFit) {
    whiskers.resize(whiskers.size()+1); // increment whiskers size
    whiskers[whiskers.size()-1].resize(lineGatherer_tracker.size());
    for (size_t k = 0; k < lineGatherer_tracker.size(); k++) {
        whiskers[whiskers.size()-1][k] = lineGatherer_tracker[k];
        if (couldNotFit[k]) {
            whiskers[whiskers.size()-1][k].angle = -1; // mark as not fit
        }
    }
}

void writeWhiskersToCSV(std::vector<std::vector<whisker>> &whiskers, int numTrackedWhiskers, int startFrame,
                             int endFrame, int videoStart, std::string outputPath) {
    // create CSV file
    std::ofstream whiskersFile;
    whiskersFile.open(outputPath + "_complete_whiskers.csv", std::ios::out);

    // frame indices headers
    whiskersFile << "Frame";

    // whiskers headers
    for (int i = 0; i < numTrackedWhiskers; i++) {
        whiskersFile << ",Xf " << std::to_string(i+1);
        whiskersFile << ",Angle " << std::to_string(i+1);
        whiskersFile << ",Length " << std::to_string(i+1);
        whiskersFile << ",Shape " << std::to_string(i+1);
    }
    whiskersFile << "\n";
    for (int j = 0; j + startFrame < endFrame; j++) {
        whiskersFile << (j + startFrame + 1);
        for (int i = 0; i < numTrackedWhiskers; i++) {
            if (whiskers[j+(startFrame-videoStart)][i].angle != -1) {
                whiskersFile << "," << std::to_string(whiskers[j + (startFrame - videoStart)][i].position) << ","
                << std::to_string(whiskers[j + (startFrame - videoStart)][i].angle) << ","
                << std::to_string(whiskers[j + (startFrame - videoStart)][i].length) << ","
                << std::to_string(whiskers[j + (startFrame - videoStart)][i].shape);
            }
            else {
                whiskersFile << "," << std::to_string(NAN) << "," << std::to_string(NAN) << "," << std::to_string(NAN) << "," << std::to_string(NAN);
            }
//            if (i + 1 < numTrackedWhiskers) whiskersFile << ","; // don't insert blank column at the end of whiskers
        }
        whiskersFile << "\n";
    }
    whiskersFile.close();
}
