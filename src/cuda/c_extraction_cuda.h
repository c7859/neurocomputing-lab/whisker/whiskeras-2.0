/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_C_EXTRACTION_CUDA_H
#define FWTS_C_C_EXTRACTION_CUDA_H

#include <opencv2/opencv.hpp>
// cuda
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/cudawarping.hpp>
#include "../stages/whisker_point_detection/c_extraction.h"

struct c_extraction_cuda {
    int width;
    int height;
    int precision;
    float *gxCol, *gxRow;
    float *gxxCol, *gxxRow;
    float *gxyCol, *gxyRow;
    float *gyCol, *gyRow;
    float *gyyCol, *gyyRow;
    float *rx, *ry, *rxx, *ryy, *rxy;
    float *gaussianBuffer; // buffer to hold intermediate result between two (separable) filters
    float *egv, *whiskerAngles; // GPU arrays to hold second derivatives and whisker angles
    float *d_preprocessedImage;
    // necessary CUB structs
    float *egv_compact;
    int32_t *startPoints;
    int32_t *startPoints_compact;
    uint8_t *flags_startPoints;
    int *numStartPoints;
    void *flagged_storage = NULL;
    size_t flagged_storage_bytes = 0;
    int32_t *startPoints_sorted;
    float *egv_sorted;
    float *preprocessedImageData;
    float *bgImageData;
    float *bwShapeData;
};

void steger_point_detection_cuda(std::vector<std::pair<int16_t,int16_t>> &startPoints, const c_extraction_opts &cExtractionOpts,
                                 c_extraction_cuda cExtractionCuda);

void performSeparableConvolution(float *d_Input, float *d_filterRow, float *d_filterCol, float *result,
                                 c_extraction_cuda cExtractionCuda, int rowSize, int colSize, int pitch, int offset,
                                 int maxKernelSize);

void allocateCudaStructs(c_extraction_cuda &cExtractionCuda, c_extraction_opts &cExtractionOpts);

void freeCudaStructs(c_extraction_cuda &cExtractionCuda, c_extraction_opts &cExtractionOpts);

void transferResults(c_extraction_cuda cExtractionCuda, float *whiskerAngles, float *preprocessedImage);

void convertCudaMatToArr(cv::cuda::GpuMat src, c_extraction_cuda &cExtractionCuda);

#endif //FWTS_C_C_EXTRACTION_CUDA_H
