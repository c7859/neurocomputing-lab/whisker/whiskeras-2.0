/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "cuda_kernels.h"
#include "../stages/whisker_point_detection/imgproc.h"

cv::cuda::GpuMat dilation_cuda(cv::cuda::GpuMat frame, int dilation_size, int dilation_elem) {
    int dilation_type;
    if (dilation_elem == 0 ) { dilation_type = cv::MORPH_RECT; }
    else if (dilation_elem == 1) { dilation_type = cv::MORPH_CROSS; }
    else if (dilation_elem == 2) { dilation_type = cv::MORPH_ELLIPSE; }

    cv::Mat element = cv::getStructuringElement(dilation_type,
                                                cv::Size(2*dilation_size + 1, 2*dilation_size+1 ),
                                                cv::Point(dilation_size, dilation_size));
    cv::cuda::GpuMat dilated;
    cv::Ptr<cv::cuda::Filter> dilateFilter = cv::cuda::createMorphologyFilter(cv::MORPH_DILATE, frame.type(), element);
    // Apply the dilation operation
    dilateFilter->apply(frame, dilated);

    return dilated;
}

cv::cuda::GpuMat erosion_cuda(cv::cuda::GpuMat frame, int erosion_size, int erosion_elem) {
    int erosion_type;
    if (erosion_elem == 0 ) { erosion_type = cv::MORPH_RECT; }
    else if (erosion_elem == 1) { erosion_type = cv::MORPH_CROSS; }
    else if (erosion_elem == 2) { erosion_type = cv::MORPH_ELLIPSE; }

    cv::Mat element = cv::getStructuringElement(erosion_type,
                                                cv::Size(2*erosion_size + 1, 2*erosion_size+1 ),
                                                cv::Point(erosion_size, erosion_size));
    cv::cuda::GpuMat eroded;
    cv::Ptr<cv::cuda::Filter> erodeFilter = cv::cuda::createMorphologyFilter(cv::MORPH_ERODE, frame.type(), element);
    // Apply the dilation operation
    erodeFilter->apply(frame, eroded);

    return eroded;
}

__global__ void deinterlaceLineRepImage_cuda(cv::cuda::PtrStepSzb src) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = 2*(blockIdx.y * blockDim.y + threadIdx.y) + 1;

    if ((y >= src.rows) || (x >= src.cols)) return;

    int step = src.step/sizeof(uchar);
    src[y*step + x] = src[(y-1)*step + x];
}

__global__ void imadjust_cuda(cv::cuda::PtrStepSzb src, double in0, double in1, double out0, double out1, float gamma) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if ((y >= src.rows) || (x >= src.cols)) return;

    double div_factor = in1 - in0;
    double mul_factor = out1 - out0;

    double dsrc = uchar2double_cuda(src(y, x));
    // make sure intensity_src is in the range (in[0], in[1])
    double ddst = dsrc < in0 ? in0 : dsrc >= in1 ? in1 : dsrc;
    ddst = powf( (ddst - in0) / div_factor, gamma);
    ddst = ddst * mul_factor + out0;
    src(y, x) = double2uchar_cuda(ddst);
}

__global__ void imadjust_removeEdge_cuda(cv::cuda::PtrStepSzb src, cv::cuda::PtrStepSzf dst,
        double in0, double in1, double out0, double out1, float gamma, int framing) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if ((y >= src.rows) || (x >= src.cols)) return;

    double div_factor = in1 - in0;
    double mul_factor = out1 - out0;

    float dsrc = (float) src(y, x) / 255.0;
    // make sure intensity_src is in the range (in[0], in[1])
    float ddst = dsrc < in0 ? in0 : dsrc >= in1 ? in1 : dsrc;
    ddst = powf( (ddst - in0) / div_factor, gamma);
    ddst = ddst * mul_factor + out0;
//    src(y, x) = double2uchar_cuda(ddst);
    dst(y, x) = ddst * 255.0;

    if ((x <= framing) || (y <= framing) || (x >= src.cols - framing) || (y >= src.rows - framing)) {
        dst(y, x) = 0;
    }
}

__device__ double uchar2double_cuda(uchar value) {
    double dval = double(value) / 255;
    return dval;
}

__device__ uchar double2uchar_cuda(double value) {
    uchar uval = lround(value * 255);
    return uval;
}

__global__ void remove_edge_cuda(cv::cuda::PtrStepSzb src, int framing) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if ((y >= src.rows) || (x >= src.cols)) return;

    if ((x <= framing) || (y <= framing) || (x >= src.cols - framing) || (y >= src.rows - framing)) {
        src(y, x) = 0;
    }
}

__global__ void invertImgColors(cv::cuda::PtrStepSzb src) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if (x >= src.cols || y >= src.rows) return;

    src(y,x) = 255 - src(y,x);
}

void showImage_cuda(cv::cuda::GpuMat img_d, cv::String label, bool gray, plotting_opts plottingOpts) {
    cv::Mat img;
    img_d.download(img);
    showImage(img, label, plottingOpts.framing, gray, plottingOpts);
}

void showImageWithSnout_cuda(cv::cuda::GpuMat img_d, clustering_opts clusteringOpts, cv::String label) {
    cv::Mat img;
    img_d.download(img);
//    showImageWithSnout(img, clusteringOpts, label);
}

__global__ void stegerDetection_cuda(int32_t *startPoints, uint8_t *flags_startPoints, float *egvMat, float *whiskerAngles,
                                     float *rx, float *ry, float *a, float *b, float *d, float a_value, float b_value,
                                     int rows, int cols, float tr2, bool upside_whiskers) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    // check boundaries
    if ((x >= cols) || (y >= rows)) return;

    float T = a[y*cols+x] + d[y*cols+x]; // Trace
    float b_sq = b[y*cols+x] * b[y*cols+x];
    float D = a[y*cols+x] * d[y*cols+x] - b_sq; // Determinant

    // eigenvalues (complex)
    float discr = T*T/4 - D;
    float real = sqrt((discr > 0) ? discr : 0);
    float im = sqrt((discr < 0) ? -discr : 0);
    float egv1_real = T/2 + real;
    float egv1_im = im;
    float egv2_real = T/2 - real;
    float egv2_im = -im;

    // eigenvalues' magnitude for comparison
    float egv1_abs = egv1_real*egv1_real+egv1_im*egv1_im;
    float egv2_abs = egv2_real*egv2_real+egv2_im*egv2_im;

    float egv;
    if (egv1_abs > egv2_abs) {
        egv = egv1_real;
    }
    else {
        egv = egv2_real;
    }

    float ev1 = egv - d[y*cols+x];
    float n = sqrt(ev1*ev1 + b_sq);
    ev1 = ev1 / n;
    float b1 = b[y*cols+x] / n;
    b_sq = b1*b1;

    float t = (rx[y*cols+x]*ev1 + ry[y*cols+x]*b1) / (a[y*cols+x]*ev1*ev1 + 2*b_sq*ev1 + d[y*cols+x]*b_sq);
    float xComponent_t = -t*ev1;
    float yComponent_t = -t*b1;

    float absX, absY;
    absX = (xComponent_t >= 0) ? xComponent_t : -xComponent_t;
    absY = (yComponent_t >= 0) ? yComponent_t : -yComponent_t;
    int hereIsAWhisker = ((absX < 0.5) & (absY < 0.5) & (egv < 0));

    // set second derivative to zero at non-whisker points and compute whisker angles
    // also mask points above the snout
    if (hereIsAWhisker == 0
            || (!upside_whiskers && x*a_value + b_value >= y)
            || (upside_whiskers && x*a_value + b_value <= y)) {
        egvMat[y*cols+x] = 0; // could be anything >= 0
        whiskerAngles[y*cols+x] = FLT_MAX;
    }
    else { //(hereIsAWhisker > 0 && x*a_value + b_value < y) {
        egvMat[y*cols+x] = egv;
//        float angle = atan2(yComponent_t, xComponent_t) * 180 / CV_PI; // this produces values << 0 for some reason
        float angle = atan(yComponent_t/xComponent_t) * 180 / CV_PI;
        // angles in the 4th quadrant are transferred to the 2nd quadrant
        if (angle < 0) angle += 180.0;
        whiskerAngles[y*cols+x] = angle;
        // save whisker starting points' positions
        if (egv < tr2) {
            // place y in lower 16 bits and x in upper 16 bits
            startPoints[y*cols+x] = ((x & 0xFFFF) << 16) | (y & 0xFFFF);
            flags_startPoints[y*cols+x] = 1;
        }
    }
}

__global__ void matToArr(cv::cuda::PtrStepSzf mat, float *arr, int width, int height) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;

    if (x >= width || y >= height) return;

    arr[y*width + x] = mat(y, x);
}

__global__ void printArr(float *arr, int width, int height) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;

    if (x >= width || y >= height) return;

    printf("arr[%i,%i] = %f", x,y,arr[y*width + x]);
}
