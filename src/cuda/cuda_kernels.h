/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_CUDA_KERNELS_H
#define FWTS_C_CUDA_KERNELS_H

#pragma once

#include <opencv2/opencv.hpp>
// cuda
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/cudawarping.hpp>
#include "../stages/whisker_forming/whisker_forming_options.h"
#include "../utils/plotting.h"

cv::cuda::GpuMat dilation_cuda(cv::cuda::GpuMat frame, int dilation_size, int dilation_elem);

void showImage_cuda(cv::cuda::GpuMat img_d, cv::String label, bool gray, plotting_opts plottingOpts);

void showImageWithSnout_cuda(cv::cuda::GpuMat img_d, clustering_opts clusteringOpts, cv::String label);

cv::cuda::GpuMat erosion_cuda(cv::cuda::GpuMat frame, int erosion_size, int erosion_elem);

__global__ void deinterlaceLineRepImage_cuda(cv::cuda::PtrStepSzb src);

// adjust range
__global__ void imadjust_cuda(cv::cuda::PtrStepSzb src, double in0, double in1, double out0, double out1, float gamma);

// adjust range and convert image to float32
__global__ void imadjust_removeEdge_cuda(cv::cuda::PtrStepSzb src, cv::cuda::PtrStepSzf dst,
                                         double in0, double in1, double out0, double out1, float gamma, int framing);

__global__ void invertImgColors(cv::cuda::PtrStepSzb src);

__device__ double uchar2double_cuda(uchar value);

__device__ uchar double2uchar_cuda(double value);

__global__ void remove_edge_cuda(cv::cuda::PtrStepSzb src, int framing);

__global__ void stegerDetection_cuda(int32_t *startPoints, uint8_t *flags_startPoints, float *egvMat, float *whiskerAngles,
                                     float *rx, float *ry, float *a, float *b, float *d, float a_value, float b_value,
                                     int rows, int cols, float tr2, bool upside_whiskers);

__global__ void matToArr(cv::cuda::PtrStepSzf mat, float *arr, int width, int height);

__global__ void printArr(float *arr, int width, int height);

#endif //FWTS_C_CUDA_KERNELS_H
