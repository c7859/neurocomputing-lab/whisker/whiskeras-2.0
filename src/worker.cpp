/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "worker.h"
#include <fstream>
#include "src/evaluation/evaluation.h"
#include "src/pipeline/pipeline_cuda.h"

void Worker::processWhiskerVideo()
{
    trackOpts.dontTrack = false;
    trackOpts.plot = false;

    int numFrames = video.get(cv::CAP_PROP_FRAME_COUNT);
    timers timers; // measure timing profile
    timers.pipeline_timer.start();
    cv::Mat overlaid_whiskers;
    if (generalOpts.use_cuda) {
#ifdef USE_CUDA
        allocateCudaStructs(cExtractionCuda, cExtractionOpts);
        getBackground_cuda(bgImage_d, bwShape_d, preprocessedImage_d, video, dumpOpts, generalOpts, preprocessingOpts, cExtractionCuda);
        // run on GPU
        // use this function in order to make use of QThread's interruption
        processUsingGPU(timers, overlaid_whiskers);
        freeCudaStructs(cExtractionCuda, cExtractionOpts);
        freeBgData(cExtractionCuda);
#else
        std::cout << "CUDA is not supported. If it is not recognized, try fixing the problem."
                     " Otherwise, try running without using CUDA instead." << std::endl;
        exit(-1);
#endif
    }
    else {
        getBackground(bgImage, bwShape, video, dumpOpts, generalOpts, preprocessingOpts);
        // Run on CPU
        processUsingCPU(timers, overlaid_whiskers);
    }
    timers.pipeline_timer.stop();
    if (dumpOpts.getTimingProfile) {
        saveTotalTimingProfile(timers, dumpOpts.timeProfileLocation);
    }
}

#ifdef USE_CUDA
void Worker::processUsingGPU(timers timers, cv::Mat overlaid_whiskers)
{
    int numFrames = video.get(cv::CAP_PROP_FRAME_COUNT);
    std::ofstream timeProfileFile;
    if (dumpOpts.getTimingProfile) {
        // open file to dump timing profile
        timeProfileFile.open(dumpOpts.timeProfileLocation, std::ios::app);
        if (!timeProfileFile) perror("Time profile file could not be created.\n");
    }

    bool start = true;
    std::vector<whisker> lineGatherer_tracker;
    std::vector<int> order;
    std::vector<classSVMData> svmFeatures;
    std::vector<std::vector<float>> whisker_angles;
    std::vector<int> whiskers_detected_Nr;
    std::vector<std::vector<whisker>> whiskers;
    // Load existing tracks from file
    if (generalOpts.loadTracks) {
        loadTracks(start, generalOpts.loadTracksLocation,lineGatherer_tracker, order, trackOpts, trainOpts, fwptdOpts);
        if (dumpOpts.getQualityResults) {
            addAngles(whisker_angles, lineGatherer_tracker, trackOpts.couldNotFit);
//            addWhiskers(whiskers, lineGatherer_tracker, trackOpts.couldNotFit);
//            whiskers_detected_Nr.push_back(lineGatherer_tracker.size());
        }
    }

    video.set(cv::CAP_PROP_POS_FRAMES, generalOpts.startFrame);
    int i;
    for (i = generalOpts.startFrame; i < generalOpts.endFrame; i++) {
        if (i % 10) emit newFrame(i); // progress
        // check for interruption
        if (i % 50 == 0 && QThread::currentThread()->isInterruptionRequested()) {
            generalOpts.endFrame = i;
            break;
        }
        if (dumpOpts.getTimingProfile) {
            timers.FPS_timer.start();
            timers.CV_timer.start();
        }
        cv::Mat frame;

        // Capture frame-by-frame
        video >> frame;

        // If the frame is empty, break immediately
        if (frame.empty())
            break;


        if (plottingOpts.plot_unprocessed_frame)
            showImage(frame, "Frame before any processing", plottingOpts.framing/2, false, plottingOpts);
        if (plottingOpts.plot_unprocessed_frame_with_snout_line)
            showImageWithSnout(frame, generalOpts.tip, generalOpts.middle, preprocessingOpts.upscaling,
                               plottingOpts.framing/2, plottingOpts, "Frame before any processing, with snout line");


        if (generalOpts.verbose) std::cout << "~~~~~~~~~~~~\n" << "Frame " << i + 1 << " of " << numFrames << std::endl;
        plottingOpts.current_frame_no = i+1;
        if (dumpOpts.getTimingProfile) {
            timers.CV_timer.stop();
            timers.CV_time += timers.CV_timer.seconds();
            std::cout << "OpenCV overhead took " << timers.CV_timer.seconds() << " seconds." << std::endl;
        }

        std::vector<int> clusters;
        std::vector<float> rotatedPositions;
        std::vector<whisker> lineGatherer_detector;
        run_pipeline_cuda(timers, frame, false, preprocessingOpts, bgImage_d, bwShape_d, preprocessedImage_d,
                          rotatedPositions, clusters, lineGatherer_detector, cExtractionOpts, cExtractionCuda,
                          clusteringOpts, stitchIOpts, stitchIIOpts, paramOpts, plottingOpts, generalOpts);

        if (!trackOpts.dontTrack) {
            run_tracking(lineGatherer_detector, lineGatherer_tracker, rotatedPositions, clusters, order, svmFeatures,
                         start, i, generalOpts, trackOpts, trainOpts, fwptdOpts, recogOpts, NExpertOpts,
                         plottingOpts, timers);
            // plot whiskers or write video with whiskers
            if (trackOpts.plot || plottingOpts.writeVideoOutput) {
                overlaid_whiskers = plotLinesOverlaid(frame, preprocessingOpts.upscaling, lineGatherer_tracker, lineGatherer_tracker.size(),
                                   -1, order, plottingOpts, "Tracked Whiskers");
            }
            if (dumpOpts.getQualityResults) {
                // save whisker angles and number of whiskers detected
                addAngles(whisker_angles, lineGatherer_tracker, trackOpts.couldNotFit);
//                addWhiskers(whiskers, lineGatherer_tracker, trackOpts.couldNotFit);
//                whiskers_detected_Nr.push_back(lineGatherer_detector.size());
            }
        }

        // Save tracks
        if (start && dumpOpts.saveTracks) {
           saveTracks(lineGatherer_tracker, dumpOpts.saveTracksLocation);
        }

        start = false;
        if (dumpOpts.getTimingProfile) {
            timers.FPS_timer.stop();
            timers.loop_time += timers.FPS_timer.seconds();
            // Dump timing profile
            saveTimingProfile(timers, i, timeProfileFile);
            std::cout << "Frame processing took " << timers.FPS_timer.seconds() << " seconds" << std::endl;
        }
    }

    if (!trackOpts.dontTrack) {
        if (dumpOpts.getQualityResults) {
            getQualityResults(whiskers_detected_Nr, whisker_angles, whiskers, lineGatherer_tracker, trackOpts, generalOpts, plottingOpts, dumpOpts);
        }
        freeLinearProblem(svmFeatures, trackOpts, recogOpts);
    }
    if (dumpOpts.getTimingProfile) timeProfileFile.close();
}
#endif

void Worker::processUsingCPU(timers timers, cv::Mat overlaid_whiskers)
{
    int numFrames = video.get(cv::CAP_PROP_FRAME_COUNT);
    std::ofstream timeProfileFile;
    if (dumpOpts.getTimingProfile) {
        // open file to dump timing profile
        timeProfileFile.open(dumpOpts.timeProfileLocation, std::ios::app);
        if (!timeProfileFile) perror("Time profile file could not be created.\n");
    }

    bool start = true;
    std::vector<whisker> lineGatherer_tracker;
    std::vector<int> order;
    std::vector<classSVMData> svmFeatures;
    std::vector<std::vector<float>> whisker_angles;
    std::vector<std::vector<whisker>> whiskers;
    std::vector<int> whiskers_detected_Nr;

    // Load existing tracks from file
    if (generalOpts.loadTracks) {
        loadTracks(start, generalOpts.loadTracksLocation,lineGatherer_tracker, order, trackOpts, trainOpts, fwptdOpts);
        if (dumpOpts.getQualityResults) {
            addAngles(whisker_angles, lineGatherer_tracker, trackOpts.couldNotFit);
            addWhiskers(whiskers, lineGatherer_tracker, trackOpts.couldNotFit);
            whiskers_detected_Nr.push_back(lineGatherer_tracker.size());
        }
    }

    video.set(cv::CAP_PROP_POS_FRAMES, generalOpts.startFrame);
    // run pipeline sequentially
    for (int i = generalOpts.startFrame; i < generalOpts.endFrame; i++) {
        if (i % 2) emit newFrame(i); // progress
        // check for user interruption
        if (i % 10 == 0 && QThread::currentThread()->isInterruptionRequested()) {
            generalOpts.endFrame = i;
            // and exit
            break;
        }
        if (dumpOpts.getTimingProfile) {
            timers.FPS_timer.start();
            timers.CV_timer.start();
        }
        cv::Mat frame;

        // Capture frame-by-frame
        video >> frame;

        // If the frame is empty, break immediately
        if (frame.empty())
            break;

        // plot unprocessed frame with and without snout line if specified
        if (plottingOpts.plot_unprocessed_frame)
            showImage(frame, "Frame before any processing", plottingOpts.framing/2, false, plottingOpts);
        if (plottingOpts.plot_unprocessed_frame_with_snout_line)
            showImageWithSnout(frame, generalOpts.tip, generalOpts.middle, preprocessingOpts.upscaling,
                           plottingOpts.framing/2, plottingOpts, "Frame before any processing, with snout line");

        if (generalOpts.verbose) std::cout << "~~~~~~~~~~~~\n" << "Frame " << i + 1 << " of " << numFrames << std::endl;
        plottingOpts.current_frame_no = i+1;

        if (dumpOpts.getTimingProfile) {
            timers.CV_timer.stop();
            timers.CV_time += timers.CV_timer.seconds();
            std::cout << "OpenCV overhead took " << timers.CV_timer.seconds() << " seconds." << std::endl;
        }

        std::vector<int> clusters;
        std::vector<float> rotatedPositions;
        std::vector<whisker> lineGatherer_detector;
        run_pipeline(timers, frame, bgImage, bwShape, rotatedPositions, clusters, lineGatherer_detector,
                     preprocessingOpts, cExtractionOpts, clusteringOpts, stitchIOpts, stitchIIOpts, paramOpts,
                     plottingOpts, generalOpts);

        if (!trackOpts.dontTrack) {
            run_tracking(lineGatherer_detector, lineGatherer_tracker, rotatedPositions, clusters, order,
                         svmFeatures, start, i, generalOpts, trackOpts, trainOpts, fwptdOpts, recogOpts,
                         NExpertOpts, plottingOpts, timers);
            // plot whiskers or write video with whiskers
            if (trackOpts.plot || plottingOpts.writeVideoOutput) {
                overlaid_whiskers = plotLinesOverlaid(frame, preprocessingOpts.upscaling, lineGatherer_tracker, lineGatherer_tracker.size(),
                                   -1, order, plottingOpts, "Tracked Whiskers");
            }
            if (dumpOpts.getQualityResults) {
                // save whisker angles and number of whiskers detected
                addAngles(whisker_angles, lineGatherer_tracker, trackOpts.couldNotFit);
                addWhiskers(whiskers, lineGatherer_tracker, trackOpts.couldNotFit);
                whiskers_detected_Nr.push_back(lineGatherer_detector.size());
            }
        }
        // Save tracks
        if (start && dumpOpts.saveTracks) {
            saveTracks(lineGatherer_tracker, dumpOpts.saveTracksLocation);
        }

        start = false;
        if (dumpOpts.getTimingProfile) {
            timers.FPS_timer.stop();
            timers.loop_time += timers.FPS_timer.seconds();
            // Dump timing profile
            saveTimingProfile(timers, i, timeProfileFile);
            std::cout << "Frame processing took " << timers.FPS_timer.seconds() << " seconds" << std::endl;
        }
    }
    if (!trackOpts.dontTrack) {
        if (dumpOpts.getQualityResults) {
            getQualityResults(whiskers_detected_Nr, whisker_angles, whiskers, lineGatherer_tracker, trackOpts, generalOpts, plottingOpts, dumpOpts);
        }
        freeLinearProblem(svmFeatures, trackOpts, recogOpts);
    }

    if (dumpOpts.getTimingProfile) timeProfileFile.close();

}
