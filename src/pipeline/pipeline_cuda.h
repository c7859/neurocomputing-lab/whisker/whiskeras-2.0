/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PIPELINE_CUDA_H
#define PIPELINE_CUDA_H

#pragma once

#include "../options/options.h"
#include "../stages/whisker_forming/whisker_forming_options.h"
#include "../cuda/preprocessing_cuda.h"
#include "../cuda/c_extraction_cuda.h"
#include "../utils/profile.h"


void run_pipeline_cuda(timers &timers, cv::Mat frame, bool calibrationMode, preprocessing_opts preprocessingOpts,
                       cv::cuda::GpuMat bgImage_d, cv::cuda::GpuMat bwShape_d, cv::cuda::GpuMat preprocessedImage_d,
                       std::vector<float> &rotatedPositions, std::vector<int> &clusters, std::vector<whisker> &parametrizations,
                       c_extraction_opts cExtractionOpts, c_extraction_cuda cExtractionCuda, clustering_opts clusteringOpts,
                       stitch_I_opts stitchIOpts, stitch_II_opts stitchIIOpts, param_opts &paramOpts, plotting_opts plottingOpts,
		       general_opts generalOpts);

void getBackground_cuda(cv::cuda::GpuMat &bgImage_d, cv::cuda::GpuMat &bwShape_d, cv::cuda::GpuMat preprocessedImage_d, cv::VideoCapture video, 
                        dump_opts dumpOpts, general_opts generalOpts, preprocessing_opts preprocessingOpts, c_extraction_cuda cExtractionCuda);

void freeBgData(c_extraction_cuda cExtractionCuda);

void run_processing_cuda(cv::VideoCapture video, cv::cuda::GpuMat &bgImage_d, cv::cuda::GpuMat &bwShape_d,
                         cv::cuda::GpuMat &preprocessedImage_d, timers &timers, preprocessing_opts preprocessingOpts,
                         c_extraction_opts cExtractionOpts, c_extraction_cuda cExtractionCuda, clustering_opts clusteringOpts,
                         stitch_I_opts stitchIOpts, stitch_II_opts stitchIIOpts, param_opts &paramOpts, track_opts &trackOpts,
                         train_opts &trainOpts, fwptd_opts fwptdOpts, recog_opts &recogOpts, N_expert_opts NExpertOpts,
                         plotting_opts plottingOpts, general_opts generalOpts, dump_opts dumpOpts, cv::Mat &overlaid_whiskers);

#endif // PIPELINE_CUDA_H
