/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pipeline.h"
#include <iostream>
#include <fstream>
#include "../evaluation/evaluation.h"
#include "../utils/dump.h"
#include "pipeline_cuda.h"

#define CHECK(err) if (err != cudaSuccess) { \
    printf("%s in %s at line %d\n", cudaGetErrorString(err), __FILE__, __LINE__); \
    exit(EXIT_FAILURE); \
}

void run_pipeline_cuda(timers &timers, cv::Mat frame, bool calibrationMode, preprocessing_opts preprocessingOpts,
                       cv::cuda::GpuMat bgImage_d, cv::cuda::GpuMat bwShape_d, cv::cuda::GpuMat preprocessedImage_d,
                       std::vector<float> &rotatedPositions, std::vector<int> &clusters, std::vector<whisker> &parametrizations,
                       c_extraction_opts cExtractionOpts, c_extraction_cuda cExtractionCuda, clustering_opts clusteringOpts,
                       stitch_I_opts stitchIOpts, stitch_II_opts stitchIIOpts, param_opts &paramOpts, plotting_opts plottingOpts,
                       general_opts generalOpts) {
    /// transfer image to GPU
    timers.transfer_timer.start();
    cv::cuda::GpuMat frame_d(frame);
    cv::cuda::GpuMat frame_gray;
    cv::cuda::cvtColor(frame_d, frame_gray, cv::COLOR_BGR2GRAY);


    cv::cuda::GpuMat whiteImage(frame_gray.rows, frame_gray.cols, frame_gray.type(), cv::Scalar(255));
    // invert colors if background is black and whiskers are white
    if (preprocessingOpts.invertColors) {
        cv::cuda::subtract(whiteImage, frame_gray, frame_gray);
    }

    timers.transfer_timer.stop();
    timers.transfer_to_gpu_time += timers.transfer_timer.seconds();
    if (generalOpts.verbose) std::cout << "\tTransfer Image to CUDA took " << timers.transfer_timer.seconds() << " seconds" << std::endl;
    /// preprocessing
    timers.tt.start();
    preprocessing_cuda(frame_gray, bgImage_d, bwShape_d, preprocessedImage_d, preprocessingOpts, plottingOpts);
    timers.tt.stop();
    timers.preprocessing_time += timers.tt.seconds();
    if (generalOpts.verbose) std::cout << "Preprocessing took " << timers.tt.seconds() << " seconds" << std::endl;

    /// whisker point detection
    timers.tt.start();
    std::vector<std::pair<int16_t,int16_t>> startPoints;
    convertCudaMatToArr(preprocessedImage_d, cExtractionCuda);
    steger_point_detection_cuda(startPoints, cExtractionOpts, cExtractionCuda);

    timers.tt.stop();
    timers.detection_time += timers.tt.seconds();
    if (generalOpts.verbose) std::cout << "Steger whisker detection took " << timers.tt.seconds() << " seconds" << std::endl;

    timers.tt.start();
    transferResults(cExtractionCuda, cExtractionOpts.whiskerAngles, cExtractionOpts.preprocessedImage);
    timers.tt.stop();
    timers.transfer_from_gpu_time += timers.tt.seconds();
    if (generalOpts.verbose) std::cout << "Transfer detection results to host took " << timers.tt.seconds() << " seconds" << std::endl;

    // clustering
    timers.tt.start();
    timers.transfer_timer.start();
    int numOfPoints;
//    std::vector<int> clusters;
    std::vector<std::pair<uint16_t, uint16_t>> whiskerPositions;
    clustering_I(startPoints, cExtractionOpts.whiskerAngles, numOfPoints, whiskerPositions, clusters, clusteringOpts);
    timers.transfer_timer.stop();
    timers.clustering_I_time += timers.transfer_timer.seconds();
    timers.transfer_timer.start();
    int numOfClusters = clustering_II(clusters, whiskerPositions, rotatedPositions, numOfPoints, cExtractionOpts.preprocessedImage,
                  plottingOpts, clusteringOpts, stitchIOpts, stitchIIOpts, timers);
    if (numOfClusters == 0) {
        std::cout << "No clusters were found. Returning." << std::endl;
        return;
    }
    timers.transfer_timer.stop();
    timers.tt.stop();
    timers.clustering_II_time += timers.transfer_timer.seconds();
    timers.clustering_time += timers.tt.seconds();
    if (generalOpts.verbose) std::cout << "Clustering took " << timers.tt.seconds() << " seconds" << std::endl;

    /// parametrization
    timers.tt.start();
    clusters2whiskers(parametrizations, clusters, rotatedPositions, paramOpts, plottingOpts);
    timers.tt.stop();
    timers.parametrization_time += timers.tt.seconds();
    if (generalOpts.verbose) std::cout << "Parametrization took " << timers.tt.seconds() << " seconds" << std::endl;
}

void getBackground_cuda(cv::cuda::GpuMat &bgImage_d, cv::cuda::GpuMat &bwShape_d, cv::cuda::GpuMat preprocessedImage_d, cv::VideoCapture video,
                        dump_opts dumpOpts, general_opts generalOpts, preprocessing_opts preprocessingOpts, c_extraction_cuda cExtractionCuda) {
    // Get background and shape of snout
    Timer bg_shape_timer;
    std::ofstream timeProfileFile;
    if (dumpOpts.getTimingProfile) {
        // open file to dump timing profile
        timeProfileFile.open(dumpOpts.timeProfileLocation, std::ios::out);
        if (!timeProfileFile) perror("Time profile file could not be created.\n");
        bg_shape_timer.start();
    }
    bgImage_d = getBackGroundImageAndShape_cuda(video, bwShape_d, cExtractionCuda.bgImageData, cExtractionCuda.bwShapeData, preprocessingOpts, generalOpts.startFrame, generalOpts.endFrame);
    if (dumpOpts.getTimingProfile) {
        bg_shape_timer.stop();
        timeProfileFile << std::endl << "Extracted background and mouse silhouette in " << bg_shape_timer.seconds() << " seconds" << std::endl << std::endl;
    }
    // Pre-allocation for preprocessing
    allocImageData(cExtractionCuda.preprocessedImageData, cExtractionCuda.height, cExtractionCuda.width);
    preprocessedImage_d = cv::cuda::GpuMat(cExtractionCuda.height, cExtractionCuda.width, CV_32FC1, cExtractionCuda.preprocessedImageData, cv::Mat::CONTINUOUS_FLAG);

    if (dumpOpts.getTimingProfile) timeProfileFile.close();
}

void freeBgData(c_extraction_cuda cExtractionCuda) {
    freeImageData(cExtractionCuda.preprocessedImageData, cExtractionCuda.height, cExtractionCuda.width);
    freeImageData(cExtractionCuda.bgImageData, cExtractionCuda.height, cExtractionCuda.width);
    freeImageData(cExtractionCuda.bwShapeData, cExtractionCuda.height, cExtractionCuda.width);
}

void resetGPU()
{
    CHECK(cudaDeviceReset());
}

void run_processing_cuda(cv::VideoCapture video, cv::cuda::GpuMat &bgImage_d, cv::cuda::GpuMat &bwShape_d,
              cv::cuda::GpuMat &preprocessedImage_d, timers &timers, preprocessing_opts preprocessingOpts,
              c_extraction_opts cExtractionOpts, c_extraction_cuda cExtractionCuda, clustering_opts clusteringOpts,
              stitch_I_opts stitchIOpts, stitch_II_opts stitchIIOpts, param_opts &paramOpts, track_opts &trackOpts,
              train_opts &trainOpts, fwptd_opts fwptdOpts, recog_opts &recogOpts, N_expert_opts NExpertOpts,
              plotting_opts plottingOpts, general_opts generalOpts, dump_opts dumpOpts, cv::Mat &overlaid_whiskers) {

    int numFrames = video.get(cv::CAP_PROP_FRAME_COUNT);
    std::ofstream timeProfileFile;
    if (dumpOpts.getTimingProfile) {
        // open file to dump timing profile
        timeProfileFile.open(dumpOpts.timeProfileLocation, std::ios::app);
        if (!timeProfileFile) perror("Time profile file could not be created.\n");
    }

    bool start = true;
    std::vector<whisker> lineGatherer_tracker;
    std::vector<int> order;
    std::vector<classSVMData> svmFeatures;
    std::vector<std::vector<float>> whisker_angles;
    std::vector<int> whiskers_detected_Nr;
    std::vector<std::vector<whisker>> whiskers;
    // Load existing tracks from file
    if (generalOpts.loadTracks) {
        loadTracks(start, generalOpts.loadTracksLocation,lineGatherer_tracker, order, trackOpts, trainOpts, fwptdOpts);
        if (dumpOpts.getQualityResults) {
            addAngles(whisker_angles, lineGatherer_tracker, trackOpts.couldNotFit);
            addWhiskers(whiskers, lineGatherer_tracker, trackOpts.couldNotFit);
            whiskers_detected_Nr.push_back(lineGatherer_tracker.size());
        }
    }

    video.set(cv::CAP_PROP_POS_FRAMES, generalOpts.startFrame);
    for (int i = generalOpts.startFrame; i < generalOpts.endFrame; i++) {
        if (dumpOpts.getTimingProfile) {
            timers.FPS_timer.start();
            timers.CV_timer.start();
        }
        cv::Mat frame;

        // Capture frame-by-frame
        video >> frame;

        // If the frame is empty, break immediately
        if (frame.empty())
            break;

        if (plottingOpts.plot_unprocessed_frame)
            showImage(frame, "Frame before any processing", plottingOpts.framing/2, false, plottingOpts);
        if (plottingOpts.plot_unprocessed_frame_with_snout_line)
            showImageWithSnout(frame, generalOpts.tip, generalOpts.middle, preprocessingOpts.upscaling,
                               plottingOpts.framing/2, plottingOpts, "Frame before any processing, with snout line");


        std::cout << "~~~~~~~~~~~~\n" << "Frame " << i + 1 << " of " << numFrames << std::endl;
        plottingOpts.current_frame_no = i+1;
        if (dumpOpts.getTimingProfile) {
            timers.CV_timer.stop();
            timers.CV_time += timers.CV_timer.seconds();
            std::cout << "OpenCV overhead took " << timers.CV_timer.seconds() << " seconds." << std::endl;
        }

        std::vector<int> clusters;
        std::vector<float> rotatedPositions;
        std::vector<whisker> lineGatherer_detector;
        run_pipeline_cuda(timers, frame, false, preprocessingOpts, bgImage_d, bwShape_d, preprocessedImage_d,
                          rotatedPositions, clusters, lineGatherer_detector, cExtractionOpts, cExtractionCuda,
                          clusteringOpts, stitchIOpts, stitchIIOpts, paramOpts, plottingOpts, generalOpts);

        // plot whiskers in preview mode
        if (generalOpts.preview) {
                order = std::vector<int>(lineGatherer_detector.size()); // for colors
                for (int j = 0; j < lineGatherer_detector.size(); j++) {
                    order[j] = j;
                }
                overlaid_whiskers = plotLinesOverlaid(frame, preprocessingOpts.upscaling, lineGatherer_detector, lineGatherer_detector.size(),
                                   -1, order, plottingOpts, "Tracked Whiskers");
        }

        if (!trackOpts.dontTrack) {
            run_tracking(lineGatherer_detector, lineGatherer_tracker, rotatedPositions, clusters, order, svmFeatures,
                         start, i, generalOpts, trackOpts, trainOpts, fwptdOpts, recogOpts, NExpertOpts,
                         plottingOpts, timers);
            // plot whiskers or write video with whiskers
            if (trackOpts.plot || plottingOpts.writeVideoOutput) {
                overlaid_whiskers = plotLinesOverlaid(frame, preprocessingOpts.upscaling, lineGatherer_tracker, lineGatherer_tracker.size(),
                                   -1, order, plottingOpts, "Tracked Whiskers");
            }
            if (dumpOpts.getQualityResults) {
                // save whisker angles and number of whiskers detected
                addAngles(whisker_angles, lineGatherer_tracker, trackOpts.couldNotFit);
                addWhiskers(whiskers, lineGatherer_tracker, trackOpts.couldNotFit);
                whiskers_detected_Nr.push_back(lineGatherer_detector.size());
            }
        }

        // Save tracks
        if (start && dumpOpts.saveTracks) {
           saveTracks(lineGatherer_tracker, dumpOpts.saveTracksLocation);
        }

        start = false;
        if (dumpOpts.getTimingProfile) {
            timers.FPS_timer.stop();
            timers.loop_time += timers.FPS_timer.seconds();
            // Dump timing profile
            saveTimingProfile(timers, i, timeProfileFile);
            std::cout << "Frame processing took " << timers.FPS_timer.seconds() << " seconds" << std::endl;
        }
    }

    if (!trackOpts.dontTrack) {
        if (dumpOpts.getQualityResults) {
            getQualityResults(whiskers_detected_Nr, whisker_angles, whiskers, lineGatherer_tracker, trackOpts, generalOpts, plottingOpts, dumpOpts);
            clearAngles(whisker_angles, lineGatherer_tracker.size());
        }
        freeLinearProblem(svmFeatures, trackOpts, recogOpts);
    }

    if (dumpOpts.getTimingProfile) timeProfileFile.close();
}
