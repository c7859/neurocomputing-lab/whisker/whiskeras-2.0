/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_PROFILE_H
#define FWTS_C_PROFILE_H

#include "Timer.hpp"

struct timers {
    float transfer_to_gpu_time = 0.0;
    float transfer_from_gpu_time = 0.0;
    float cvt_time = 0.0;
    float preprocessing_time = 0.0;
    float detection_time = 0.0;
    float clustering_I_time = 0.0;
    float stitching_I_time = 0.0;
    float stitching_II_time = 0.0;
    float clustering_II_time = 0.0;
    float clustering_time = 0.0;
    float parametrization_time = 0.0;
    float tracking_time = 0.0;
    float training_time = 0.0;
    float loop_time = 0.0;
    float CV_time = 0.0;
    Timer tt;
    Timer transfer_timer;
    Timer pipeline_timer;
    Timer FPS_timer;
    Timer CV_timer;
};

#endif //FWTS_C_PROFILE_H
