/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "video.h"
#include <iostream>

cv::VideoCapture openVideo(const cv::String videopath) {
    // read whisker video
    cv::VideoCapture cap(videopath, cv::CAP_FFMPEG);

    // Check if camera opened successfully
    if(!cap.isOpened()){
        std::cout << "Error opening video file" << std::endl;
        exit(-1);
    }

    return cap;
}
