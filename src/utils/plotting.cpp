/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <fstream>
#include "plotting.h"
#include "../stages/whisker_point_detection/imgproc.h"
#include "../../libs/eigen/Eigen/Dense"

void rotateBackClusters(std::vector<float> rotatedPositions, std::vector<float> &plotPositions, plotting_opts plottingOpts) {
    Eigen::MatrixXf rotatedMat = Eigen::Map<Eigen::MatrixXf>(rotatedPositions.data(), 2, rotatedPositions.size()/2);
    Eigen::MatrixXf plotMat(2, rotatedPositions.size() / 2);
    Eigen::MatrixXf sbaseMat = Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(plottingOpts.sbase[0], 2, 2);
    rotatedMat.row(1) = rotatedMat.row(1).array().eval() + plottingOpts.cutoff;
    Eigen::Vector2f originVec = Eigen::Map<Eigen::Vector2f>(plottingOpts.origin, 2);
    plotMat = (sbaseMat.inverse() * rotatedMat).colwise() + originVec;
    std::vector<float> plotVec(plotMat.data(), plotMat.data() + plotMat.rows() * plotMat.cols());
    plotPositions = plotVec;
}

void plotPairOfClusters(std::vector<int> clusters, std::vector<float> rotatedPositions,
                        int whisker1, int whisker2, plotting_opts plottingOpts, std::string label) {
    std::vector<float> plotPositions;
    rotateBackClusters(rotatedPositions, plotPositions, plottingOpts);

    cv::Mat clustersImg(plottingOpts.height, plottingOpts.width, CV_8UC3, cv::Scalar(0, 0, 0));
    // asssign random colours to each cluster
    uchar blue[2]; uchar green[2]; uchar red[2];
    blue[0] = 255; red[0] = 0; green[0] = 0;
    blue[1] = 0; red[1] = 255; green[1] = 0;

    // plot only one whisker
    for (size_t i = 0; i < clusters.size(); i++) {
        if (clusters[i] == whisker1) {
            int plotX = plotPositions[2 * i];
            int plotY = plotPositions[2 * i + 1];
            clustersImg.at<cv::Vec3b>(plotY, plotX)[0] = blue[0];
            clustersImg.at<cv::Vec3b>(plotY, plotX)[1] = green[0];
            clustersImg.at<cv::Vec3b>(plotY, plotX)[2] = red[0];
        }
        else if (clusters[i] == whisker2) {
            int plotX = plotPositions[2 * i];
            int plotY = plotPositions[2 * i + 1];
            clustersImg.at<cv::Vec3b>(plotY, plotX)[0] = blue[1];
            clustersImg.at<cv::Vec3b>(plotY, plotX)[1] = green[1];
            clustersImg.at<cv::Vec3b>(plotY, plotX)[2] = red[1];
        }
    }
    showImage(clustersImg, label, plottingOpts.framing, false, plottingOpts);
}

void rotateAndPlotClusters(std::vector<int> clusters, std::vector<float> rotatedPositions, int numOfClusters,
                           int whisker_num, plotting_opts plottingOpts, bool withColors, std::string label) {
    if (numOfClusters == -1) {
        numOfClusters = *std::max_element(clusters.begin(), clusters.end());
    }
    std::vector<float> plotPositions;
    rotateBackClusters(rotatedPositions, plotPositions, plottingOpts);

    cv::Mat clustersImg(plottingOpts.height, plottingOpts.width, CV_8U, cv::Scalar(255));
    // assign uniform colors to each line, randomize order for clarity
    uchar color[numOfClusters];
    float color_step = 255 / (numOfClusters+1);
    if (numOfClusters+1 > 255) color_step = 1;
    std::vector<int> random_colors;
    for (int i = 0; i < numOfClusters; i++) random_colors.push_back(i+1);
    std::random_shuffle(random_colors.begin(), random_colors.end());


    for (int i = 0; i < numOfClusters; i++) {
        color[i] = color_step * random_colors[i];
        if (color[i] % 256 == 0) color[i] += 100;
        if (!withColors) {
            color[i] = 0;
        }
    }

    // plot only one whisker
    if (whisker_num != -1) {
        for (size_t i = 0; i < clusters.size(); i++) {
            if (clusters[i] == whisker_num) {
                int plotX = plotPositions[2 * i];
                int plotY = plotPositions[2 * i + 1];
                clustersImg.at<uchar>(plotY, plotX) = 0;
            }
        }
    }
    // plot all whiskers
    else {
        for (size_t i = 0; i < clusters.size(); i++) {
            int plotX = plotPositions[2 * i];
            int plotY = plotPositions[2 * i + 1];
            // apply a marker (make the points wider) for clarity
            for (int k = plotY - 1; k < plotY + 2; k++) {
                for (int l = plotX - 1; l < plotX + 2; l++) {
                    clustersImg.at<uchar>(k, l) = color[clusters[i]];
                }
            }
        }
    }

    // use colormap
    cv::Mat colormapClusters;
    if (withColors) {
        cv::applyColorMap(clustersImg, colormapClusters, plottingOpts.colormap);
        // get background color
        cv::Vec3b bgColor = colormapClusters.at<cv::Vec3b>(0, 0);
        cv::Vec3b white(255, 255, 255);
        // and turn it back white
        for (int y = 0; y < colormapClusters.rows; y++) {
            for (int x = 0; x < colormapClusters.cols; x++) {
                if (colormapClusters.at<cv::Vec3b>(y, x) == bgColor) {
                    colormapClusters.at<cv::Vec3b>(y, x) = white;
                }
            }
        }
    }
    else colormapClusters = clustersImg;

//    showImage(clustersImg, label, plottingOpts.framing, false, plottingOpts);
    showImage(colormapClusters, label, plottingOpts.framing, false, plottingOpts);
}

void createFraming(cv::Mat &src, int framing_width) {
    for (int r = 0; r < framing_width; r++) {
        for (int c = 0; c < src.cols; ++c) {
            src.at<cv::Vec3b>(r,c)[0] = 0;
            src.at<cv::Vec3b>(r,c)[1] = 0;
            src.at<cv::Vec3b>(r,c)[2] = 0;
            src.at<cv::Vec3b>(src.rows - 1 - r,c)[0] = 0;
            src.at<cv::Vec3b>(src.rows - 1 - r,c)[1] = 0;
            src.at<cv::Vec3b>(src.rows - 1 - r,c)[2] = 0;
        }
    }
    for (int r = 0; r < src.rows; r++) {
        for (int c = 0; c < framing_width; ++c) {
            src.at<cv::Vec3b>(r,c)[0] = 0;
            src.at<cv::Vec3b>(r,c)[1] = 0;
            src.at<cv::Vec3b>(r,c)[2] = 0;
            src.at<cv::Vec3b>(r,src.cols - 1 - c)[0] = 0;
            src.at<cv::Vec3b>(r,src.cols - 1 - c)[1] = 0;
            src.at<cv::Vec3b>(r,src.cols - 1 - c)[2] = 0;
        }
    }
}

void createFramingGrayscale(cv::Mat &src, int framing_width) {
    for (int r = 0; r < framing_width; r++) {
        for (int c = 0; c < src.cols; ++c) {
            src.at<uchar>(r,c) = 0;
            src.at<uchar>(src.rows - 1 - r,c) = 0;
        }
    }
    for (int r = 0; r < src.rows; r++) {
        for (int c = 0; c < framing_width; ++c) {
            src.at<uchar>(r,c) = 0;
            src.at<uchar>(r,src.cols - 1 - c) = 0;
        }
    }
}

void plotLines(std::vector<struct whisker> whiskers, int numOfLines, int whisker_num, std::vector<int> order, plotting_opts plottingOpts, std::string label) {
//    cv::Mat LinesImg(plottingOpts.height, plottingOpts.width, CV_8UC3, cv::Scalar(255, 255, 255));
    cv::Mat LinesImg(plottingOpts.height, plottingOpts.width, CV_8U, cv::Scalar(255));

    // assign uniform colors to each line, randomize order for clarity
    uchar color[numOfLines];
    float color_step = 255 / (numOfLines+1);
    std::vector<int> random_colors;
    std::srand(3); // set a specific RNG seed, so that same whiskers have the same colour over time
    for (int i = 0; i < numOfLines; i++) random_colors.push_back(i+1);
    std::random_shuffle(random_colors.begin(), random_colors.end());

    for (int i = 0; i < numOfLines; i++) {
        // plot only one line
        if (whisker_num != -1) i = whisker_num;
        color[i] = color_step * random_colors[i];

        int numOfPoints = static_cast<int>(whiskers[i].length);
        Eigen::MatrixXf coord(2, numOfPoints);
        for (int j = 0; j < numOfPoints; j++) {
            coord.row(0)(j) = j;
        }
        coord.row(1) = whiskers[i].shape * coord.row(0).array().pow(2);
        float q = whiskers[i].angle;
        Eigen::MatrixXf rotation(2, 2);
        rotation << cos(q), -sin(q),
                sin(q), cos(q);
        Eigen::MatrixXf coordRotated = rotation * coord;
        coordRotated.row(0) = coordRotated.row(0).array().eval() + whiskers[i].position;

        // Rotate back points
        Eigen::MatrixXf sbaseMat = Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(plottingOpts.sbase[0], 2, 2);
        coordRotated.row(1) = coordRotated.row(1).array().eval() + plottingOpts.cutoff;
        Eigen::Vector2f originVec = Eigen::Map<Eigen::Vector2f>(plottingOpts.origin, 2);
        Eigen::MatrixXf plotMat = (sbaseMat.inverse() * coordRotated).colwise() + originVec;

        // Plot
        int step = (order[i] >= 0) ? 1 : 10;
        for (int j = 0; j < numOfPoints; j += step) {
            int plotX = plotMat(0, j);
            int plotY = plotMat(1, j);
            // apply a marker (make the points wider) for clarity
            for (int k = plotY - 1; k < plotY + 2; k++) {
                for (int l = plotX - 1; l < plotX + 2; l++) {
                    if (l >= plottingOpts.width || k >= plottingOpts.height
                        || l < 0 || k < 0) continue; // don't go out of bounds
                        LinesImg.at<uchar>(k,l) = color[i];
                }
            }
        }
        if (whisker_num != -1) break;
    }

    // use colormap
    cv::Mat colorLines;
    cv::applyColorMap(LinesImg, colorLines, plottingOpts.colormap);
    // get background color
    cv::Vec3b bgColor = colorLines.at<cv::Vec3b>(0,0);
    cv::Vec3b white(255,255,255);
    // and turn it back white
    for (int y = 0; y < colorLines.rows; y++) {
        for (int x = 0; x < colorLines.cols; x++) {
            if (colorLines.at<cv::Vec3b>(y,x) == bgColor) {
                colorLines.at<cv::Vec3b>(y,x) = white;
            }
        }
    }

    showImage(colorLines, label, plottingOpts.framing, false, plottingOpts);
}

cv::Mat plotLinesOverlaid(cv::Mat unprocessed_frame, int upscaling, std::vector<struct whisker> whiskers, int numOfLines,
        int whisker_num, std::vector<int> order, plotting_opts &plottingOpts, std::string label) {
    cv::Mat LinesImg(plottingOpts.height, plottingOpts.width, CV_8U, cv::Scalar(255));

    // assign uniform colors to each line, randomize order for clarity
    uchar color[numOfLines];
    float color_step = 255 / (numOfLines+1);
    std::vector<int> random_colors;
    std::srand(3); // set a specific RNG seed, so that same whiskers have the same colour over time
    for (int i = 0; i < numOfLines; i++) random_colors.push_back(i+1);
    std::random_shuffle(random_colors.begin(), random_colors.end());

    for (int i = 0; i < numOfLines; i++) {
        // plot only one line
        if (whisker_num != -1) i = whisker_num;
        color[i] = color_step * random_colors[i];

        int numOfPoints = static_cast<int>(whiskers[i].length);
        Eigen::MatrixXf coord(2, numOfPoints);
        for (int j = 0; j < numOfPoints; j++) {
            coord.row(0)(j) = j;
        }
        coord.row(1) = whiskers[i].shape * coord.row(0).array().pow(2);
        float q = whiskers[i].angle;
        Eigen::MatrixXf rotation(2, 2);
        rotation << cos(q), -sin(q),
                sin(q), cos(q);
        Eigen::MatrixXf coordRotated = rotation * coord;
        coordRotated.row(0) = coordRotated.row(0).array().eval() + whiskers[i].position;

        // Rotate back points
        Eigen::MatrixXf sbaseMat = Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(plottingOpts.sbase[0], 2, 2);
        coordRotated.row(1) = coordRotated.row(1).array().eval() + plottingOpts.cutoff;
        Eigen::Vector2f originVec = Eigen::Map<Eigen::Vector2f>(plottingOpts.origin, 2);
        Eigen::MatrixXf plotMat = (sbaseMat.inverse() * coordRotated).colwise() + originVec;

        // circle params
        int radius = 2;
        // Plot
        int step = (order[i] >= 0) ? 30 : 10;
        for (int j = 0; j < numOfPoints; j += step) {
            int plotX = plotMat(0, j);
            int plotY = plotMat(1, j);
            // apply a marker (make the points wider) for clarity
            for (int k = plotY - 1; k < plotY + 2; k++) {
                for (int l = plotX - 1; l < plotX + 2; l++) {
                    if (l >= plottingOpts.width || k >= plottingOpts.height
                        || l < 0 || k < 0) continue; // don't go out of bounds
                    // draw circle instead for whiskers that were found
                    if (order[i] >= 0) {
                        cv::circle(LinesImg, cv::Point(l, k), radius, color[i], cv::FILLED);
                    }
                    // draw dotted line for whiskers that were not found
                    else {
                        LinesImg.at<uchar>(k, l) = color[i];
                    }
                }
            }
        }
        if (whisker_num != -1) break;
    }

    cv::Mat unprocessed_frame_upscaled;
    cv::resize(unprocessed_frame, unprocessed_frame_upscaled, cv::Size(lround(upscaling * unprocessed_frame.cols),
                                                                       lround(upscaling * unprocessed_frame.rows)), 0, 0, cv::INTER_CUBIC);

    // use colormap
    cv::Mat colorLines;
    cv::applyColorMap(LinesImg, colorLines, plottingOpts.colormap);
    // get background color
    cv::Vec3b bgColor = colorLines.at<cv::Vec3b>(0,0);
    cv::Vec3b white(255,255,255);
    // and turn it back white
    for (int y = 0; y < colorLines.rows; y++) {
        for (int x = 0; x < colorLines.cols; x++) {
            if (colorLines.at<cv::Vec3b>(y,x) == bgColor) {
                colorLines.at<cv::Vec3b>(y,x) = unprocessed_frame_upscaled.at<cv::Vec3b>(y,x);
            }
        }
    }
    // make tracking results opaque
    double alpha = 0.8; // alpha is a measure of opacity
    cv::Mat output;
    cv::addWeighted(colorLines, alpha, unprocessed_frame_upscaled, 1-alpha, 0, output);

    // write video
    if (plottingOpts.writeVideoOutput) plottingOpts.videoOutput.write(output);
    showImage(output, label, plottingOpts.framing, false, plottingOpts);
//    plottingOpts.tracked_whiskers = output;
    return output;
}

void showImage(cv::Mat image, cv::String label, int framing, bool gray, plotting_opts plottingOpts) {
    cv::Mat framedImg = image.clone();
    if (gray)
        createFramingGrayscale(framedImg, framing);
    else
        createFraming(framedImg, framing);

    if (plottingOpts.saveIntermediateResults)
        cv::imwrite(plottingOpts.outputDir + label + "_" + std::to_string(plottingOpts.current_frame_no) + ".jpg", framedImg);

    if (!plottingOpts.dontShow) {
        while (1) {
            cv::imshow(label, framedImg);
            cv::waitKey(0);
            // Press  ESC on keyboard to exit
            char c = (char) cv::waitKey(0);
            if (c == 27)
                break;
        }
    }
}

void showImageWithSnout(cv::Mat image, int tip[], int middle[], float upscaling, int framing, plotting_opts plottingOpts, cv::String label) {
    cv::Mat imageWithLine = image.clone();
    createFraming(imageWithLine, framing);
    cv::Point start(tip[0] / upscaling, tip[1] / upscaling);
    cv::Point end(middle[0] / upscaling, middle[1] / upscaling);
    cv::line(imageWithLine, start, end, cv::Scalar(255, 255, 255), 2);
    if (!plottingOpts.dontShow) {
        while (1) {
            cv::imshow(label, imageWithLine);
            cv::waitKey(0);
            // Press  ESC on keyboard to exit
            char c = (char) cv::waitKey(0);
            if (c == 27)
                break;
        }
    }
}
