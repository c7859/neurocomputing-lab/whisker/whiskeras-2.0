/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IMAGEPREVIEW_H
#define IMAGEPREVIEW_H
#include <QLabel>
#include <Qt>
#include <QString>
#include <QPixmap>
#include <QPainter>

class ImagePreview : public QLabel {
    Q_OBJECT

public:

    QString color = "black";
    explicit ImagePreview(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~ImagePreview();

    int tip[2] = {-1, -1};
    int middle[2] = {-1, -1};
    QPixmap frame_pixmap;
    QPixmap painted_pixmap;
    QPainter painter;

signals:
    void clicked();
    void lineDrawn();
    void lineCleared();

public slots:
    void clearSnoutLine();
    void drawSnoutLine();

protected:
    void mousePressEvent(QMouseEvent* event);

};

#endif // IMAGEPREVIEW_H
