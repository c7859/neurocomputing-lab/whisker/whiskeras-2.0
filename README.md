<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
***
***
***
*** To avoid retyping too much info. Do a search and replace for the following:
*** github_username, repo_name, twitter_handle, email, project_title, project_description
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!--
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]
-->



<!-- PROJECT LOGO -->
<br />
<div align="center">
<!--
  <a href="https://gitlab.com/neurocomputing-lab/whisker/whiskeras-2.0">
    <img src="images/logo.png" alt="Logo" width="640" height="480">
  </a>
-->
    <img src="GIFs/tracking.gif" alt="tracking" width="640" height="480">

  <h3 align="center">WhiskEras 2.0: Whisker Tracking on Head-Fixed Rodents</h3>

  <div align="center">
    WhiskEras 2.0 is a whisker-tracking algorithm, designed to track whiskers on untrimmed, head-restrained rodents.
    It contains two modules:
    1. A Detection Module which finds whiskers frame-by-frame on a video recorded during the whisking experiments. This module uses Computer-Vision-based algorithms.
    2. A Tracking Module which tracks whiskers over time. This module makes use of a data tracking algorithm and a Machine-Learning algorithm which deploys SVMs.
    The algorithm can process videos at more than 50 FPS, depending on the video specifications, while requiring minimal human effort to provide state-of-the-art results, to-date.
<!--
    <br />
    <a href="https://gitlab.com/neurocomputing-lab/whisker/whiskeras-2.0"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/neurocomputing-lab/whisker/whiskeras-2.0">View Demo</a>
    ·
    <a href="https://gitlab.com/neurocomputing-lab/whisker/whiskeras-2.0/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/neurocomputing-lab/whisker/whiskeras-2.0/issues">Request Feature</a>
-->
  </div>
</div>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <!-- <li><a href="#roadmap">Roadmap</a></li> -->
    <!-- <li><a href="#contributing">Contributing</a></li> -->
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <!-- <li><a href="#acknowledgements">Acknowledgements</a></li> -->
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
This project was developed by Petros Arvanitis, as a follow-up to [WhiskEras MATLAB](https://gitlab.com/neurocomputing-lab/whisker/whiskeras-matlab) by J.L.F Betting.
It embodies an accelerated-C++ version of WhiskEras 2.0, an improved version of the original WhiskEras.

<!-- 
[![Product Name Screen Shot][product-screenshot]](https://example.com)
-->

<!--
Here's a blank template to get started:
**To avoid retyping too much info. Do a search and replace with your text editor for the following:**
`parvanitis15`, `WhiskEras_2.0_GUI`, `twitter_handle`, `pearvan14@gmail.com`, `WhiskEras 2.0`, `project_description`

What whisker video setups it requires
-->

### Built With

* [CUDA](https://developer.nvidia.com/cuda-downloads)
* [OpenCV](https://github.com/opencv/opencv)
* [OpenCV extra modules (CUDA)](https://github.com/opencv/opencv_contrib)
* [Eigen](https://gitlab.com/libeigen/eigen)
* [LibLinear](https://www.csie.ntu.edu.tw/~cjlin/liblinear/)
* [Qt](https://www.qt.io/download)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.


### Prerequisites

This algorithm is intended for processing videos recorded during experiments with head-restrained rodents, focused on their whiskers. Using it on videos where the head of the rodent is moving freely will not provide accurate results and may even cause the program to crash.
In addition, the background of the video should be stable for the whole duration of whisker-tracking, meaning that only whiskers should ideally be moving, while no lighting conditions are altered or external objects are moving.

The project has only been tested on Ubuntu, CUDA-capable, systems thus far. This means that, in order to run WhiskEras 2.0 you will need a linux system and a CUDA-capable GPU, if you are going to use a GPU at all.

1. Install [CUDA](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html). WhiskEras 2.0 has been tested to work with CUDA 11.2. CUDA 10.2 was tested to work too, but then the [CUB](https://nvlabs.github.io/cub/) library will have to be downloaded and linked manually where appropriate.

2. Install OpenCV with CUDA support. An example guide can be found [here](https://gist.github.com/raulqf/f42c718a658cddc16f9df07ecc627be7). WhiskEras 2.0 has been tested to work with OpenCV 4.2.2 and 4.5.2.

3. Install [OpenBLAS](https://www.openblas.net/) (or [LAPACK](http://www.netlib.org/lapack/)). From an Ubuntu terminal
   ```sh
   sudo apt-get install libopenblas-dev # option 1
   # or
   sudo apt-get install libblas-dev liblapack-dev # option 2
   ```

4. Intall Qt5.
   ```sh
   sudo apt-get install qt5-default
   ```

The rest of the packages have been provided in _libs_ directory.

<!--
This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```
-->

### Installation

There are two ways to run WhiskEras 2.0. Either you can directly run the pre-built binary executable, or build it yourself. You can try to directly run the executable, but if some required package is missing from your system, it will probably not run.
In that case, it is better to try and build it, see the errors and address them correspondingly.

Run the binary only:

1. Download just the file <b>WhiskEras_GUI</b> in the _bin_ directory of this repository.

2. Go to the directory of the file and open a terminal in that directory (right-click on the directory, _Open From Terminal_).

3. Change permissions of the binary to make it executable
   ```sh
   chmod +x WhiskEras_GUI
   ```

4. Run the executable
   ```sh
   ./WhiskEras_GUI
   ```

Build and run:

1. Open a terminal. Clone the repo
   ```sh
   git clone https://gitlab.com/neurocomputing-lab/whisker/whiskeras-2.0
   ```

2. In case you are going to use a CUDA-capable GPU, the default CUDA directory is /usr/local/cuda.
   If this is not the case for your system, modify the line below in WhiskEras_GUI.pro file:
   ```sh
   CUDA_DIR = /usr/local/cuda
   ```

3. Build the project using QMake and Make
   ```sh
   cd whiskeras-2.0 # go to the project's directory
   mkdir build && cd build
   qmake ..
   make -j<N> # replace <N> with a number of threads that your CPU supports
   ```
4. Run the executable
   ```sh
   ./WhiskEras_GUI
   ```


<!-- USAGE EXAMPLES -->
## Usage

<!--
Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://example.com)_
-->

1. Run

2. Click on **File&rarr;Open** at the top-right corner of the window and open the whisker-video file which you want to perform whisker-tracking on. 

<div align="center">
<img src="/images/preview_tab_start.png">
</div>

3. Click on the **Draw snout line** button in the Preview tab (or in Advanced tab). In the prompted window, draw a line on the the edge of the snout: click once on the frame to place the starting point of the line, then click once more to place the ending point of the line.
If you are satisfied with the snout line drawn, click _yes_ on the prompted question, otherwise click _no_ and redraw the snout line.
For videos of type A, where only one side of the snout is visible, draw the snout line as shown in image below.
For videos of type B, where the whole snout is showing, only draw the left-side snout line on the image, also shown below. The algorithm is designed to detect and track whiskers only on one side of the snout.

Also, if part of the video is either not processed or unwanted (e.g black frames), these can be removed from the background extraction stage, by moving the sliders of _First frame_ and _Last frame_ to exclude them. This option has been included because the program was found to crash when involving black frames during background extraction, using a GPU.

_Note: it is not important whether the line starts and ends at the edges of the image itself to capture the whole snout showing. But, it is important for the middle of the line to be approx. at the center of the snout shown, i.e you should not draw a line which ends midway of the snout._

<div align="center">
<img src="/images/draw_snout_line_dialog.png" width="500">
</div>

Snout line - Video A             |  Snout line - Video B
:-------------------------:|:-------------------------:
![](/images/snout_line_drawn.png)  |  ![](/images/snout_line_drawn_2.png)

If the whiskers which need to be tracked grow upwards from the snout, then check the box _Whiskers grow upwards_. For example, in the image below, whiskers on the upper side of the snout are going to be tracked.

<div align="center">
<img src="/images/snout_line_whiskers_upwards.png" width="500">
</div>

3. Preview whisker-detection results. You can view results from different frames of the video, simply by changing the _frame_ index.
In addition, the most important and tunable parameters are presented at the bottom of the **Preview** tab. You can observe the different results as you alter these options on-the-fly.
For get an insight of the algorithm and how each parameter functions and influences results, read the [parameter-tuning guide](WhiskEras_2.0_tuning_guide.pdf).

<div align="center">
  <img src="/images/preview.png" width="600">
</div>

For more advanced users, the **Advanced** tab allows the user to specify every parameter value of WhiskEras 2.0.
Note that only the **Detection Module** options alter detection results, thus generate a different preview of the results.
**Tracking Module** options only concern how the algorithm tracks whiskers over time.
Additionally, you can save all options using the **Save Options** button and load them back by pressing **Load Options**.
These options are saved in the machine's registry file, associated with this application. You can save and load different sets of options by specifying a name for your options in the **Options Entry**, e.g "Settings Video A" or "Settings Video B".
The default Options Entry name is blank.


<div align="center">
  <img src="/images/advanced_tab.png" width="600">
</div>

4. To track whiskers over the video duration, go to the **Processing** tab. You can change the first and last frame to run over a smaller segment of the video. 
You can specify the directory where results will be saved by pressing the button next to **Output Dir.**, with a filename identical to the video being processed.
Start processing the video by hitting the **Start** button.
You also have the option of stopping the processing midway through, by clicking **Stop**. In that case, results up to that point of processing will be generated.

Another option is to output a video, with the tracking results, similar to the preview. This can help you assess the quality of tracking during processing. It can be enabled by checking the box next to **Tracking Video Output Dir.** and specifying a directory path for the video output.

**WARNING**: by enabling the Tracking Video Output, processing becomes significantly slower. You are advised to do this only for small video segments.

<div align="center">
  <img src="/images/processing_tab.png">
</div>


<!-- ROADMAP -->
<!--
## Roadmap

See the [open issues](https://github.com/parvanitis15/WhiskEras_2.0_GUI/issues) for a list of proposed features (and known issues).
-->


<!-- CONTRIBUTING -->
<!--
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request
-->

<!-- LICENSE -->
## License

Distributed under the GPL license. See `LICENSE` for more information.

Third-party libraries include [Eigen](https://gitlab.com/libeigen/eigen), [LibLinear](https://www.csie.ntu.edu.tw/~cjlin/liblinear/) and [Polyfit](https://github.com/patLoeber/Polyfit). 
They are located in folder _libs_, attached with their own license/copyright files in their corresponding directories.
Also, _cuda/samples_ contains files which came with the NVIDIA Toolkit and were modified for the purposes of this project. Its license is provided in that directory.

<!-- CONTACT -->
## Contact

You can contact me regarding bugs/issues/questions.
<!--
Your Name - [@twitter_handle](https://twitter.com/twitter_handle) - pearvan14@gmail.com
-->
Petros Arvanitis - pearvan14@gmail.com

Project Link: [https://gitlab.com/neurocomputing-lab/whisker/whiskeras-2.0](https://gitlab.com/neurocomputing-lab/whisker/whiskeras-2.0)


<!-- ACKNOWLEDGEMENTS -->
<!--
## Acknowledgements

* []()
* []()
* []()
-->




<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/parvanitis15/repo.svg?style=for-the-badge
[contributors-url]: https://github.com/parvanitis15/repo/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/parvanitis15/repo.svg?style=for-the-badge
[forks-url]: https://github.com/parvanitis15/repo/network/members
[stars-shield]: https://img.shields.io/github/stars/parvanitis15/repo.svg?style=for-the-badge
[stars-url]: https://github.com/parvanitis15/repo/stargazers
[issues-shield]: https://img.shields.io/github/issues/parvanitis15/repo.svg?style=for-the-badge
[issues-url]: https://github.com/parvanitis15/repo/issues
[license-shield]: https://img.shields.io/github/license/parvanitis15/repo.svg?style=for-the-badge
[license-url]: https://github.com/parvanitis15/repo/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/parvanitis15
