#-------------------------------------------------
#
# Project created by QtCreator 2021-04-08T20:42:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WhiskEras_GUI
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# C++ flags
QMAKE_CXXFLAGS+= -O3 -DNDEBUG

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#######################################
## OPENMP
#######################################
DEFINES += "USE_OPENMP=1"
QMAKE_CXXFLAGS+= -fopenmp
QMAKE_LFLAGS +=  -fopenmp
#######################################

SOURCES += \
        src/main.cpp \
        src/mainwindow.cpp \
        # Options
        src/options/options.cpp \
        # Utils
        src/utils/video.cpp \
        src/utils/error.cpp \
        src/utils/plotting.cpp \
        src/utils/dump.cpp \
        # Pipeline
        src/pipeline/pipeline.cpp \
        # Whisker-point Detection
        src/stages/whisker_point_detection/preprocessing.cpp \
        src/stages/whisker_point_detection/imgproc.cpp \
        src/stages/whisker_point_detection/c_extraction.cpp \
        # Whisker Forming
        src/stages/whisker_forming/clustering.cpp \
        src/stages/whisker_forming/stitching_I.cpp \
        src/stages/whisker_forming/stitching_II.cpp \
        src/stages/whisker_forming/parametrization.cpp \
        # Tracking over Time
        src/stages/tracking_over_time/tracking.cpp \
        src/stages/tracking_over_time/training.cpp \
        src/stages/tracking_over_time/recognition.cpp \
        src/stages/tracking_over_time/kalmanFitting.cpp \
        #libLinear
        libs/liblinear/linear.cpp libs/liblinear/newton.cpp \
        libs/liblinear/blas/daxpy.c libs/liblinear/blas/ddot.c \
        libs/liblinear/blas/dnrm2.c libs/liblinear/blas/dscal.c \
        # Evaluation
        src/evaluation/evaluation.cpp \
        # Qt
        imagepreview.cpp \
        src/snoutlinewindow.cpp \
        src/worker.cpp \

CUDA_SOURCES += \
        src/cuda/preprocessing_cuda.cu \
        src/cuda/cuda_kernels.cu \
        src/cuda/c_extraction_cuda.cu \
        src/pipeline/pipeline_cuda.cu \
        src/cuda/samples/convolutionSeparable.cu \

HEADERS += \
        src/mainwindow.h \
        # Options
        src/options/options.h \
        # Utils
        src/utils/Timer.hpp src/utils/video.h \
        src/utils/error.h \
        src/utils/plotting.h \
        src/utils/dump.h \
        # Pipeline
        src/utils/profile.h src/pipeline/pipeline.h \
        # Whisker-point Detection
        src/stages/whisker_point_detection/preprocessing.h \
        src/stages/whisker_point_detection/imgproc.h \
        src/stages/whisker_point_detection/c_extraction.h \
        # Whisker Forming
        src/stages/whisker_forming/clustering.h \
        src/stages/whisker_forming/stitching_I.h \
        src/stages/whisker_forming/stitching_II.h \
        src/stages/whisker_forming/parametrization.h \
        src/stages/whisker_forming/whisker_forming_options.h \
        # Tracking over Time
        src/stages/tracking_over_time/tracking.h \
        src/stages/tracking_over_time/training.h \
        src/stages/tracking_over_time/recognition.h \
        src/stages/tracking_over_time/kalmanFitting.h \
        #libLinear
        libs/liblinear/linear.h libs/liblinear/newton.h \
        libs/liblinear/blas/blas.h libs/liblinear/blas/blasp.h \
        # Polyfit (kalman fitting)
        libs/polyfitEigen/PolyfitEigen.hpp \
        # Evaluation
        src/evaluation/evaluation.h \
        # Qt
        imagepreview.h \
        src/snoutlinewindow.h \
        src/worker.h \


FORMS += \
        mainwindow.ui \
        snoutlinewindow.ui

# OpenCV
INCLUDEPATH += /usr/local/include/opencv4
LIBS += $(shell pkg-config opencv --libs)

########################################################################

#  CUDA

########################################################################

DEFINES += "USE_CUDA=1"
CUDA_DIR = /usr/local/cuda
INCLUDEPATH  += $$CUDA_DIR/include
INCLUDEPATH += $$CUDA_DIR/samples/common/inc # helper.h CUDA
QMAKE_LIBDIR += $$CUDA_DIR/lib64
LIBS += -lcudart -lcuda
#CUDA_ARCH = sm_75 # modify <75> and insert your GPU's compute capability
#NVCCFLAGS     = --compiler-options -fno-strict-aliasing -use_fast_math --ptxas-options=-v
CUDA_INC = $$join(INCLUDEPATH,' -I','-I',' ')
cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -O3 -c $$NVCCFLAGS \
            $$CUDA_INC $$LIBS  ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT} \
            2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2
cuda.dependency_type = TYPE_C
cuda.depend_command = $$CUDA_DIR/bin/nvcc -O3 -M $$CUDA_INC $$NVCCFLAGS ${QMAKE_FILE_NAME} | sed \"s/^.*: //\"
cuda.input = CUDA_SOURCES
cuda.output = ${OBJECTS_DIR}${QMAKE_FILE_BASE}_cuda.o
QMAKE_EXTRA_COMPILERS += cuda

#########################################################################



